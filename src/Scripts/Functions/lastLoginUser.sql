DELIMITER $$
DROP function IF EXISTS `lastLoginUser`$$
CREATE FUNCTION `lastLoginUser` (`user_id` INT, `company_id` INT)
RETURNS DATETIME
BEGIN
	SET @lastLogin = (SELECT date FROM auth_logins 
						JOIN users ON auth_logins.user_id = users.id 
						WHERE auth_logins.user_id = `user_id`
                        AND users.company_id = `company_id`
                        ORDER BY auth_logins.date DESC
                        LIMIT 1
                   );
	RETURN @lastLogin;
END$$

#SELECT lastLoginUser(1, 1);
