DELIMITER $$
DROP function IF EXISTS `bulkPricesProduct`$$
CREATE FUNCTION `bulkPricesProduct` (`product_id` INT, `default_currency` VARCHAR(5), `currency` VARCHAR(5), `company_id` INT)
RETURNS VARCHAR(1000)
BEGIN
	SET @exchange_rate_currency = (SELECT IFNULL(exchange_rate, 1) FROM currencies WHERE iso_code = `currency` AND company_id = `company_id`);
	SET @exchange_rate_default = (SELECT IFNULL(exchange_rate, 1) FROM currencies WHERE iso_code = `default_currency` AND company_id = `company_id`);
	SET @Cadena = (
			SELECT 
			IFNULL(CONCAT('"[', 
				GROUP_CONCAT('{"id": ', BP.id , ', "max_bulk": ',BP.max_bulk,', "original_sale_price": ', (SELECT calculateSalePriceProduct( BP.sale_price, 0 , 'percentage', `default_currency`, BP.company_id)) , ', "sale_price": ', (SELECT calculateSalePriceProduct( BP.sale_price, BP.discount, 'percentage', `default_currency`, BP.company_id)) , ', "exchange_original_sale_price": ', ROUND(BP.sale_price * (SELECT IFNULL(exchange_rate, 1) FROM currencies WHERE iso_code = `currency` AND company_id = `company_id`), 4) , ', "exchange_sale_price": ', (SELECT calculateSalePriceProduct( BP.sale_price, BP.discount, 'percentage', `currency`, BP.company_id)) ,', "discount": ',IFNULL(BP.discount,0), ', "unit_sale_price": ', ROUND((BP.unit_sale_price * @exchange_rate_default),2) ,', "exchange_unit_sale_price": ' , ROUND((BP.unit_sale_price * @exchange_rate),2) , ', "allow_select_qty": ', BP.allow_select_qty ,'}' SEPARATOR ',') ,
                ']"'),'"[]"')
			FROM bulk_pricing BP
            WHERE BP.product_id = `product_id`
            AND BP.company_id = `company_id`
            AND (
				(BP.valid_from IS NOT NULL AND BP.valid_until IS NULL AND BP.valid_from >= NOW())
				OR (BP.valid_from IS NULL AND BP.valid_until IS NOT NULL AND BP.valid_until > NOW())
                OR (BP.valid_from IS NOT NULL AND BP.valid_until IS NOT NULL AND NOW() BETWEEN BP.valid_from AND BP.valid_until)
                OR (BP.valid_from IS NULL AND BP.valid_until IS NULL)
                )
            );
	RETURN @Cadena;
END$$

#SELECT bulkPricesProduct(1,'ARS', 'ARS', 1);


DELIMITER ;