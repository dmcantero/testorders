DELIMITER $$
DROP function IF EXISTS `calculateSalePriceProduct`$$
CREATE FUNCTION `calculateSalePriceProduct` (`original_sale_price` DECIMAL(10, 4), `discount` DECIMAL(10, 4), `type_discount` VARCHAR(10), `currency` VARCHAR(5), `company_id` INT)
RETURNS DECIMAL(10,4)
BEGIN
	SET @defaultCurrencyAdmin = (SELECT IFNULL(`value`, 'ARS') FROM settings WHERE `key` = 'default_currency_admin' AND company_id = `company_id`);
	SET @exchange_rate = (SELECT IFNULL(exchange_rate, 1) FROM currencies WHERE iso_code = `currency` AND company_id = `company_id`);
    SET @mount_discount = IFNULL((SELECT CASE WHEN `type_discount` = 'percentage' THEN ((`original_sale_price` *  `discount`) / 100) ELSE `discount` END),0);
	
    SET @Price = (SELECT 
					CASE 
					WHEN @defaultCurrencyAdmin = `currency` THEN ROUND((`original_sale_price` - @mount_discount) * 1, 4 )
					WHEN @defaultCurrencyAdmin = 'ARS' AND `currency` = 'USD' THEN ROUND((`original_sale_price` - @mount_discount) / @exchange_rate, 4 )
					WHEN @defaultCurrencyAdmin = 'USD' AND `currency` = 'ARS' THEN ROUND((`original_sale_price` - @mount_discount) * @exchange_rate, 4 )
                END);
    
	RETURN @Price;
END$$

#SELECT calculateSalePriceProduct(100, 0, 'mount', 'ARS', 1);
