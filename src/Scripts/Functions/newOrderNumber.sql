DELIMITER $$
DROP FUNCTION IF EXISTS `newOrderNumber`$$
CREATE FUNCTION `newOrderNumber` (`company_id` INT)
RETURNS NVARCHAR(100)
BEGIN
	SET @NewCode = (SELECT CONCAT('NP-',LPAD((IFNULL(COUNT(*),0) + 1) ,10,'00'))
					FROM orders ORD 
                    WHERE ORD.company_id = `company_id`
    );
            
	RETURN @NewCode;
END$$

/*
TEST

SELECT NewOrderCode(1)
*/