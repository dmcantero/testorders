DELIMITER $$
DROP function IF EXISTS `countOrdersClient`$$
CREATE FUNCTION `countOrdersClient` (`client_id` INT, `company_id` INT)
RETURNS INTEGER
BEGIN
	SET @count = IFNULL((SELECT COUNT(*) FROM orders WHERE client_id = `client_id` AND company_id = `company_id`),0);
	RETURN @count;
END$$

#SELECT countOrdersClient(1, 1);
