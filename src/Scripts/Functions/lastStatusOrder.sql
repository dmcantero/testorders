DELIMITER $$
DROP function IF EXISTS `lastStatusOrder`$$
CREATE FUNCTION `lastStatusOrder` (orderId INT, companyId INT)
RETURNS VARCHAR(20)
BEGIN
	SET @lastStatus = (SELECT status FROM order_status 
						WHERE order_id = orderId
                        AND company_id = company_id
                        ORDER BY date DESC
                        LIMIT 1
					);
	RETURN @lastStatus;
END$$

#SELECT lastStatusOrder(1, 1);
