DELIMITER $$
DROP procedure IF EXISTS `GetDetailsOrder`$$

CREATE PROCEDURE `GetDetailsOrder`(
				IN orderId INT,
                IN currencyQuery VARCHAR(5),
				IN companyId INT)
BEGIN
		SET @row_number=0;
		#SET @defaultCurrency = (SELECT CASE WHEN currency = '' OR currency = 'ARS' THEN 'USD' ELSE 'ARS' END);
        #SET @exchange_rate = (SELECT exchange_rate FROM currencies WHERE iso_code = @defaultCurrency);
        SET @exchange_rate_order = (SELECT exchange_rate FROM orders WHERE id = orderId);
		SET @currency_order = (SELECT `currency` FROM orders WHERE id = orderId);
        
		SELECT 
			DET.id,
            DET.order_id,
            DET.product_id,
            currencyQuery,
            @currency_order,
            (SELECT new_name FROM uploads WHERE `type` = 'products' AND id_type = PROD.id LIMIT 1) AS product_image_url,
            PROD.name AS product_name,
            PROD.code AS product_code,
            #bulk price data
            FORMAT(FLOOR(IFNULL(DET.bulk_items, 1)),0) AS bulk_items_format,
            CASE 
            WHEN currencyQuery = 'USD' AND @currency_order = 'ARS' THEN FORMAT((DET.bulk_price / @exchange_rate_order), 2, 'es_AR')
			WHEN currencyQuery = 'ARS' AND @currency_order = 'USD' THEN FORMAT((DET.bulk_price * @exchange_rate_order), 2, 'es_AR')
            ELSE FORMAT(DET.bulk_price, 2, 'es_AR')
            END AS bulk_price_format,
            
            DET.bulk_price AS original_bulk_price,
            
			CASE 
            WHEN currencyQuery = 'USD' AND @currency_order = 'ARS' THEN FORMAT((DET.bulk_unit_price / @exchange_rate_order), 2, 'es_AR')
			WHEN currencyQuery = 'ARS' AND @currency_order = 'USD' THEN FORMAT((DET.bulk_unit_price * @exchange_rate_order), 2, 'es_AR')
            ELSE FORMAT(DET.bulk_unit_price, 2, 'es_AR')
            END AS bulk_unit_price_format,
            
            DET.bulk_unit_price AS original_bulk_unit_price,
            
            #detail data
            FORMAT(FLOOR(DET.quantity),0) AS quantity_format,
            
            CASE 
            WHEN currencyQuery = 'USD' AND @currency_order = 'ARS' THEN FORMAT((DET.unit_price / @exchange_rate_order), 2, 'es_AR')
			WHEN currencyQuery = 'ARS' AND @currency_order = 'USD' THEN FORMAT((DET.unit_price * @exchange_rate_order), 2, 'es_AR')
            ELSE FORMAT(DET.unit_price, 2, 'es_AR')
            END AS unit_price_format,
            
            DET.unit_price AS original_unit_price,
            
            CASE 
            WHEN currencyQuery = 'USD' AND @currency_order = 'ARS' THEN FORMAT((DET.subtotal / @exchange_rate_order), 2, 'es_AR')
			WHEN currencyQuery = 'ARS' AND @currency_order = 'USD' THEN FORMAT((DET.subtotal * @exchange_rate_order), 2, 'es_AR')
            ELSE FORMAT(DET.subtotal, 2, 'es_AR')
            END AS subtotal_format,
            
            DET.subtotal AS original_subtotal,
            
            FORMAT(DET.confirmed_quantity, 0) AS confirmed_quantity_format,
            
            CASE 
            WHEN currencyQuery = 'USD' AND @currency_order = 'ARS' THEN FORMAT((DET.confirmed_subtotal / @exchange_rate_order), 2, 'es_AR')
			WHEN currencyQuery = 'ARS' AND @currency_order = 'USD' THEN FORMAT((DET.confirmed_subtotal * @exchange_rate_order), 2, 'es_AR')
            ELSE FORMAT(DET.confirmed_subtotal, 2, 'es_AR')
            END AS confirmed_subtotal_format,
            
            DET.confirmed_subtotal AS original_confirmed_subtotal,
            
            DET.bulk_items,
            
            CASE 
            WHEN currencyQuery = 'USD' AND @currency_order = 'ARS' THEN FORMAT((DET.bulk_price / @exchange_rate_order), 2)
			WHEN currencyQuery = 'ARS' AND @currency_order = 'USD' THEN FORMAT((DET.bulk_price * @exchange_rate_order), 2)
            ELSE DET.bulk_price
            END AS bulk_price,
            
            CASE 
            WHEN currencyQuery = 'USD' AND @currency_order = 'ARS' THEN FORMAT((DET.bulk_unit_price / @exchange_rate_order), 2)
			WHEN currencyQuery = 'ARS' AND @currency_order = 'USD' THEN FORMAT((DET.bulk_unit_price * @exchange_rate_order), 2)
            ELSE DET.bulk_unit_price
            END AS bulk_unit_price,
            
            DET.quantity,
            
            CASE 
            WHEN currencyQuery = 'USD' AND @currency_order = 'ARS' THEN FORMAT((DET.unit_price / @exchange_rate_order), 2)
			WHEN currencyQuery = 'ARS' AND @currency_order = 'USD' THEN FORMAT((DET.unit_price * @exchange_rate_order), 2)
            ELSE DET.unit_price
            END AS unit_price,
            
            CASE 
            WHEN currencyQuery = 'USD' AND @currency_order = 'ARS' THEN FORMAT((DET.subtotal / @exchange_rate_order), 2)
			WHEN currencyQuery = 'ARS' AND @currency_order = 'USD' THEN FORMAT((DET.subtotal * @exchange_rate_order), 2)
            ELSE DET.subtotal
            END AS subtotal,
            
            DET.confirmed_quantity,
            
            CASE 
            WHEN currencyQuery = 'USD' AND @currency_order = 'ARS' THEN FORMAT((DET.confirmed_subtotal / @exchange_rate_order), 2)
			WHEN currencyQuery = 'ARS' AND @currency_order = 'USD' THEN FORMAT((DET.confirmed_subtotal * @exchange_rate_order), 2)
            ELSE DET.confirmed_subtotal
            END AS confirmed_subtotal,
		
			(@row_number := @row_number + 1) AS rn
        FROM order_details DET
        JOIN products PROD
        ON DET.product_id = PROD.id
        AND DET.company_id = PROD.company_id
        WHERE DET.order_id = orderId
        AND DET.company_id = companyId;
        
END$$

/*====================== TEST ===================
@totalRecords INT(2)
@filteredRecords INT(2)

call GetDetailsOrder(1, 'USD', 1);
select @totalRecords;
select @firtsRecords;
select @lastRecords;

*/
