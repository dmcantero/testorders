DELIMITER $$
DROP PROCEDURE IF EXISTS `GetProductFeatures`$$
CREATE PROCEDURE `GetProductFeatures`(
				IN companyId INT(11),
				IN productId INT(11))
BEGIN
		SET @row_number=0;
        
		SELECT 	
				pf.id,
                pf.product_id,
                pf.feature_id,
                pf.value_id AS feature_value_id,
                f.name AS feature,
				fv.value,
                pf.custom_value,
              	(@row_number := @row_number + 1) AS rn
        FROM product_features pf
        INNER JOIN features f
        ON pf.feature_id = f.id
        AND pf.company_id = f.company_id
        LEFT JOIN features_values fv
        ON fv.id = pf.value_id
        WHERE pf.product_id = productId
        AND pf.company_id = companyId
        AND pf.deleted_at IS NULL
        ORDER BY f.name;
END$$

/*====================== TEST ===================
CALL GetProductFeatures(1, 1);
*/
