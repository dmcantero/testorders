DELIMITER $$
DROP procedure IF EXISTS `ProcessOrder`$$

CREATE PROCEDURE `ProcessOrder`(
				IN id INT,
                IN clientId INT,
                IN userId INT,
                IN confirmationDate DATETIME,
                IN deliveryDate DATETIME,
                IN shippingDate DATETIME,
                IN shippingMethod VARCHAR(10),
                IN freeShipping VARCHAR(1),
                IN clientFullName VARCHAR(100),
                IN estimateId INT,
                IN discountPercentage DECIMAL(10,4),
                IN saleConditionId INT,
                IN creditTermId INT,
                IN notes VARCHAR(200),
                IN `status` VARCHAR(20),
				IN companyId INT,
                IN userName VARCHAR(100)
                )
BEGIN

         IF Id = 0 THEN
			CALL InsertOrder(clientId ,
                userId ,
                confirmationDate,
                deliveryDate ,
                shippingDate ,
                shippingMethod ,
                freeShipping ,
                clientFullName ,
                estimateId ,
                discountPercentage ,
                saleConditionId ,
                creditTermId ,
                notes ,
				companyId ,
                userName );
         ELSE
			CALL UpdateOrder(id,
                confirmationDate,
                deliveryDate,
                shippingDate,
                shippingMethod ,
                freeShipping ,
                clientFullName ,
                estimateId ,
                discountPercentage ,
                saleConditionId ,
                creditTermId ,
                notes ,
                `status`,
				companyId ,
                userName);
         END IF;
END$$

/*====================== TEST ===================

call ProcessOrder(1,
	1,
     2,
      NULL,
          NULL,
              NULL,
                'DELIVERY',
                '0',
                '',
                NULL,
                0,
                NULL,
                NULL,
                'qadasd',
                'CONFIRMED',
				1,
                'TEST');


*/
