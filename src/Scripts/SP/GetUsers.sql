DELIMITER $$
DROP PROCEDURE IF EXISTS `GetUsers`$$
CREATE PROCEDURE `GetUsers`(
				IN companyId INT(11),
                IN `username` VARCHAR(30),
                IN `email` VARCHAR(254),
                IN orderBy VARCHAR(20),
                #para paginado
                IN pageIndex INT(2),
                IN pageSize INT(2),
                OUT totalRecords INT(2),
                OUT firtsRecords INT(2),
                OUT lastRecords INT(2))
BEGIN
		SET @row_number=0;
        
        SET @row_number=0;
        
        SET firtsRecords = (
        (pageIndex - 1) * pageSize + 1
        );
        SET lastRecords = (
        (pageIndex - 1) * pageSize + pageSize
        );
        
		SELECT * FROM 
        (
			SELECT 
            U.id AS Id,
            U.username,
            U.first_name,
            U.last_name,
            ID.secret AS email,
            U.active,
            GU.group,
            lastLoginUser(U.id, U.company_id) AS last_login,
            (@row_number := @row_number + 1) AS rn
            FROM users U
            JOIN auth_groups_users GU
            ON U.id = GU.user_id
            JOIN auth_identities ID
            ON U.id = ID.user_id
            WHERE
				(`username` IS NULL OR `username` = '' OR  U.username LIKE CONCAT('%',`username`,'%'))
                AND (`email` IS NULL OR `email` = '' OR ID.secret  LIKE CONCAT('%',`email`,'%'))
                AND U.company_id = companyId
            ORDER BY 
			 (CASE WHEN orderBy = 'username asc' 	THEN U.username END) ASC,
			 (CASE WHEN orderBy = 'username desc' 	THEN U.username END) DESC,
			 (CASE WHEN orderBy = 'email asc' 		THEN ID.secret END) ASC,
			 (CASE WHEN orderBy = 'email desc' 		THEN ID.secret END) DESC
           ) Records
         WHERE Records.rn BETWEEN firtsRecords AND lastRecords
         ORDER BY Records.rn ASC;
        
         #Properties
			SELECT 'username' AS Identificador,
			'Usuario' AS Nombre,
			1 AS Filtrable,
			1 AS Ordenable,
			'textbox' AS TipoFiltro,
			'' AS ValoresFiltro,
            NULL AS TipoCeldaInput
		UNION ALL
			SELECT 'first_name' AS Identificador,
			'Nombre' AS Nombre,
			0 AS Filtrable,
			0 AS Ordenable,
			'' AS TipoFiltro,
			'' AS ValoresFiltro,
            NULL AS TipoCeldaInput
		UNION ALL
			SELECT 'last_name' AS Identificador,
			'Apellido' AS Nombre,
			0 AS Filtrable,
			0 AS Ordenable,
			'' AS TipoFiltro,
			'' AS ValoresFiltro,
            NULL AS TipoCeldaInput
		UNION ALL
			SELECT 'email' AS Identificador,
			'Email' AS Nombre,
			1 AS Filtrable,
			1 AS Ordenable,
			'textbox' AS TipoFiltro,
			'' AS ValoresFiltro,
            NULL AS TipoCeldaInput
		UNION ALL
			SELECT 'group' AS Identificador,
			'Rol' AS Nombre,
			0 AS Filtrable,
			0 AS Ordenable,
			'' AS TipoFiltro,
			'' AS ValoresFiltro,
            NULL AS TipoCeldaInput
		UNION ALL
			SELECT 'active' AS Identificador,
			'Estado' AS Nombre,
			0 AS Filtrable,
			0 AS Ordenable,
			'' AS TipoFiltro,
			'' AS ValoresFiltro,
            NULL AS TipoCeldaInput
		UNION ALL
			SELECT 'last_login' AS Identificador,
			'Ult. Ingreso' AS Nombre,
			0 AS Filtrable,
			0 AS Ordenable,
			'' AS TipoFiltro,
			'' AS ValoresFiltro,
            NULL AS TipoCeldaInput;
            
        SET totalRecords = (SELECT  COUNT(*)
							FROM users U
							JOIN auth_groups_users GU
							ON U.id = GU.user_id
							JOIN auth_identities ID
							ON U.id = ID.user_id
							WHERE
							(`username` IS NULL OR `username` = '' OR  U.username LIKE CONCAT('%',`username`,'%'))
							AND (`email` IS NULL OR `email` = '' OR ID.secret  LIKE CONCAT('%',`email`,'%'))
							AND U.company_id = companyId);
	
		SELECT totalRecords, firtsRecords, lastRecords;
END$$

/*====================== TEST ===================
CALL GetUsers(1,'','info', 'username ASC', 1, 10, @totalRecords, @firtsRecords, @lastRecords);
*/
