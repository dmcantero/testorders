DELIMITER $$
DROP procedure IF EXISTS `GetCurrencies`$$

CREATE PROCEDURE `GetCurrencies`(
				IN companyId INT,
                IN `name` VARCHAR(100),
                IN orderBy VARCHAR(20),
                #para paginado
                IN pageIndex INT(2),
                IN pageSize INT(2),
                OUT totalRecords INT(2),
                OUT firtsRecords INT(2),
                OUT lastRecords INT(2))
BEGIN
		SET @row_number=0;
	
		SET firtsRecords = (
        (pageIndex - 1) * pageSize + 1
        );
        SET lastRecords = (
        (pageIndex - 1) * pageSize + pageSize
        );
        
		SELECT *
        FROM 
        (
		SELECT 
			c.id AS Id,
            c.currency,
            c.iso_code,
            IFNULL(c.exchange_rate, 1) as exchange_rate,
            c.active,
			(@row_number := @row_number + 1) AS rn
        FROM currencies c
        WHERE (`name` IS NULL OR `name` = '' OR c.name LIKE CONCAT('%',`name`,'%'))
        AND c.company_id = company_id
        ORDER BY 
         (CASE WHEN orderBy = 'name ASC' OR orderBy = '' THEN c.name END) ASC,
         (CASE WHEN orderBy = 'name DESC' THEN c.name END) DESC
        ) Records 
        WHERE Records.rn BETWEEN firtsRecords AND lastRecords
        ORDER BY Records.rn ASC;
        
        
        #Properties
        SELECT 'currency' AS Identificador,
			'Moneda' AS Nombre,
			0 AS Filtrable,
			0 AS Ordenable,
			'' AS TipoFiltro,
			'' AS ValoresFiltro,
            NULL AS TipoCeldaInput
		UNION ALL
			SELECT 'iso_code' AS Identificador,
			'Code' AS Nombre,
			0 AS Filtrable,
			0 AS Ordenable,
			'' AS TipoFiltro,
			'' AS ValoresFiltro,
            NULL AS TipoCeldaInput
		UNION ALL
			SELECT 'exchange_rate' AS Identificador,
			'Tasa cambio' AS Nombre,
			0 AS Filtrable,
			0 AS Ordenable,
			'' AS TipoFiltro,
			'' AS ValoresFiltro,
            NULL AS TipoCeldaInput
		UNION ALL
			SELECT 'active' AS Identificador,
			'Activa' AS Nombre,
			0 AS Filtrable,
			0 AS Ordenable,
			'' AS TipoFiltro,
			'' AS ValoresFiltro,
			'checkbox' AS TipoCeldaInput;
        
        SET totalRecords = (
							SELECT COUNT(*) 
							FROM currencies c
								  WHERE (`name` IS NULL OR `name` = '' OR c.name LIKE CONCAT('%',`name`,'%'))
								  AND c.company_id = companyId
        );
        
        SELECT totalRecords, firtsRecords, lastRecords;
        
END$$

/*====================== TEST ===================
@totalRecords INT(2)
@filteredRecords INT(2)

call GetCurrencies(1, '', 'nombre DESC', 1, 10, @totalRecords, @firtsRecords, @lastRecords);
select @totalRecords;
select @firtsRecords;
select @lastRecords;

*/
