DELIMITER $$
DROP procedure IF EXISTS `InsertOrderStatus`$$

CREATE PROCEDURE `InsertOrderStatus`(
                IN orderId INT,
                IN `status` VARCHAR(20),
				IN companyId INT,
                IN userName VARCHAR(100)
                )
BEGIN
		INSERT INTO `order_status`
		(
		`order_id`,
		`status`,
		`company_id`,
		`date`,
		`created_by`,
		`created_at`)
		VALUES
		(
		orderId,
		status,
		companyId,
		current_timestamp(),
		userName,
		current_timestamp());
END$$

/*====================== TEST ===================
CALL `InsertOrderStatus`(2, 'CONFIRMED', 1, 'TEST')
*/
