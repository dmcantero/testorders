DELIMITER $$
DROP procedure IF EXISTS `GetCatalogueClient`$$
CREATE PROCEDURE `GetCatalogueClient`(
				IN companyId INT(11),
                IN currency VARCHAR(5), 
				IN categoryId VARCHAR(20),
                IN `search` VARCHAR(100),
                IN `minPrice` DECIMAL(10, 2),
                IN `maxPrice` DECIMAL(10, 2),
                IN `offer` INT,
                IN `available` INT,
				IN orderBy VARCHAR(20),
                #para paginado
                IN pageIndex INT(2),
                IN pageSize INT(2),
                OUT totalRecords INT(2),
                OUT firtsRecords INT(2),
                OUT lastRecords INT(2))
BEGIN
		SET @row_number=0;
		#SET @exchange_rate = (SELECT IFNULL(exchange_rate, 1) FROM currencies WHERE iso_code = currency AND company_id = companyId);
        SET @defaultCurrencyAdmin = (SELECT IFNULL(`value`, 'ARS') FROM settings WHERE `key` = 'default_currency_admin' AND company_id = companyId);
        SET @defaultCurrency = (SELECT IFNULL(`value`, 'ARS') FROM settings WHERE `key` = 'default_currency' AND company_id = companyId);
        SET firtsRecords = ((pageIndex - 1) * pageSize + 1);
        SET lastRecords = ((pageIndex - 1) * pageSize + pageSize);

		SELECT * FROM 
        (
		SELECT 	
				prod.id,
                prod.name,
                prod.code,
                prod.description,
                
                CASE 
                WHEN @defaultCurrencyAdmin = @defaultCurrency THEN prod.sale_price
                WHEN @defaultCurrencyAdmin = 'ARS' AND @defaultCurrency = 'USD' THEN prod.sale_price / 845
                WHEN @defaultCurrencyAdmin = 'USD' AND @defaultCurrency = 'ARS' THEN prod.sale_price * 845
                END AS original_sale_price,
                
                (SELECT calculateSalePriceProduct(prod.sale_price, prod.discount_percentage, 'percentage', @defaultCurrency, prod.company_id)) AS sale_price,
                
                CASE 
                WHEN @defaultCurrencyAdmin = 'ARS' AND @defaultCurrency = 'USD' THEN prod.sale_price / (SELECT IFNULL(exchange_rate, 1) FROM currencies WHERE iso_code = currency AND company_id = companyId)
                WHEN @defaultCurrencyAdmin = 'USD' AND @defaultCurrency = 'ARS' THEN prod.sale_price * (SELECT IFNULL(exchange_rate, 1) FROM currencies WHERE iso_code = currency AND company_id = companyId)
                END AS exchange_original_sale_price,
                
				(SELECT calculateSalePriceProduct(prod.sale_price, prod.discount_percentage, 'percentage', currency, prod.company_id)) AS exchange_sale_price,
                
                prod.discount_percentage,
                ROUND((prod.sale_price_unit *  @exchange_rate), 4) AS sale_price_unit,
                (SELECT bulkPricesProduct(prod.id,  @defaultCurrency, currency, prod.company_id)) AS bulk_prices,
                prod.category_id,
                (SELECT new_name FROM uploads WHERE `type` = 'products' AND id_type = prod.id LIMIT 1) AS main_image, # prod.image_url AS main_image,
                IFNULL(prod.stock,0) AS stock,
                IFNULL(cat.name,'') AS category,
                IFNULL(prod.is_new, 0) AS is_new,
              	(@row_number := @row_number + 1) AS rn
        FROM products prod
        LEFT JOIN categories cat
        ON prod.category_id = cat.id
        AND prod.company_id = cat.company_id
        WHERE 
        ((categoryId = '') OR (find_in_set(prod.category_id, categoryId) > 0)) 
		AND (`search` IS NULL 
        OR `search` = '' 
        OR prod.name LIKE CONCAT('%',`search`,'%')
        OR prod.description LIKE CONCAT('%',`search`,'%'))
        AND prod.company_id = companyId
        AND prod.active = 1
        AND (`available` = 0 OR (`available` = 1 AND prod.stock > 0))
        ORDER BY 
         (CASE WHEN orderBy = 'A-Z' THEN prod.name END) ASC,
         (CASE WHEN orderBy = 'Z-A' THEN prod.name END) DESC,
         (CASE WHEN orderBy = 'min_price' THEN (SELECT calculateSalePriceProduct(prod.sale_price, prod.discount_percentage, 'percentage', currency, prod.company_id)) END) ASC,
         (CASE WHEN orderBy = 'max_price' THEN (SELECT calculateSalePriceProduct(prod.sale_price, prod.discount_percentage, 'percentage', currency, prod.company_id)) END) DESC
        ) Records
         WHERE Records.rn BETWEEN firtsRecords AND lastRecords
        ORDER BY Records.rn ASC;
        
       
            
        SET totalRecords = (SELECT COUNT(*) FROM products prod
							LEFT JOIN categories cat
							ON prod.category_id = cat.id
							AND prod.company_id = cat.company_id
							WHERE 
							((categoryId = '') OR (find_in_set(prod.category_id, categoryId) > 0))
							AND (`search` IS NULL 
							OR `search` = '' 
							OR prod.name LIKE CONCAT('%',`search`,'%')
							OR prod.description LIKE CONCAT('%',`search`,'%'))
							AND prod.company_id = companyId
							AND prod.active = 1
							AND (`available` = 0 OR (`available` = 1 AND prod.stock > 0)));
	
		SELECT totalRecords, firtsRecords, lastRecords;
END$$

/*====================== TEST ===================
@totalRecords INT(2)
@filteredRecords INT(2)

call GetCatalogueClient(1, 'ARS',  0, '', 0, 0, 0, 0, 'A-Z', 1, 10, @totalRecords, @firtsRecords, @lastRecords);
select @totalRecords;
select @filteredRecords;

*/
