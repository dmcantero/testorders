DELIMITER $$
DROP procedure IF EXISTS `GetOrdersClient`$$

CREATE PROCEDURE `GetOrdersClient`(
				IN companyId INT,
                IN clientId INT,
                IN dateFrom DATETIME,
                IN dateUntil DATETIME,
                IN orderStatus VARCHAR(20),
                IN currency VARCHAR(5),
                IN orderBy VARCHAR(20),
                #para paginado
                IN pageIndex INT(2),
                IN pageSize INT(2),
                OUT totalRecords INT(2),
                OUT firtsRecords INT(2),
                OUT lastRecords INT(2))
BEGIN
		SET @row_number=0;
		SET @defaultCurrency = (SELECT CASE WHEN currency = '' OR currency = 'ARS' THEN 'USD' ELSE 'ARS' END);
        #SET @exchange_rate = (SELECT exchange_rate FROM currencies WHERE iso_code = @defaultCurrency);
		SET firtsRecords = ((pageIndex - 1) * pageSize + 1);
        SET lastRecords = ((pageIndex - 1) * pageSize + pageSize);
        
        SELECT * FROM (
		SELECT *,
        (@row_number := @row_number + 1) AS rn
        FROM 
        (
		SELECT 
			ORD.Id,
            ORD.number,
			DATE_FORMAT(ORD.date, '%d/%m/%Y %H:%i') AS date,
            DATE_FORMAT(ORD.confirmation_date, '%d/%m/%Y %H:%i') AS confirmation_date,
			DATE_FORMAT(ORD.delivery_date, '%d/%m/%Y %H:%i') AS delivery_date,
			DATE_FORMAT(ORD.shipping_date, '%d/%m/%Y %H:%i') AS shipping_date,
            ORD.shipping_method,
            ORD.free_shipping,
            ORD.client_id,
            ORD.currency,
			ORD.exchange_rate,
			CASE WHEN Qstatus.status = 'ISSUED' THEN 'EMITIDO'
            WHEN Qstatus.status = 'CONFIRMED' THEN 'CONFIRMADO'
            WHEN Qstatus.status = 'CANCELLED' THEN 'CANCELADO'
            WHEN Qstatus.status = 'INPROGRESS' THEN 'EN PROGRESO' END AS `status_name`,
            Qstatus.status AS status,
            COUNT(DET.id) AS items,
             #TOTALES
            CONCAT('$', currency, ' ', FORMAT(IFNULL(CASE WHEN ORD.currency = currency THEN ORD.total ELSE (ORD.total / ORD.exchange_rate) END, 0), 2, 'es_AR')) AS total,
            CONCAT('$', currency, ' ', FORMAT(IFNULL(CASE WHEN ORD.currency = currency THEN ORD.total_discount ELSE (ORD.total_discount / ORD.exchange_rate) END, 0), 2, 'es_AR')) AS total_discount,
            CONCAT('$', currency, ' ', FORMAT(IFNULL(CASE WHEN ORD.currency = currency THEN ORD.total_net ELSE (ORD.total_net / ORD.exchange_rate) END, 0), 2, 'es_AR')) AS total_net,
			CONCAT('$', currency, ' ', FORMAT(IFNULL(CASE WHEN ORD.currency = currency THEN  SUM(DET.confirmed_subtotal) ELSE (SUM(DET.confirmed_subtotal) / ORD.exchange_rate) END, 0), 2, 'es_AR'))  AS total_confirmed
        FROM orders ORD
        LEFT JOIN order_details DET
        ON ORD.id = DET.order_id 
        AND ORD.company_id = DET.company_id
        JOIN (SELECT 
				order_id, 
                status,
                date,
                ROW_NUMBER() OVER(PARTITION BY order_id ORDER BY date DESC) AS `rn`
                FROM order_status) QStatus
        ON  ORD.id = QStatus.order_id
        AND QStatus.rn = 1
        WHERE (ORD.client_id = clientId)
        AND  (
				(dateFrom <> '' AND dateUntil = '' AND ORD.date >= dateFrom)
				OR (dateFrom = '' AND dateUntil <> '' AND ORD.date <= dateUntil)
                OR (dateFrom <> '' AND dateUntil <> '' AND ORD.date BETWEEN dateFrom AND dateUntil)
                OR (dateFrom = '' AND dateUntil = '')
                )
		AND (orderStatus = '' OR QStatus.status = orderStatus)
        AND ORD.company_id = companyId
        GROUP BY ORD.id
        ORDER BY 
         (CASE WHEN orderBy = 'date ASC' THEN ORD.date END) ASC,
         (CASE WHEN orderBy = 'date DESC'  OR orderBy = '' THEN ORD.date END) DESC
        ) RecordsOrdered 
        ) RecordsNumbered
        WHERE RecordsNumbered.rn BETWEEN firtsRecords AND lastRecords;
        
        /**
        SET totalRecords = (
							SELECT COUNT(*) 
							FROM orders ORD
							JOIN clients CLI
							ON ORD.client_id = CLI.id
							AND ORD.company_id = CLI.company_id
							JOIN (
                            SELECT 
								order_id, 
								status,
								date,
								ROW_NUMBER() OVER(PARTITION BY order_id ORDER BY date DESC) AS `row_number`
								FROM order_status) QStatus
							 ON QStatus.order_id = ORD.id
							AND QStatus.row_number = 1
							WHERE (ORD.client_id = clientId)
							AND  (
									(dateFrom IS NOT NULL AND dateUntil = '' AND ORD.date >= dateFrom)
									OR (dateFrom = '' AND dateUntil IS NOT NULL AND ORD.date <= dateUntil)
									OR (dateFrom IS NOT NULL AND dateUntil IS NOT NULL AND ORD.date BETWEEN dateFrom AND dateUntil)
									OR (dateFrom = '' AND dateUntil = '')
									)
							AND (orderStatus = '' OR QStatus.status = orderStatus)
							AND ORD.company_id = companyId
							);
        
        SELECT totalRecords, firtsRecords, lastRecords;
        */
END$$

/*====================== TEST ===================
@totalRecords INT(2)
@filteredRecords INT(2)

call GetOrdersClient(1, 1, '', '', '', 'USD', 'date DESC', 1, 50, @totalRecords, @firtsRecords, @lastRecords);
select @totalRecords;
select @firtsRecords;
select @lastRecords;

*/
