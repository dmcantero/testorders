DELIMITER $$
DROP procedure IF EXISTS `GetProducts`$$
CREATE PROCEDURE `GetProducts`(
				IN companyId INT(11),
				IN categoryId INT(11),
                IN supplierId INT(11),
                IN brandId INT(11),
                IN `name` VARCHAR(200),
                IN `code` VARCHAR(100),
				IN orderBy VARCHAR(20),
                #para paginado
                IN pageIndex INT(2),
                IN pageSize INT(2),
                OUT totalRecords INT(2),
                OUT firtsRecords INT(2),
                OUT lastRecords INT(2))
BEGIN
		SET @row_number=0;
        
        SET firtsRecords = (
        (pageIndex - 1) * pageSize + 1
        );
        SET lastRecords = (
        (pageIndex - 1) * pageSize + pageSize
        );

		SELECT * FROM 
        (
		SELECT 	
				prod.id AS Id,
                prod.name,
                IF(prod.active = '1', TRUE, FALSE) AS active,
                prod.category_id,
                prod.supplier_id,
                prod.code,
                prod.description,
				(SELECT new_name FROM uploads WHERE `type` = 'products' AND id_type = prod.id LIMIT 1) AS imageurl, # prod.image_url AS main_image,
                IFNULL(prod.purchase_price,0) AS purchase_price,
                IFNULL(prod.stock,0) AS stock,
                IFNULL(prod.min_stock,0) AS min_stock,
                IFNULL(cat.name,'') AS category,
                IFNULL(sup.name,'') AS supplier,
              	(@row_number := @row_number + 1) AS rn
        FROM products prod
        LEFT JOIN categories cat
        ON prod.category_id = cat.id
        AND prod.company_id = cat.company_id
        LEFT JOIN suppliers sup
        ON prod.supplier_id = sup.id
        AND prod.company_id = sup.company_id
        WHERE 
        ((categoryId = 0) OR (categoryId <> 0 AND prod.category_id IS NOT NULL AND prod.category_id = categoryId)) 
        AND ((supplierId = 0) OR (supplierId <> 0 AND prod.supplier_id IS NOT NULL AND prod.supplier_id = supplierId)) 
        AND ((brandId = 0) OR (brandId <> 0 AND prod.brand_id IS NOT NULL AND prod.brand_id = brandId)) 
		AND (`name` IS NULL 
        OR `name` = '' 
        OR prod.name LIKE CONCAT('%',`name`,'%'))
        AND (`code` IS NULL 
        OR `code` = '' 
        OR prod.code LIKE CONCAT('%',`code`,'%'))
        AND prod.company_id = companyId
        ORDER BY 
         (CASE WHEN orderBy = 'name asc' THEN prod.name END) ASC,
         (CASE WHEN orderBy = 'name desc' THEN prod.name END) DESC,
         (CASE WHEN orderBy = 'code asc' THEN prod.code END) ASC,
         (CASE WHEN orderBy = 'code desc' THEN prod.code END) DESC,
         (CASE WHEN orderBy = 'stock asc' THEN prod.stock END) ASC,
         (CASE WHEN orderBy = 'stock desc' THEN prod.stock END) DESC
        ) Records
         WHERE Records.rn BETWEEN firtsRecords AND lastRecords
        ORDER BY Records.rn ASC;
        
        #Properties
			SELECT 'imageurl' AS Identificador,
			'' AS Nombre,
			0 AS Filtrable,
			0 AS Ordenable,
			'' AS TipoFiltro,
			'' AS ValoresFiltro,
            'imgurl' AS TipoCeldaInput
		UNION ALL
			SELECT 'name' AS Identificador,
			'Nombre' AS Nombre,
			1 AS Filtrable,
			1 AS Ordenable,
			'textbox' AS TipoFiltro,
			'' AS ValoresFiltro,
            NULL AS TipoCeldaInput
		UNION ALL
			SELECT 'code' AS Identificador,
			'Codigo' AS Nombre,
			1 AS Filtrable,
			1 AS Ordenable,
			'textbox' AS TipoFiltro,
			'' AS ValoresFiltro,
            NULL AS TipoCeldaInput
		UNION ALL
			SELECT 'purchase_price' AS Identificador,
			'Costo' AS Nombre,
			0 AS Filtrable,
			0 AS Ordenable,
			'' AS TipoFiltro,
			'' AS ValoresFiltro,
            NULL AS TipoCeldaInput
		UNION ALL
			SELECT 'stock' AS Identificador,
			'Stock' AS Nombre,
			0 AS Filtrable,
			1 AS Ordenable,
			'' AS TipoFiltro,
			'' AS ValoresFiltro,
            NULL AS TipoCeldaInput
		UNION ALL
			SELECT 'category' AS Identificador,
			'Categoria' AS Nombre,
			0 AS Filtrable,
			0 AS Ordenable,
			'' AS TipoFiltro,
			'' AS ValoresFiltro,
            NULL AS TipoCeldaInput
		UNION ALL
			SELECT 'active' AS Identificador,
			'Activo' AS Nombre,
			0 AS Filtrable,
			0 AS Ordenable,
			'' AS TipoFiltro,
			'' AS ValoresFiltro,
            'checkbox' AS TipoCeldaInput;
            
        SET totalRecords = (SELECT COUNT(*) FROM products prod
							WHERE 
							((categoryId = 0) OR (categoryId <> 0 AND prod.category_id IS NOT NULL AND prod.category_id = categoryId)) 
							AND ((categoryId = 0) OR (categoryId <> 0 AND prod.category_id IS NOT NULL AND prod.category_id = categoryId)) 
							AND ((supplierId = 0) OR (supplierId <> 0 AND prod.supplier_id IS NOT NULL AND prod.supplier_id = supplierId)) 
							AND ((brandId = 0) OR (brandId <> 0 AND prod.brand_id IS NOT NULL AND prod.brand_id = brandId)) 
                            AND (`name` IS NULL 
							OR `name` = '' 
							OR prod.name LIKE CONCAT('%',`name`,'%'))
							AND (`code` IS NULL 
							OR `code` = '' 
							OR prod.code LIKE CONCAT('%',`code`,'%'))
							AND prod.company_id = companyId);
	
		SELECT totalRecords, firtsRecords, lastRecords;
END$$

/*====================== TEST ===================
@totalRecords INT(2)
@filteredRecords INT(2)

call GetProducts(1, 0, 0, 0, '', '', 'name ASC', 1, 10, @totalRecords, @firtsRecords, @lastRecords);
select @totalRecords;
select @filteredRecords;

*/
