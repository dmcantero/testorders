DELIMITER $$
DROP procedure IF EXISTS `GetClients`$$
CREATE PROCEDURE `GetClients`(
				IN companyId INT(11),
                IN username VARCHAR(30),
                IN businessName VARCHAR(100),
                IN email VARCHAR(100),
                IN contactPerson VARCHAR(100),
				IN orderBy VARCHAR(20),
                #para paginado
                IN pageIndex INT,
                IN pageSize INT,
                OUT totalRecords INT,
                OUT firtsRecords INT,
                OUT lastRecords INT)
BEGIN
		SET @row_number=0;
        
        SET firtsRecords = (
        (pageIndex - 1) * pageSize + 1
        );
        SET lastRecords = (
        (pageIndex - 1) * pageSize + pageSize
        );

		SELECT * FROM 
        (
		SELECT 	
				cl.id AS Id,
                cl.user_id AS user_id,
                us.username,
                IFNULL(cl.business_name,'-') AS business_name,
                us.created_at AS register_date,
                lastLoginUser(us.id, us.company_id) AS last_login,
                IFNULL(cl.contact_person, '-') AS contact_person,
                ci.id AS city_id,
                IFNULL(ci.name, '-') AS city,
                ci.province_id AS province_id,
                IFNULL(prov.name, '-') AS province,
                cl.email,
                IFNULL(cl.phone_number,'-') AS phone_number,
                countOrdersClient(cl.id, cl.company_id) AS orders,
                0 AS total_spend,
              	(@row_number := @row_number + 1) AS rn
        FROM clients cl
        INNER JOIN users us
        ON us.id = cl.user_id
        AND us.company_id = cl.company_id
        LEFT JOIN cities ci
        ON cl.city_id = ci.id
        LEFT JOIN provinces prov
        ON ci.province_id = prov.id
        WHERE 
        (businessName IS NULL 
        OR businessName = '' 
        OR cl.business_name LIKE CONCAT('%',businessName,'%'))
		AND  (email IS NULL 
        OR email = '' 
        OR cl.email LIKE CONCAT('%',email,'%'))
        AND  (contactPerson IS NULL 
        OR contactPerson = '' 
        OR cl.contact_person LIKE CONCAT('%',contactPerson,'%'))
		AND  (username IS NULL 
        OR username = '' 
        OR us.username LIKE CONCAT('%',username,'%'))
        AND cl.company_id = companyId
        ORDER BY 
         (CASE WHEN orderBy = 'business_name asc' 	THEN cl.business_name END) ASC,
         (CASE WHEN orderBy = 'business_name desc' 	THEN cl.business_name END) DESC,
         (CASE WHEN orderBy = 'contact_person asc' 	THEN cl.contact_person END) ASC,
         (CASE WHEN orderBy = 'contact_person desc' THEN cl.contact_person END) DESC,
         (CASE WHEN orderBy = 'register_date asc' 	THEN us.created_at END) ASC,
         (CASE WHEN orderBy = 'register_date desc' 	THEN us.created_at END) DESC,
         (CASE WHEN orderBy = 'last_active asc' 	THEN us.last_active END) ASC,
         (CASE WHEN orderBy = 'last_active desc' 	THEN us.last_active END) DESC,
		 (CASE WHEN orderBy = 'province_id asc' 	THEN ci.province_id END) ASC,
         (CASE WHEN orderBy = 'province_id desc' 	THEN ci.province_id END) DESC,
         (CASE WHEN orderBy = 'city_id asc' 		THEN ci.id END) ASC,
         (CASE WHEN orderBy = 'city_id desc' 		THEN ci.id END) DESC
		 #(CASE WHEN orderBy = 'orders asc' 			THEN COUNT(ord.id) END) ASC,
         #(CASE WHEN orderBy = 'orders desc' 		THEN COUNT(ord.id) END) DESC
        ) Records
         WHERE Records.rn BETWEEN firtsRecords AND lastRecords
        ORDER BY Records.rn ASC;
        
        #Properties
			SELECT 'business_name' AS Identificador,
			'Nombre comercial' AS Nombre,
			1 AS Filtrable,
			1 AS Ordenable,
			'textbox' AS TipoFiltro,
			'' AS ValoresFiltro,
            NULL AS TipoCeldaInput
		UNION ALL
			SELECT 'contact_person' AS Identificador,
			'Persona contacto' AS Nombre,
			1 AS Filtrable,
			1 AS Ordenable,
			'textbox' AS TipoFiltro,
			'' AS ValoresFiltro,
            NULL AS TipoCeldaInput
		UNION ALL
			SELECT 'username' AS Identificador,
			'Usuario' AS Nombre,
			1 AS Filtrable,
			0 AS Ordenable,
			'' AS TipoFiltro,
			'' AS ValoresFiltro,
            NULL AS TipoCeldaInput
		UNION ALL
			SELECT 'register_date' AS Identificador,
			'Fecha registro' AS Nombre,
			0 AS Filtrable,
			1 AS Ordenable,
			'' AS TipoFiltro,
			'' AS ValoresFiltro,
            NULL AS TipoCeldaInput
		UNION ALL
			SELECT 'last_login' AS Identificador,
			'Ult. ingreso' AS Nombre,
			0 AS Filtrable,
			1 AS Ordenable,
			'' AS TipoFiltro,
			'' AS ValoresFiltro,
            NULL AS TipoCeldaInput
		UNION ALL
			SELECT 'email' AS Identificador,
			'Email' AS Nombre,
			1 AS Filtrable,
			0 AS Ordenable,
			'textbox' AS TipoFiltro,
			'' AS ValoresFiltro,
            NULL AS TipoCeldaInput
		UNION ALL
			SELECT 'phone_number' AS Identificador,
			'Teléfono' AS Nombre,
			0 AS Filtrable,
			0 AS Ordenable,
			'' AS TipoFiltro,
			'' AS ValoresFiltro,
            NULL AS TipoCeldaInput
		UNION ALL
			SELECT 'province' AS Identificador,
			'Provincia' AS Nombre,
			0 AS Filtrable,
			1 AS Ordenable,
			'' AS TipoFiltro,
			'' AS ValoresFiltro,
            NULL AS TipoCeldaInput
		UNION ALL
			SELECT 'city' AS Identificador,
			'Ciudad' AS Nombre,
			0 AS Filtrable,
			1 AS Ordenable,
			'' AS TipoFiltro,
			'' AS ValoresFiltro,
            NULL AS TipoCeldaInput
		UNION ALL
			SELECT 'orders' AS Identificador,
			'Ordenes' AS Nombre,
			0 AS Filtrable,
			0 AS Ordenable,
			'' AS TipoFiltro,
			'' AS ValoresFiltro,
            NULL AS TipoCeldaInput;
            
        SET totalRecords = (SELECT COUNT(*) FROM clients cl
							INNER JOIN users us
							ON cl.user_id = us.id
							AND cl.company_id = us.company_id
							LEFT JOIN cities ci
							ON cl.city_id = ci.id
							LEFT JOIN provinces prov
							ON ci.province_id = prov.id
							LEFT JOIN orders ord
							ON cl.id = ord.client_id
							AND cl.company_id = ord.company_id
							WHERE 
							(businessName IS NULL 
							OR businessName = '' 
							OR cl.business_name LIKE CONCAT('%',businessName,'%'))
							AND  (email IS NULL 
							OR email = '' 
							OR cl.email LIKE CONCAT('%',email,'%'))
							AND  (contactPerson IS NULL 
							OR contactPerson = '' 
							OR cl.contact_person LIKE CONCAT('%',contactPerson,'%'))
							AND  (username IS NULL 
							OR username = '' 
							OR us.username LIKE CONCAT('%',username,'%'))
							AND cl.company_id = companyId);
	
		SELECT totalRecords, firtsRecords, lastRecords;
END$$

/*====================== TEST ===================
@totalRecords INT(2)
@filteredRecords INT(2)

call GetClients(1,'','','','','name ASC', 1, 10, @totalRecords, @firtsRecords, @lastRecords);
select @totalRecords;
select @filteredRecords;

*/
