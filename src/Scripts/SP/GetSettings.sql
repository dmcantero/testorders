DELIMITER $$
DROP procedure IF EXISTS `GetSettings`$$

CREATE PROCEDURE `GetSettings`(IN companyId INT, IN `group` VARCHAR(100))
BEGIN
       SELECT *
       FROM settings S
       WHERE company_id = companyId
       AND (`group` IS NULL OR `group` = '' OR S.group = `group`)
       ORDER BY S.group;
END$$

/*====================== TEST ===================

call GetSettings(1,'');

*/
