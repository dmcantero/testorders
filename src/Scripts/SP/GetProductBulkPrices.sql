DELIMITER $$
DROP PROCEDURE IF EXISTS `GetProductBulkPrices`$$
CREATE PROCEDURE `GetProductBulkPrices`(
				IN companyId INT(11),
				IN productId INT(11))
BEGIN
		SET @row_number=0;
        
		SELECT 	bp.id,
				bp.product_id,
                ROUND(bp.max_bulk,0) AS max_bulk,
                bp.sale_price,
                bp.unit_sale_price,
                bp.discount,
				IFNULL(ROUND(bp.sale_price - ((bp.sale_price * bp.discount) / 100), 4), bp.sale_price) AS sale_price_discount,
                bp.active,
				bp.allow_select_qty,
                IFNULL(CONCAT(bp.valid_from, ' - ', bp.valid_until), 'Indefinido') AS period,
              	(@row_number := @row_number + 1) AS rn
        FROM bulk_pricing bp
        WHERE bp.product_id = productId
        AND bp.company_id = companyId
        ORDER BY bp.max_bulk;
END$$

/*====================== TEST ===================
CALL GetProductBulkPrices(1, 1);
*/
