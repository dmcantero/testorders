DELIMITER $$
DROP procedure IF EXISTS `GetOrdersAdmin`$$

CREATE PROCEDURE `GetOrdersAdmin`(
				IN orderId INT,
				IN companyId INT,
                IN `number` NVARCHAR(100),
                IN clientId INT,
                IN dateFrom DATETIME,
                IN dateUntil DATETIME,
                IN orderStatus VARCHAR(20),
                IN currencyQuery VARCHAR(5),
                IN orderBy VARCHAR(20),
                #para paginado
                IN pageIndex INT(2),
                IN pageSize INT(2),
                OUT totalRecords INT(2),
                OUT firtsRecords INT(2),
                OUT lastRecords INT(2))
BEGIN
		SET @row_number=0;
        #SET @exchange_rate = (SELECT exchange_rate FROM currencies WHERE iso_code = @defaultCurrency);
		SET firtsRecords = ((pageIndex - 1) * pageSize + 1);
        SET lastRecords = ((pageIndex - 1) * pageSize + pageSize);
        
        SELECT * FROM (
		SELECT *,
        (@row_number := @row_number + 1) AS rn
        FROM 
        (
		SELECT 
			ORD.Id,
            ORD.number,
			DATE_FORMAT(ORD.date, '%d/%m/%Y %H:%i') AS date,
            DATE_FORMAT(ORD.confirmation_date, '%Y-%m-%d') AS confirmation_date,
            DATE_FORMAT(ORD.confirmation_date, '%d/%m/%Y %H:%i') AS confirmation_date_format,
            DATE_FORMAT(ORD.delivery_date, '%Y-%m-%d') AS delivery_date,
			DATE_FORMAT(ORD.delivery_date, '%d/%m/%Y %H:%i') AS delivery_date_format,
			DATE_FORMAT(ORD.shipping_date, '%Y-%m-%d') AS shipping_date,
            DATE_FORMAT(ORD.shipping_date, '%d/%m/%Y %H:%i') AS shipping_date_format,
            ORD.shipping_method,
            CASE WHEN shipping_method = 'delivery' THEN 'Envío a domicilio'
            WHEN shipping_method = 'pickup' THEN 'Retiro en depósito'
            END AS shipping_method_name,
            ORD.free_shipping,
            ORD.client_id,
            ORD.user_id,
            ORD.currency,
            currencyQuery,
			ORD.exchange_rate,
            IFNULL(ORD.discount_percentage,0) AS discount_percentage,
            ORD.notes,
            IFNULL(CASE WHEN ORD.client_id IS NOT NULL AND (CLI.business_name IS NULL OR CLI.business_name = '') THEN CLI.contact_person
            WHEN ORD.client_id IS NOT NULL AND (CLI.contact_person IS NULL OR CLI.contact_person = '') THEN CLI.business_name 
            ELSE ORD.client_fullname END, 'N/E') AS client_fullname,
            CLI.email AS client_email,
			CASE WHEN Qstatus.status = 'ISSUED' THEN 'EMITIDO'
            WHEN Qstatus.status = 'CONFIRMED' THEN 'CONFIRMADO'
            WHEN Qstatus.status = 'CANCELLED' THEN 'CANCELADO'
            WHEN Qstatus.status = 'INPROGRESS' THEN 'EN PROGRESO' END AS `status_name`,
            Qstatus.status AS status,
            IFNULL(ORD.sale_condition_id,0) AS sale_condition_id,
            IFNULL(ORD.credit_term_id,0) AS credit_term_id,
            IFNULL(ORD.estimate_id,0) AS estimate_id,
            COUNT(DET.id) AS items,
            #TOTALES
            CASE 
            WHEN currencyQuery = 'USD' AND ORD.currency = 'ARS' THEN FORMAT((ORD.total / ORD.exchange_rate), 2)
			WHEN currencyQuery = 'ARS' AND ORD.currency = 'USD' THEN FORMAT((ORD.total * ORD.exchange_rate), 2)
            ELSE FORMAT(ORD.total, 2)
            END AS total,
            
            ORD.total AS original_total,
            
            CASE 
            WHEN currencyQuery = 'USD' AND ORD.currency = 'ARS' THEN FORMAT((ORD.total_discount / ORD.exchange_rate), 2)
			WHEN currencyQuery = 'ARS' AND ORD.currency = 'USD' THEN FORMAT((ORD.total_discount * ORD.exchange_rate), 2)
            ELSE FORMAT(ORD.total_discount, 2)
            END AS total_discount,
            
            ORD.total_discount AS original_total_discount,
            
            CASE 
            WHEN currencyQuery = 'USD' AND ORD.currency = 'ARS' THEN FORMAT((ORD.total_net / ORD.exchange_rate), 2)
			WHEN currencyQuery = 'ARS' AND ORD.currency = 'USD' THEN FORMAT((ORD.total_net * ORD.exchange_rate), 2)
            ELSE FORMAT(ORD.total_net, 2)
            END AS total_net,
            
            ORD.total_net AS original_total_net,
            
            CASE 
            WHEN currencyQuery = 'USD' AND ORD.currency = 'ARS' THEN FORMAT((SUM(DET.confirmed_subtotal) / ORD.exchange_rate), 2)
			WHEN currencyQuery = 'ARS' AND ORD.currency = 'USD' THEN FORMAT((SUM(DET.confirmed_subtotal) * ORD.exchange_rate), 2)
            ELSE FORMAT(SUM(DET.confirmed_subtotal), 2)
            END AS total_confirmed,
            
            SUM(DET.confirmed_subtotal) AS original_total_confirmed
            /*
            CONCAT('$', currency, ' ', FORMAT(IFNULL(CASE WHEN ORD.currency = currency THEN ORD.total ELSE (ORD.total / ORD.exchange_rate) END, 0), 2, 'es_AR')) AS total,
            CONCAT('$', currency, ' ', FORMAT(IFNULL(CASE WHEN ORD.currency = currency THEN ORD.total_discount ELSE (ORD.total_discount / ORD.exchange_rate) END, 0), 2, 'es_AR')) AS total_discount,
            CONCAT('$', currency, ' ', FORMAT(IFNULL(CASE WHEN ORD.currency = currency THEN ORD.total_net ELSE (ORD.total_net / ORD.exchange_rate) END, 0), 2, 'es_AR')) AS total_net,
			CONCAT('$', currency, ' ', FORMAT(IFNULL(CASE WHEN ORD.currency = currency THEN  SUM(DET.confirmed_subtotal) ELSE (SUM(DET.confirmed_subtotal) / ORD.exchange_rate) END, 0), 2, 'es_AR'))  AS total_confirmed
            */
        FROM orders ORD
        LEFT JOIN order_details DET
        ON ORD.id = DET.order_id 
        AND ORD.company_id = DET.company_id
        JOIN clients CLI
        ON ORD.client_id = CLI.id
        AND ORD.company_id = CLI.company_id
        JOIN (SELECT 
				order_id, 
                status,
                date,
                ROW_NUMBER() OVER(PARTITION BY order_id ORDER BY date DESC) AS `rn`
                FROM order_status) QStatus
        ON  ORD.id = QStatus.order_id
        AND QStatus.rn = 1
        WHERE (clientId IS NULL OR clientId = 0 OR ORD.client_id = clientId)
        AND  (
				(dateFrom <> '' AND dateUntil = '' AND ORD.date >= dateFrom)
				OR (dateFrom = '' AND dateUntil <> '' AND ORD.date <= dateUntil)
                OR (dateFrom <> '' AND dateUntil <> '' AND ORD.date BETWEEN dateFrom AND dateUntil)
                OR (dateFrom = '' AND dateUntil = '')
                )
		AND (orderStatus = '' OR QStatus.status = orderStatus)
        AND (orderId = 0 OR ORD.id = orderId)
        AND (`number` = '' OR `number` IS NULL OR ORD.number LIKE CONCAT('%', `number` , '%'))
        AND ORD.company_id = companyId
        GROUP BY ORD.id
        ORDER BY 
         (CASE WHEN orderBy = 'date ASC' THEN ORD.date END) ASC,
         (CASE WHEN orderBy = 'date DESC'  OR orderBy = '' THEN ORD.date END) DESC,
         (CASE WHEN orderBy = 'number ASC' THEN ORD.number END) ASC,
         (CASE WHEN orderBy = 'number DESC' THEN ORD.number END) DESC
        ) RecordsOrdered 
        ) RecordsNumbered
        WHERE RecordsNumbered.rn BETWEEN firtsRecords AND lastRecords;
        
        
        #Properties
         SELECT 'number' AS Identificador,
			'Numero' AS Nombre,
			1 AS Filtrable,
			1 AS Ordenable,
			'textbox' AS TipoFiltro,
			'' AS ValoresFiltro,
            NULL AS TipoCeldaInput
		UNION ALL
        SELECT 'date' AS Identificador,
			'Fecha' AS Nombre,
			0 AS Filtrable,
			1 AS Ordenable,
			'' AS TipoFiltro,
			'' AS ValoresFiltro,
            NULL AS TipoCeldaInput
		UNION ALL
			SELECT 'client_fullname' AS Identificador,
			'Cliente' AS Nombre,
			1 AS Filtrable,
			0 AS Ordenable,
			'textbox' AS TipoFiltro,
			'' AS ValoresFiltro,
            NULL AS TipoCeldaInput
		UNION ALL
			SELECT 'status_name' AS Identificador,
			'Estado' AS Nombre,
			0 AS Filtrable,
			0 AS Ordenable,
			'select' AS TipoFiltro,
			'' AS ValoresFiltro,
            NULL AS TipoCeldaInput
		UNION ALL
			SELECT 'items' AS Identificador,
			'Items' AS Nombre,
			0 AS Filtrable,
			0 AS Ordenable,
			'' AS TipoFiltro,
			'' AS ValoresFiltro,
			NULL AS TipoCeldaInput
		UNION ALL
			SELECT 'total' AS Identificador,
			'Total' AS Nombre,
			0 AS Filtrable,
			0 AS Ordenable,
			'' AS TipoFiltro,
			'' AS ValoresFiltro,
			NULL AS TipoCeldaInput
		UNION ALL
			SELECT 'total_discount' AS Identificador,
			'Total descuento' AS Nombre,
			0 AS Filtrable,
			0 AS Ordenable,
			'' AS TipoFiltro,
			'' AS ValoresFiltro,
			NULL AS TipoCeldaInput
		UNION ALL
			SELECT 'total_net' AS Identificador,
			'Total neto' AS Nombre,
			0 AS Filtrable,
			0 AS Ordenable,
			'' AS TipoFiltro,
			'' AS ValoresFiltro,
			NULL AS TipoCeldaInput
		UNION ALL
			SELECT 'total_confirmed' AS Identificador,
			'Total confirmado' AS Nombre,
			0 AS Filtrable,
			0 AS Ordenable,
			'' AS TipoFiltro,
			'' AS ValoresFiltro,
			NULL AS TipoCeldaInput;
        
        SET totalRecords = (
							SELECT COUNT(*) 
							FROM orders ORD
							JOIN clients CLI
							ON ORD.client_id = CLI.id
							AND ORD.company_id = CLI.company_id
							JOIN (
                            SELECT 
								order_id, 
								status,
								date,
								ROW_NUMBER() OVER(PARTITION BY order_id) AS `row_number`
								FROM order_status) QStatus
							 ON QStatus.order_id = ORD.id
							AND QStatus.row_number = 1
							WHERE (clientId IS NULL OR clientId = 0 OR ORD.client_id = clientId)
							AND  (
									(dateFrom IS NOT NULL AND dateUntil = '' AND ORD.date >= dateFrom)
									OR (dateFrom = '' AND dateUntil IS NOT NULL AND ORD.date <= dateUntil)
									OR (dateFrom IS NOT NULL AND dateUntil IS NOT NULL AND ORD.date BETWEEN dateFrom AND dateUntil)
									OR (dateFrom = '' AND dateUntil = '')
									)
							AND (orderStatus = '' OR QStatus.status = orderStatus)
							AND ORD.company_id = companyId
							);
        
        SELECT totalRecords, firtsRecords, lastRecords;
        
END$$

/*====================== TEST ===================
@totalRecords INT(2)
@filteredRecords INT(2)

call GetOrdersAdmin(2,1,'', 0, '', '', '', 'USD', 'date DESC', 1, 50, @totalRecords, @firtsRecords, @lastRecords);
select @totalRecords;
select @firtsRecords;
select @lastRecords;

*/
