DELIMITER $$
DROP PROCEDURE IF EXISTS `GetFeatures`$$
CREATE PROCEDURE `GetFeatures`(
				IN companyId INT(11),
                IN orderBy VARCHAR(20),
                #para paginado
                IN pageIndex INT(2),
                IN pageSize INT(2),
                OUT totalRecords INT(2),
                OUT firtsRecords INT(2),
                OUT lastRecords INT(2))
BEGIN
		SET @row_number=0;
        
        SET @row_number=0;
        
        SET firtsRecords = (
        (pageIndex - 1) * pageSize + 1
        );
        SET lastRecords = (
        (pageIndex - 1) * pageSize + pageSize
        );
        
		SELECT * FROM 
        (
			SELECT 	
					f.id AS Id,
					f.name,
					IFNULL((SELECT GROUP_CONCAT(fv.value SEPARATOR ', ') FROM features_values fv WHERE fv.feature_id = f.id GROUP BY fv.feature_id), '') AS `values`,
					(@row_number := @row_number + 1) AS rn
			FROM features f
			WHERE f.company_id = companyId
			AND f.deleted_at IS NULL
			ORDER BY f.name
           ) Records
         WHERE Records.rn BETWEEN firtsRecords AND lastRecords
         ORDER BY Records.rn ASC;
        
         #Properties
			SELECT 'name' AS Identificador,
			'Nombre' AS Nombre,
			1 AS Filtrable,
			1 AS Ordenable,
			'textbox' AS TipoFiltro,
			'' AS ValoresFiltro,
            NULL AS TipoCeldaInput
		UNION ALL
			SELECT 'values' AS Identificador,
			'Valores' AS Nombre,
			0 AS Filtrable,
			0 AS Ordenable,
			'' AS TipoFiltro,
			'' AS ValoresFiltro,
            NULL AS TipoCeldaInput;
            
        SET totalRecords = (SELECT COUNT(*)
				FROM features f
				WHERE f.company_id = companyId
				AND f.deleted_at IS NULL
				ORDER BY f.name);
	
		SELECT totalRecords, firtsRecords, lastRecords;
END$$

/*====================== TEST ===================
CALL GetFeatures(1,'name ASC', 1, 10, @totalRecords, @firtsRecords, @lastRecords);
*/
