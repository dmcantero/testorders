DELIMITER $$
DROP procedure IF EXISTS `UpdateOrder`$$

CREATE PROCEDURE `UpdateOrder`(
				IN orderId INT,
                IN confirmationDate DATETIME,
                IN deliveryDate DATETIME,
                IN shippingDate DATETIME,
                IN shippingMethod VARCHAR(10),
                IN freeShipping VARCHAR(1),
                IN clientFullName VARCHAR(100),
                IN estimateId INT,
                IN discountPercentage DECIMAL(10,4),
                IN saleConditionId INT,
                IN creditTermId INT,
                IN notes VARCHAR(200),
                IN orderStatus VARCHAR(20),
				IN companyId INT,
                IN userName VARCHAR(100)
                )
BEGIN
         
        UPDATE `orders`
		SET
		`confirmation_date` = confirmationDate,
		`delivery_date` = deliveryDate,
		`shipping_date` = shippingDate,
		`shipping_method` = shippingMethod,
		`free_shipping` = freeShipping,
		`client_fullname` = clientFullname,
		`estimate_id` = (SELECT CASE WHEN estimateId = 0 THEN NULL ELSE estimateId END),
		`discount_percentage` = discountPercentage,
		`sale_condition_id` = (SELECT CASE WHEN saleConditionId = 0 THEN NULL ELSE saleConditionId END),
		`credit_term_id` = (SELECT CASE WHEN creditTermId = 0 THEN NULL ELSE creditTermId END),
		`notes` = notes,
		`updated_by` = userName,
		`updated_at` = current_timestamp()
		WHERE id = orderId AND company_id = companyId;
		
        SET @currentStatus = (SELECT `status` FROM order_status WHERE order_id = orderId AND company_id = companyId ORDER BY `date` DESC LIMIT 1);
        
        IF @currentStatus <> orderStatus THEN
			CALL InsertOrderStatus(orderId, orderStatus, companyId, userName);
        END IF;
		
         SELECT 
         `number`, 
         id,
         orderStatus,
         @currentStatus
         FROM orders WHERE id = orderId AND company_id = companyId;
END$$

/*====================== TEST ===================
CALL `UpdateOrder`(
                IN clientId INT,
                IN userId INT,
                IN confirmationDate DATETIME,
                IN deliveryDate DATETIME,
                IN shippingDate DATETIME,
                IN shippingStatus VARCHAR(10),
                IN freeShipping VARCHAR(1),
                IN clientFullName VARCHAR(100),
                IN estimateId INT,
                IN discountPercentage DECIMAL(10,4),
                IN saleConditionId INT,
                IN creditTermId INT,
                IN notes VARCHAR(200),
				IN companyId INT,
                IN userName VARCHAR(100)
*/
