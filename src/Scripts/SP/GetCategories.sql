DELIMITER $$
DROP procedure IF EXISTS `GetCategories`$$

CREATE PROCEDURE `GetCategories`(
				IN companyId INT,
                IN search VARCHAR(100),
                IN orderBy VARCHAR(20),
                #para paginado
                IN pageIndex INT(2),
                IN pageSize INT(2),
                OUT totalRecords INT(2),
                OUT firtsRecords INT(2),
                OUT lastRecords INT(2))
BEGIN
		SET @row_number=0;
	
		SET firtsRecords = (
        (pageIndex - 1) * pageSize + 1
        );
        SET lastRecords = (
        (pageIndex - 1) * pageSize + pageSize
        );
        
		SELECT 
        Records.id AS Id,
        Records.name,
        Records.description,
        Records.rn 
        FROM 
        (
		SELECT 
			(@row_number := @row_number + 1) AS rn,
			c.*
        FROM categories c
        WHERE (search IS NULL OR search = '' OR c.name LIKE CONCAT('%',search,'%'))
        AND c.company_id = company_id
        AND c.deleted_at IS NULL
        ORDER BY 
         (CASE WHEN orderBy = 'name ASC' OR orderBy = '' THEN c.name END) ASC,
         (CASE WHEN orderBy = 'name DESC' THEN c.name END) DESC
        ) Records 
        WHERE Records.rn BETWEEN firtsRecords AND lastRecords
        ORDER BY Records.rn ASC;
        
        
        #Properties
			SELECT 'name' AS Identificador,
			'Nombre' AS Nombre,
			1 AS Filtrable,
			1 AS Ordenable,
			'textbox' AS TipoFiltro,
			'' AS ValoresFiltro
		UNION ALL
			SELECT 'description' AS Identificador,
			'Descripcion' AS Nombre,
			0 AS Filtrable,
			0 AS Ordenable,
			'' AS TipoFiltro,
			'' AS ValoresFiltro;
        
        SET totalRecords = (
							SELECT COUNT(*) 
							FROM categories c
								  WHERE (search IS NULL OR search = '' OR c.name LIKE CONCAT('%',search,'%'))
								  AND c.company_id = companyId
                                  AND c.deleted_at IS NULL
        );
        
        SELECT totalRecords, firtsRecords, lastRecords;
        
END$$

/*====================== TEST ===================
@totalRecords INT(2)
@filteredRecords INT(2)

call BuscarCategorias(1, '', 'nombre DESC', 1, 10, @totalRecords, @firtsRecords, @lastRecords);
select @totalRecords;
select @firtsRecords;
select @lastRecords;

*/
