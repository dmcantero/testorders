DELIMITER $$
DROP procedure IF EXISTS `InsertOrder`$$

CREATE PROCEDURE `InsertOrder`(
                IN clientId INT,
                IN userId INT,
                IN confirmationDate DATETIME,
                IN deliveryDate DATETIME,
                IN shippingDate DATETIME,
                IN shippingMethod VARCHAR(10),
                IN freeShipping VARCHAR(1),
                IN clientFullName VARCHAR(100),
                IN estimateId INT,
                IN discountPercentage DECIMAL(10,4),
                IN saleConditionId INT,
                IN creditTermId INT,
                IN notes VARCHAR(200),
				IN companyId INT,
                IN userName VARCHAR(100)
                )
BEGIN
		 SET @defaultCurrency 	= (SELECT IFNULL(`value`, 'ARS') FROM settings WHERE `key` = 'default_currency' AND company_id = companyId);
         SET @exchangeRate 		= (SELECT IFNULL(exchange_rate, 1) FROM currencies WHERE iso_code = @defaultCurrency AND company_id = companyId);
         SET @newOrderNumber 	= (SELECT newOrderNumber(1));
         
         
         INSERT INTO `orders`
			(
			`number`,
			`client_id`,
			`user_id`,
			`date`,
			`confirmation_date`,
			`delivery_date`,
			`shipping_date`,
			`shipping_method`,
			`free_shipping`,
			`client_fullname`,
			`estimate_id`,
			`currency`,
			`exchange_rate`,
			`discount_percentage`,
			`sale_condition_id`,
			`credit_term_id`,
			`notes`,
			`company_id`,
			`created_by`,
			`created_at`)
			VALUES
			(
            @newOrderNumber,
			clientId,
			userId,
			current_timestamp(),
			confirmationDate,
			deliveryDate,
			shippingDate,
			shippingMethod,
			freeShipping,
			clientFullname,
			(SELECT CASE WHEN estimateId = 0 THEN NULL ELSE estimateId END),
			@defaultCurrency,
			@exchangeRate,
			discountPercentage,
			(SELECT CASE WHEN saleConditionId = 0 THEN NULL ELSE saleConditionId END),
			(SELECT CASE WHEN creditTermId = 0 THEN NULL ELSE creditTermId END),
			notes,
			companyId,
			userName,
			current_timestamp()
            );
            
         SET @orderId = LAST_INSERT_ID();
         
         CALL InsertOrderStatus(@orderId, 'ISSUED', companyId, userName);
         
         SELECT 
         @newOrderNumber AS `number`,
         @orderId AS id;
		
END$$

/*====================== TEST ===================
CALL `InsertOrder`(
                IN clientId INT,
                IN userId INT,
                IN confirmationDate DATETIME,
                IN deliveryDate DATETIME,
                IN shippingDate DATETIME,
                IN shippingStatus VARCHAR(10),
                IN freeShipping VARCHAR(1),
                IN clientFullName VARCHAR(100),
                IN estimateId INT,
                IN discountPercentage DECIMAL(10,4),
                IN saleConditionId INT,
                IN creditTermId INT,
                IN notes VARCHAR(200),
				IN companyId INT,
                IN userName VARCHAR(100)
*/
