DELIMITER $$
DROP procedure IF EXISTS `GetCategoriesTreeView`$$
CREATE PROCEDURE `GetCategoriesTreeView`(IN companyId INT)
BEGIN
/*
	WITH RECURSIVE category_path (id, label, father_id, path) AS
	(
	  SELECT id, name AS label, father_id, name as path
		FROM categories
		WHERE (father_id IS NULL OR father_id = 0)
        AND company_id = companyId
	  UNION ALL
	  SELECT c.id, c.name AS label, c.father_id, CONCAT(cp.path, ' > ', c.name)
		FROM category_path AS cp
		JOIN categories AS c
		ON cp.id = c.father_id
        WHERE company_id = companyId
	)
	SELECT * FROM category_path
	ORDER BY path;
    */
    
     SELECT id, name AS label, father_id, name as path
		FROM categories
        WHERE company_id = 1
        ORDER BY label;

END$$

/*
============= TEST ============
CALL GetCategoriesTreeView(1);
*/