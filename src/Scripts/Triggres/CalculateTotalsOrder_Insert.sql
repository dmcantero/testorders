-- TRIGGER PARA UPDATE 
DELIMITER $$
DROP TRIGGER IF EXISTS `CalculateTotalsOrder_Insert` $$
DELIMITER // 
CREATE TRIGGER CalculateTotalsOrder_Insert
AFTER INSERT ON order_details 
FOR EACH ROW 
BEGIN

SET @DiscountPercentage = (SELECT IFNULL(discount_percentage, 0) FROM orders WHERE id = NEW.id AND company_id = NEW.company_id);
SET @Total = (SELECT IFNULL(SUM(subtotal),0) FROM order_details WHERE order_id = NEW.order_id AND company_id = NEW.company_id);
SET @TotalDiscount = IFNULL((@Total * @DiscountPercentage) / 100 , 0);
SET @TotalNet = IFNULL(@Total - @TotalDiscount, @Total);

UPDATE orders
SET total = @Total,
total_discount = @TotalDiscount,
total_taxes = 0,
total_net = @TotalNet
WHERE id = NEW.order_id AND company_id = NEW.company_id;

END//