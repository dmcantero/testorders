let mix = require("laravel-mix");

console.log("Packing admin js...");
//admin js
mix
  .js("src/js/admin/vendor.js", "dist/js/admin")
  .js("src/js/admin/categories/datatable.js", "dist/js/admin/categories")
  .js("src/js/admin/categories/form.js", "dist/js/admin/categories")
  .js("src/js/admin/products/datatable.js", "dist/js/admin/products")
  .js("src/js/admin/products/form.js", "dist/js/admin/products")
  .js("src/js/admin/clients/datatable.js", "dist/js/admin/clients")
  .js("src/js/admin/clients/form.js", "dist/js/admin/clients")
  .js("src/js/admin/features/datatable.js", "dist/js/admin/features")
  .js("src/js/admin/features/form.js", "dist/js/admin/features")
  .js("src/js/admin/users/datatable.js", "dist/js/admin/users")
  .js("src/js/admin/users/form.js", "dist/js/admin/users")
  .js("src/js/admin/currencies/datatable.js", "dist/js/admin/currencies")
  .js("src/js/admin/currencies/form.js", "dist/js/admin/currencies")
  .js("src/js/admin/settings/form.js", "dist/js/admin/settings")
  .js("src/js/admin/orders/datatable.js", "dist/js/admin/orders")
  .js("src/js/admin/orders/form.js", "dist/js/admin/orders")
  .vue()

//admin css
mix
  .css("src/css/admin/styles.css", "dist/css/admin")
  .options({
    fileLoaderDirs: {
      fonts: "dist/fonts",
      images: "dist/images",
    },
  })
  .css("src/css/admin/vendor.css", "dist/css/admin")
  .options({
    fileLoaderDirs: {
      fonts: "dist/fonts",
      images: "dist/images",
    },
  });
