let mix = require("laravel-mix");


require(`${__dirname}/webpack_admin.mix.js`);

//Utils and services
mix.js("src/js/util/shoppingcart.js", "dist/js/util");
//catalogue
mix.js("src/js/catalogue/main.js", "dist/js/catalogue");
mix.js("src/js/catalogue/orders.js", "dist/js/catalogue");
mix.js("src/js/catalogue/profile.js", "dist/js/catalogue");
//css
mix.css("src/css/common.css", "dist/css/common");

//Images
mix.copy(["src/img/undraw_profile.svg", "src/img/loading.gif", "src/img/not-image.svg"], "dist/images");

if (process.env.npm_config_foo) {
  console.log("project:" + process.env.npm_config_foo);
  require(`${__dirname}/webpack_${process.env.npm_config_foo}.mix.js`);
}


if (mix.inProduction()) {
  mix.version();
} else {
  mix.sourceMaps();
  mix.webpackConfig({ devtool: 'source-map' })
}
