import {createApp} from "vue";
import { Field, Form, ErrorMessage } from "vee-validate";
import "../js/util/veevalidate-config.js";
import vSelect from "vue-select";
import Toast from "vue-toastification";

import "../js/util/filters.js";

import DateTimePicker from "vue-vanilla-datetime-picker";
import DataGrid from "../components/Datagrid.vue";
import CustomSwitch from "../components/switch.vue";
import DialogConfirmation from "../components/dialogconfirmation.vue";
import GenericSelect2 from "../components/genericselect2.vue";
import GenericSelect from "../components/genericselect.vue";
import GenericMultiSelect from "../components/genericmultiselect.vue";
import Money from "../components/money.vue";

let app = createApp({});

app.use(Toast, {});
app.component("v-select", vSelect);
app.component("datagrid", DataGrid);
app.component("customswitch", CustomSwitch);
app.component("dialogconfirmation", DialogConfirmation);
app.component("genericselect2", GenericSelect2);
app.component("genericselect", GenericSelect);
app.component("genericmultiselect", GenericMultiSelect);
app.component("money", Money);
app.component("date-time-picker", DateTimePicker);

// app.mount('#app');