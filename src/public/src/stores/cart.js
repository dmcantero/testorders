import { piniaInstance } from "../js/global";
import { defineStore } from "pinia";
import { ref } from "vue";
import { postData } from "../js/util/fetchhandler";

export default defineStore("cart", () => {
  const items = ref([]);
  const totalItems = ref(0);
  const totalCost = ref(0);

  const getItems = async () => {
    let response = await services.shoppingcart.get();
    items.value = Object.values(response);
    totalItems.value = items.value.length;
    totalCost.value = items.value.reduce((accumulator, currentValue) => {
      return accumulator + currentValue.price * currentValue.qty;
    }, 0);
  };

  const addItem = async (data) => {
    let response = await services.shoppingcart.add(data);
    if (response.status) {
      getItems();
    }
  };

  const updateItem = async (rowid, data) => {
    let response = await services.shoppingcart.update(rowid, data);
    if (response.status) {
      getItems();
    }
  };

  const removeItem = async (rowid) => {
    let response = await services.shoppingcart.remove(rowid);
    if (response.status) {
      getItems();
    }
  };

  const clear = async () => {
    let response = await services.shoppingcart.clear();
    if (response.status) {
      getItems();
    }
  };

  const createOrder = async (
    freeShipping = false,
    shippingMethod = "DELIVERY",
    chippingProvinceId = "",
    shippingCityId = "",
    shippingAddress = "",
    shippingZipCode = ""
  ) => {
    try {
      let dataOrder = {
        free_shipping: freeShipping,
        shipping_method: shippingMethod,
        province_id: chippingProvinceId,
        city_id: shippingCityId,
        address: shippingAddress,
        zip_code: shippingZipCode,
      };

      let result = await postData(
        `${APP.BASEURL}/api/orders/create`,
        dataOrder
      ).then((data) => {
        return data;
      });

      return result;
    } catch (error) {
      alert(error);
      console.log(error);
    }
  };

  getItems();

  return {
    items,
    totalItems,
    totalCost,
    getItems,
    addItem,
    removeItem,
    updateItem,
    clear,
    createOrder,
  };
})(piniaInstance);
