import { piniaInstance } from '../js/global';
import { defineStore } from 'pinia';


export const useSettingsStore = defineStore("settings", {
  state: () => ({
    defaultCurrency: "USD",
    exchangeRate: 1,
    freeShippingMount: 10000,
    minPurchaseMount: 0,
    defaultImage: `${APP.BASEURL}/dist/images/not-image.svg`,
    //area admin
    defaultCurrencyAdmin: "USD"
  }),
  getters: {
    getCurrency(state) {
      return {
        'currency': this.defaultCurrency,
        'exchange_rate': this.exchangeRate
      }
    },
    getCurrencyAdmin(state) {
      return {
        'currency': this.defaultCurrencyAdmin
      }
    },
    getMinPurchaseMount(state){
      return this.minPurchaseMount;
    }
  },
  actions: {
    async getData() {
      try {
        await fetch('/api/settings/defaults')
          .then((response) => { return response.json(); })
          .then((data) => {
            this.defaultCurrency = data.default_currency.value;
            this.exchangeRate = data.default_currency_exchange_rate.value;
            this.defaultCurrencyAdmin = data.default_currency_admin.value;
            this.minPurchaseMount = data.min_purchase_mount;
          });
      }
      catch (error) {
        alert(error)
        console.log(error)
      }
    }
  }
})(piniaInstance);
