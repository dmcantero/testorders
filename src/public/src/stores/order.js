import { piniaInstance } from "../js/global";
import { defineStore } from "pinia";
import { postData } from "../js/util/fetchhandler";

export const useOrdersStore = defineStore("orders", {
  state: () => ({
    id: 0,
    data: {},
  }),
  getters: {
    getData(state) {
      return data;
    },
  },

  actions: {
    async getOrder() {
      try {
        await fetch(`/api/orders/${this.id}`)
          .then((response) => {
            return response.json();
          })
          .then((data) => {
            this.data = data;
          });
      } catch (error) {
        alert(error);
        console.log(error);
      }
    },
    async saveOrder() {
        try {
          let response = postData('/api/orders/create', this.data);
        } catch (error) {
          alert(error);
          console.log(error);
        }
      }
  },
})(piniaInstance);
