require("../validations-localize.js");
require("../validations-custom.js");
import { createApp } from 'vue';
import { deleteData, getData, postData, putData } from '../../util/fetchhandler.js';
import { Form, ErrorMessage } from 'vee-validate';
import * as yup from 'yup';
import TextInput from '../../../components/TextInput.vue';

let config = {
    module: 'features'
};
createApp({
    data() {
        return {
            module: config.module,
            id: 0,
            datavm: {},
            vschema: yup.object({
                name: yup.string().required()
            })
        }
    },
    components: {
        validationform: Form,
        validationerror: ErrorMessage,
        textinput: TextInput,
    },
    created() {
    },
    mounted() {
        let _this = this;
        this.id = document.getElementById('id').value;
        this.get().then((response) => {
            this.datavm = response;
            this.getValues().then((response) => {
                _this.feature_values = response;
            });
        });
    },
    computed: {
        title() {
            return this.id == 0 ? "Crear atributo" : "Editar atributo";
        }
    },
    methods: {
        get() {
            return getData(
                `${APP.BASEURL}/api/${this.module}/${this.id}`
            );
        },
        getValues() {
            return getData(
                `${APP.BASEURL}/api/featuresvalues?feature_id=${this.id}`
            );
        },
        async validForm() {
            const valid = await this.$refs.ValidationObserver.validate();
            return valid;
        },
        async save(values) {
            if (this.id == 0) {
                postData(`${APP.BASEURL}/api/${this.module}`, this.datavm)
                    .then((response) => {
                        if (response.status) {
                            location.href = `${APP.ADMINBASEURL}/${this.module}`;
                        }
                    })
                    .catch((error) => {
                    });
            } else {
                putData(`${APP.BASEURL}/api/${this.module}`, this.id, this.datavm)
                    .then((response) => {
                        if (response.status) {
                            location.href = `${APP.ADMINBASEURL}/${this.module}`;
                        }
                    })
                    .catch((error) => {
                    });
            }

        },
        async remove() {
            deleteData(`${APP.BASEURL}/api/${this.module}`, this.id)
                .then((response) => {
                    if (response.status) {
                        location.href = `${APP.ADMINBASEURL}/${this.module}`;
                    }
                })
                .catch((error) => {
                });
        }
    }
}).mount(`#app-form-${config.module}`)