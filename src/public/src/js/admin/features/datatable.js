require("../validations-localize.js");
require("../validations-custom.js");
import { createApp } from "vue";
import {
  getData,
  postData,
  deleteData,
  putData,
} from "../../util/fetchhandler";
import { Form, ErrorMessage } from "vee-validate";
import * as yup from "yup";
import DataGrid from "../../../components/Datagrid.vue";
import TextInput from "../../../components/TextInput.vue";
import DialogConfirmation from "../../../components/DialogConfirmation.vue";
import DynamicModalForm from "../../../components/DynamicModalForm.vue";

const config = {
  module: "features",
};

createApp({
  data() {
    return {
      actions: [],
      loaded: false,
      urlRequest: `/api/${config.module}`,
      writeUrlRequest: true,
      defaultParameters: {},
      module: config.module,
      loaded: false,
      id: 0,
      datavm: {},
      datavmValue: {},
      vschema: yup.object({
        name: yup.string().required(),
      }),
      formValueSchema: {
        fields: {
          id: {
            name: "id",
            as: "input",
            type: "hidden",
          },
          feature_id: {
            name: "feature_id",
            as: "input",
            type: "hidden",
          },
          value: {
            label: "Valor",
            name: "value",
            as: "input",
            type: "text",
          }
        },
      },
      formValueData: {},
      vschemaValue: yup.object({
        value: yup.string().required().max(250)
      }),
    };
  },
  components: {
    datagrid: DataGrid,
    validationform: Form,
    validationerror: ErrorMessage,
    textinput: TextInput,
    dialogconfirmation: DialogConfirmation,
    dynamicmodalform: DynamicModalForm,
  },
  created() {
    getData(`/api/permissions?module=${config.module}`).then((data) => {
      this.actions = data;
    });
  },
  mounted() {
    let _this = this;

    this.fetchDataRecord();

    const table = document.getElementById(`${this.title}-datatable`);

    if (table != null) {
      table.addEventListener(
        "CREATE",
        function (e) {
          _this.clearForm();
          //window.location.href = `${config.module}/edit/${e.detail.Id}`;
        },
        false
      );
      table.addEventListener(
        "EDIT",
        function (e) {
          _this.id = e.detail.Id;
          _this.fetchDataRecord();
          //window.location.href = `${config.module}/edit/${e.detail.Id}`;
        },
        false
      );
      table.addEventListener(
        "DELETE",
        function (e) {
          _this.id = e.detail.Id;
          _this.remove();

          //window.location.href = `${config.module}/delete/${e.detail.Id}`;
        },
        false
      );
      table.addEventListener(
        "DETAILS",
        function (e) {
          _this.id = e.detail.Id;
          _this.fetchDataRecord();
          //window.location.href = `${config.module}/delete/${e.detail.Id}`;
        },
        false
      );
    }
  },
  updated() {},
  computed: {
  },
  methods: {
    refreshDataGrid() {
      this.$refs.datagrid.get();
    },
    fetchDataRecord() {
      this.loaded = false;
      getData(
        `${APP.BASEURL}/api/${this.module}/${this.id == 0 ? "new" : this.id}`
      ).then((response) => {
        this.datavm = response;
        this.loaded = true;
      });
    },
    async save(values) {
      let _this = this;
      if (this.id == 0) {
        postData(`${APP.BASEURL}/api/${this.module}`, this.datavm)
          .then((response) => {
            if (response.status) {
              _this.id = response.data.id;
              _this.refreshDataGrid();
              _this.fetchDataRecord();
            }
          })
          .catch((error) => {});
      } else {
        putData(`${APP.BASEURL}/api/${this.module}`, this.id, this.datavm)
          .then((response) => {
            if (response.status) {
              // location.href = `${APP.ADMINBASEURL}/${this.module}`;
              _this.clearForm();
              _this.refreshDataGrid();
            }
          })
          .catch((error) => {});
      }
    },
    async remove() {
      let _this = this;
      await this.$refs["modalDelete"].showDialog().then((confirm) => {
        if (confirm) {
          _this.confirmRemove();
        }
      });
    },
    async confirmRemove() {
      let _this = this;
      deleteData(`${APP.BASEURL}/api/${this.module}`, this.id)
        .then((response) => {
          if (response.status) {
            // location.href = `${APP.ADMINBASEURL}/${this.module}`;
            _this.refreshDataGrid();
          }
        })
        .catch((error) => {});
    },
    async saveFeatureValue(values) {
      let _this = this;
      if (values.id == 0) {
        postData(`${APP.BASEURL}/api/featuresvalues`, values)
          .then((response) => {
            if (response.status) {
              this.$refs["modalValue"].hideDialog();
              this.fetchDataRecord();
            }
          })
          .catch((error) => {});
      } else {
        putData(`${APP.BASEURL}/api/featuresvalues`, values.id, values)
          .then((response) => {
            if (response.status) {
              this.$refs["modalValue"].hideDialog();
              this.fetchDataRecord();
            }
          })
          .catch((error) => {});
      }
    },
    clearForm() {
      this.id = 0;
      this.fetchDataRecord();
    },
    async openModalValue(id) {
      let _this = this;
      if (id != 0) {
        let item = this.datavm.values.filter((v, i) => {
          return v.id == id;
        });
        this.formValueData = item[0];
      } else {
        this.formValueData = {
          id: 0,
          feature_id: this.id,
          value: "",
        };
      }

      this.$refs["modalValue"].showDialog();
    },
  },
}).mount(`#app-${config.module}`);
