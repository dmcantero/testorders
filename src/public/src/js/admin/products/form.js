require("../validations-localize.js");
require("../validations-custom.js");
import { createApp } from "vue";
import {
  deleteData,
  getData,
  postData,
  putData,
} from "../../util/fetchhandler.js";
import axios from "axios";
import { Field, Form, ErrorMessage } from "vee-validate";
import * as yup from "yup";
import GenericSelect from "../../../components/GenericSelect.vue";
import TextInput from "../../../components/TextInput.vue";
import CategoriesTree from "../../../components/CategoriesTree.vue";
import Switch from "../../../components/Switch.vue";
import TextAreaEditor from "../../../components/TextAreaEditor.vue";
import MoneyInput from "../../../components/MoneyInput.vue";
import ImageUpload from "../../../components/ImageUpload.vue";
import BulkPriceProductModal from "../../../components/BulkPriceProductModal.vue";
import ProductFeatureModal from "../../../components/ProductFeatureModel.vue";
import { useSettingsStore } from "../../../stores/settings.js";
import DialogConfirmation from "../../../components/DialogConfirmation.vue";

let config = {
  module: "products",
};
createApp({
  data() {
    return {
      module: config.module,
      id: 0,
      datavm: {
        bulk_pricing: [],
        product_features: [],
        photos: [],
      },
      vschema: yup.object({
        code: yup.string().required().max(100),
        name: yup.string().required().max(250),
        description: yup.string().max(250),
        purchase_price: yup.string().decimal("Fomato incorrecto"),
        stock: yup.number("Valor incorrecto"),
        min_stock: yup.number("Valor incorrecto"),
        min_package: yup.number("Valor incorrecto"),
      }),
      defaultCurrency: "",
    };
  },
  components: {
    validationform: Form,
    field: Field,
    validationerror: ErrorMessage,
    genericselect: GenericSelect,
    textinput: TextInput,
    treelist: CategoriesTree,
    customswitch: Switch,
    textareaeditor: TextAreaEditor,
    moneyinput: MoneyInput,
    imageupload: ImageUpload,
    bulkpricemodal: BulkPriceProductModal,
    productfeaturemodal: ProductFeatureModal,
    dialogconfirmation: DialogConfirmation,
  },
  created() {
    this.settingsStorage = useSettingsStore;
  },
  async mounted() {
    //loading settings
    await this.settingsStorage.getData();
    this.defaultCurrency = this.settingsStorage.getCurrencyAdmin.currency;

    this.id = parseInt(document.getElementById("id").value);
    if (this.id != 0) {
      const dataResponse = await this.fetchAsyncData();
      this.datavm = dataResponse[0];
      this.datavm.product_features = dataResponse[1].data;
      this.datavm.bulk_pricing = dataResponse[2].data;
      this.datavm.photos = dataResponse[3].data;
    } else {
      getData(`${APP.BASEURL}/api/${this.module}/new`).then((data) => {
        this.datavm = data;
      });
    }
  },
  computed: {
    title() {
      return this.id == 0 ? "Crear Producto" : "Editar Producto";
    },
    action() {
      return this.id == 0 ? "create" : "update";
    },
  },
  methods: {
    async fetchAsyncData() {
      const res = await Promise.all([
        getData(
          `${APP.BASEURL}/api/${this.module}/${this.id == 0 ? "new" : this.id}`
        ),
        getData(`${APP.BASEURL}/api/products/getfeatures/${this.id}`),
        getData(`${APP.BASEURL}/api/bulkpricings/getbyproduct/${this.id}`),
        getData(`${APP.BASEURL}/api/products/getphotos/${this.id}`),
      ]);
      const data = res.map((res) => res);
      return data;
    },
    openModalBulkPrice(id, action) {
      this.$refs["modalBulkPrice"].openModal(parseInt(id), action);
    },
    refreshBulkPrices() {
      getData(`${APP.BASEURL}/api/bulkpricings/getbyproduct/${this.id}`).then(
        (result) => {
          this.datavm.bulk_pricing = result.data;
        }
      );
    },
    refreshPhotos() {
      getData(`${APP.BASEURL}/api/products/getphotos/${this.id}`).then(
        (result) => {
          this.datavm.photos = result.data;
        }
      );
    },
    openModalFeature(id, action) {
      this.$refs["modalProductFeature"].openModal(parseInt(id), action);
    },
    refreshFeatures() {
      getData(`${APP.BASEURL}/api/products/getfeatures/${this.id}`).then(
        (result) => {
          this.datavm.product_features = result.data;
        }
      );
    },
    async validForm() {
      const valid = await this.$refs.ValidationObserver.validate();
      return valid;
    },
    async save(values) {
      if (this.id == 0) {
        postData(`${APP.BASEURL}/api/${this.module}`, this.datavm)
          .then((response) => {
            if (response.status) {
              location.href = `${APP.ADMINBASEURL}/${this.module}/edit/${response.data.id}`;
            }
          })
          .catch((error) => {});
      } else {
        putData(`${APP.BASEURL}/api/${this.module}`, this.id, this.datavm)
          .then((response) => {
            if (response.status) {
              location.reload();
            }
          })
          .catch((error) => {});
      }
    },
    async remove() {
      deleteData(`${APP.BASEURL}/api/${this.module}`, this.id)
        .then((response) => {
          if (response.status) {
            location.href = `${APP.ADMINBASEURL}/${this.module}`;
          }
        })
        .catch((error) => {});
    },
    async removePhoto(id) {
      await this.$refs["modalDeletePhoto"].showDialog().then((confirm) => {
        if (confirm) {
          deleteData("/api/file", id).then((result) => {
            this.refreshPhotos();
          });
        }
      });
    },
    async uploadPhotos() {
      let _this = this;
      let formData = new FormData();
      // formData.append("csrf_token", csrf);
      this.isUploading = true;
      for (var i = 0; i < this.$refs.file.files.length; i++) {
        let file = this.$refs.file.files[i];
        formData.append("files[]", file);
      }

      formData.append("product_id", this.datavm.id);

      $.ajax({
        xhr: function () {
          var xhr = new window.XMLHttpRequest();
          xhr.upload.addEventListener(
            "progress",
            function (element) {
              if (element.lengthComputable) {
                var percentComplete = (element.loaded / element.total) * 100;
                $("#file-progress-bar").width(percentComplete + "%");
                $("#file-progress-bar").html(percentComplete + "%");
              }
            },
            false
          );
          return xhr;
        },
        type: "POST",
        url: "/api/products/uploadPhotos",
        data: formData,
        contentType: false,
        cache: false,
        processData: false,
        dataType: "json",
        beforeSend: function () {
          $("#file-progress-bar").width("0%");
          $("#uploadResult").html("");
        },
        success: function (json) {
          if (json.status) {
            json.data.forEach((element) => {
              $("#uploadResult").append(
                `<div class="col-12"><p class="${
                  element.success ? "text-success" : "text-danger"
                }">Archivo ${element.file} : ${element.message}</p></div>`
              );
            });

            _this.refreshPhotos();
          }
        },
        error: function (xhr, ajaxOptions, thrownError) {
          console.log(
            thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText
          );
        },
      });
    },
  },
}).mount(`#app-form-${config.module}`);
