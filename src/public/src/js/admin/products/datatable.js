import { createApp } from "vue";
import {getData, postData, putData} from '../../util/fetchhandler';
import DataGrid from "../../../components/Datagrid.vue";

const config = {
    title: "Productos",
    module: "products"
}

createApp({
    data(){
        return{
            actions: [],
            title: config.title,
            urlRequest: `/api/${config.module}`,
            writeUrlRequest: true,
            defaultParameters: {}
        }
    },
    components:{
        datagrid: DataGrid
    },
    created() {
        getData(`/api/permissions?module=${config.module}`).then((data) => {
            this.actions = data;
        });
    },
    mounted() {
        const table = document.getElementById(`${this.title}-datatable`);
        const _this = this;
        if (table != null) {
            table.addEventListener(
                "CREATE",
                function (e) {
                    window.location.href = `${config.module}/new`;
                },
                false
            );
            table.addEventListener(
                "EDIT",
                function (e) {
                    window.location.href = `${config.module}/edit/${e.detail.Id}`;
                },
                false
            );
            table.addEventListener(
                "DELETE",
                function (e) {
                    window.location.href = `${config.module}/delete/${e.detail.Id}`;
                },
                false
            );
            table.addEventListener(
                "DETAILS",
                function (e) {
                    window.location.href = `${config.module}/delete/${e.detail.Id}`;
                },
                false
            );
            table.addEventListener(
                "CELLROWCHANGED",
                function (e) {
                  if(e.detail.cellName == "active"){
                    _this.toggleActive(e.detail.rowData.Id, e.detail.value);
                  }
                },
                false
              );
        }
    },
    updated() { },
    computed: {},
    methods: {
        get() {
            this.$refs.datatable.get();
        },
        toggleActive(id, active){
            const _this = this;
            let data = {
                id: id,
                active: active
            }

            postData(`${this.urlRequest}/activate`, data).then(result =>{
                _this.get();
            });
        }
    },

}).mount(`#app-datatable-${config.module}`);