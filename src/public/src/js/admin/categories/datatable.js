require("../validations-localize.js");
require("../validations-custom.js");
import { createApp } from "vue";
import DataGrid from "../../../components/Datagrid.vue";
import {
  deleteData,
  getData,
  postData,
  putData,
} from "../../util/fetchhandler.js";
import { Form, ErrorMessage } from "vee-validate";
import * as yup from "yup";
import TextInput from "../../../components/TextInput.vue";
import CategoriesTree from "../../../components/CategoriesTree.vue";
import DialogConfirmation from "../../../components/DialogConfirmation.vue";
import Switch from '../../../components/Switch.vue';

const config = {
  module: "categories",
};

createApp({
  data() {
    return {
      actions: [],
      loaded: false,
      urlRequest: `/api/${config.module}`,
      writeUrlRequest: true,
      defaultParameters: {},
      module: config.module,
      id: 0,
      datavm: {},
      vschema: yup.object({
        name: yup.string().required(),
        description: yup.string().nullable().max(250),
      }),
    };
  },
  components: {
    datagrid: DataGrid,
    validationform: Form,
    validationerror: ErrorMessage,
    textinput: TextInput,
    treelist: CategoriesTree,
    dialogconfirmation: DialogConfirmation,
    customswitch: Switch
  },
  created() {
    getData(`/api/permissions?module=${config.module}`).then((data) => {
      this.actions = data;
    });
  },
  mounted() {
    let _this = this;

    this.fetchDataRecord();

    const table = document.getElementById(`${this.title}-datatable`);

    if (table != null) {
      table.addEventListener(
        "EDIT",
        function (e) {
          _this.id = e.detail.Id;
          _this.fetchDataRecord();
          // window.location.href = `${config.module}/edit/${e.detail.Id}`;
        },
        false
      );
      table.addEventListener(
        "DELETE",
        function (e) {
          _this.id = e.detail.Id;
          _this.remove();
          // window.location.href = `${config.module}/delete/${e.detail.Id}`;
        },
        false
      );
      table.addEventListener(
        "DETAILS",
        function (e) {
          _this.id = e.detail.Id;
          _this.fetchDataRecord();
          // window.location.href = `${config.module}/delete/${e.detail.Id}`;
        },
        false
      );
    }
  },
  updated() { },
  computed: {
    title() {
      return this.id == 0 ? "Crear Categoría" : "Editar Categoría";
    }
  },
  methods: {
    refreshDataGrid() {
      this.$refs.datagrid.get();
    },
    fetchDataRecord() {
      this.loaded = false;
      getData(`${APP.BASEURL}/api/${this.module}/${this.id}`).then(
        (response) => {
          this.datavm = response;
          this.loaded = true;
        }
      );
    },
    async validForm() {
      const valid = await this.$refs.ValidationObserver.validate();
      return valid;
    },
    async save(values) {
      let _this = this;
      if (this.id == 0) {
        postData(`${APP.BASEURL}/api/${this.module}`, this.datavm)
          .then((response) => {
            if (response.status) {
              // location.href = `${APP.ADMINBASEURL}/${this.module}`;
              _this.clearForm();
              _this.refreshDataGrid();
            }
          })
          .catch((error) => { });
      } else {
        putData(`${APP.BASEURL}/api/${this.module}`, this.id, this.datavm)
          .then((response) => {
            if (response.status) {
              // location.href = `${APP.ADMINBASEURL}/${this.module}`;
              _this.clearForm();
              _this.refreshDataGrid();
            }
          })
          .catch((error) => { });
      }
    },
    async remove() {
      let _this = this;
      await this.$refs["modalDelete"]
        .showDialog()
        .then((confirm) => {
          if (confirm) {
            _this.confirmRemove();
          }
        });
    },
    async confirmRemove() {
      let _this = this;
      deleteData(`${APP.BASEURL}/api/${this.module}`, this.id)
        .then((response) => {
          if (response.status) {
            // location.href = `${APP.ADMINBASEURL}/${this.module}`;
            _this.refreshDataGrid();
          }
        })
        .catch((error) => { });
    },
    clearForm() {
      this.id = 0;
      this.fetchDataRecord();
    }
  },
}).mount(`#app-${config.module}`);
