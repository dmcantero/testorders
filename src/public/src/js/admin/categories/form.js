require("../validations-localize.js");
require("../validations-custom.js");
import { createApp } from 'vue';
import { deleteData, getData, postData, putData } from '../../util/fetchhandler.js';
import { Field, Form, ErrorMessage } from 'vee-validate';
import * as yup from 'yup';
import GenericSelect from '../../../components/GenericSelect.vue';
import TextInput from '../../../components/TextInput.vue';
import CategoriesTree from '../../../components/CategoriesTree.vue';
import Switch from '../../../components/Switch.vue';

let config = {
    module: 'categories'
};
createApp({
    data() {
        return {
            module: config.module,
            id: 0,
            datavm: {},
            vschema: yup.object({
                name: yup.string().required(),
                description: yup.string().nullable().max(250)
            })
        }
    },
    components: {
        validationform: Form,
        field: Field,
        validationerror: ErrorMessage,
        genericselect: GenericSelect,
        textinput: TextInput,
        treelist: CategoriesTree,
        customswitch: Switch
    },
    created() {
    },
    mounted() {
        this.id = document.getElementById('id').value;
        this.get().then((response) => {
            this.datavm = response;
        });
    },
    computed: {
        title() {
            return this.id == 0 ? "Crear Categoría" : "Editar Categoría";
        }
    },
    methods: {
        get() {
            return getData(
                `${APP.BASEURL}/api/${this.module}/${this.id}`
            );
        },
        async validForm() {
            const valid = await this.$refs.ValidationObserver.validate();
            return valid;
        },
        async save(values) {
            if (this.id == 0) {
                postData(`${APP.BASEURL}/api/${this.module}`, this.datavm)
                    .then((response) => {
                        if (response.status) {
                            location.href = `${APP.ADMINBASEURL}/${this.module}`;
                        }
                    })
                    .catch((error) => {
                    });
            } else {
                putData(`${APP.BASEURL}/api/${this.module}`, this.id, this.datavm)
                    .then((response) => {
                        if (response.status) {
                            location.href = `${APP.ADMINBASEURL}/${this.module}`;
                        }
                    })
                    .catch((error) => {
                    });
            }

        },
        async remove() {
            deleteData(`${APP.BASEURL}/api/${this.module}`, this.id)
                .then((response) => {
                    if (response.status) {
                        location.href = `${APP.ADMINBASEURL}/${this.module}`;
                    }
                })
                .catch((error) => {
                });
        }
    }
}).mount(`#app-form-${config.module}`)