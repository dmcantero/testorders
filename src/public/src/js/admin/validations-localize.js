import { setLocale } from 'yup';

setLocale({
    mixed: {
        required: "Campo requerido"
    },
    string: {
        required: ({ val }) => ("Campo requerido"),
        email:({ val }) => "Ingrese un email válido",
        min: ({ min }) => (`máximo ${min} caracteres`),
        max: ({ max }) => (`máximo ${max} caracteres`),
    },
    // use functions to generate an error object that includes the value from the schema
    number: {
        min: ({ min }) => (`El valor debe ser mayor a ${min}`),
        max: ({ max }) => (`El valor debe ser menor a ${max}`),
        required: "Campo requerido"
    }
});