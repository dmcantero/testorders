require("../validations-localize.js");
require("../validations-custom.js");
import { createApp } from "vue";
import { getData, postData, putData } from "../../util/fetchhandler.js";
import { Form, ErrorMessage } from "vee-validate";
import * as yup from "yup";
import DataGrid from "../../../components/dataGrid.vue";
import TextInput from "../../../components/TextInput.vue";
import GenericSelect from "../../../components/GenericSelect.vue";
import DynamicModalForm from "../../../components/DynamicModalForm.vue"; 
import CustomSwitch from "../../../components/Switch.vue"; 
import MoneyInput from "../../../components/MoneyInput.vue";

let config = {
  module: "settings",
};
createApp({
  data() {
    return {
      loaded: false,
      module: config.module,
      datavm: {
        generals: [],
        products: [],
      },
      vschema: yup.object({}),
      //Data tab currency
      actionsCurrencies: [],
      vschemaCurrency: yup.object({
        currency: yup.string().required(),
        iso_code: yup.string().required(),
        exchange_rate: yup.number().required(),
      }),
      formSchemaCurrency: {
        fields: {
          id: {
            name: "id",
            as: "input",
            type: "hidden",
          },
          currency: {
            name: "currency",
            label: "Moneda",
            as: "select",
            type: "select",
            children: [
              {
                tag: "option",
                value: "",
                text: "",
              },
              {
                tag: "option",
                value: "Peso Argentino (ARS)",
                text: "Peso Argentino (ARS)",
              },
              {
                tag: "option",
                value: "US Dollar (USD)",
                text: "US Dollar (USD)",
              },
              {
                tag: "option",
                value: "Euro (EUR)",
                text: "Euro (EUR)",
              },
            ],
          },
          name: {
            label: "Nombre",
            name: "name",
            as: "input",
            type: "text",
            disabled: true,
          },
          iso_code: {
            label: "ISO Code",
            name: "iso_code",
            as: "input",
            type: "text",
          },
          exchange_rate: {
            label: "Tasa de cambio",
            name: "exchange_rate",
            as: "input",
            type: "text",
          },
        },
      },
      datavmCurrency: {},
    };
  },
  components: {
    validationform: Form,
    validationerror: ErrorMessage,
    datagrid: DataGrid,
    textinput: TextInput,
    customswitch: CustomSwitch,
    genericselect: GenericSelect,
    modalform: DynamicModalForm,
    moneyinput: MoneyInput
  },
  created() {
    this.getPermissionsCurrency();
  },
  mounted() {
    this.getSettings();
    this.listenersDataGridCurrency();
  },
  computed: {},
  watch: {
    datavm: {
      deep: true,
      handler: (newVal, oldVal) => {},
    },
  },
  methods: {
    getSettings() {
      getData(`${APP.BASEURL}/api/${this.module}`).then((response) => {
        this.datavm = response;
        this.loaded = true;
      });
    },
    getCurrencies() {
      this.$refs.datagridcurrency.get();
    },
    getPermissionsCurrency() {
      getData(`/api/permissions?module=currencies`).then((data) => {
        this.actionsCurrencies = data;
      });
    },
    listenersDataGridCurrency() {
      const table = document.getElementById(`currencies-datatable`);
      const _this = this;
      if (table != null) {
        table.addEventListener(
          "CREATE",
          function (e) {
            _this.getDataRecordCurrency(0);
          },
          false
        );
        table.addEventListener(
          "EDIT",
          function (e) {
            _this.getDataRecordCurrency(e.detail.Id);
          },
          false
        );
        table.addEventListener(
          "CELLROWCHANGED",
          function (e) {
            if (e.detail.cellName == "active") {
              _this.toggleActiveCurrency(e.detail.rowData.Id, e.detail.rowData);
            }
          },
          false
        );
      }
    },
    getDataRecordCurrency(id) {
      this.loaded = false;
      getData(`${APP.BASEURL}/api/currencies/${id == 0 ? "new" : id}`).then(
        (response) => {
          this.datavmCurrency = response;
          this.$refs["modalFormCurrency"].showDialog();
          this.loaded = true;
        }
      );
    },
    toggleActiveCurrency(id, data) {
      putData(`${APP.BASEURL}/api/currencies`, id, data).then((result) => {
        this.getCurrencies();
      });
    },
    async saveCurrency(values) {
      let _this = this;
      if (this.datavmCurrency.id == 0) {
        postData(`${APP.BASEURL}/api/currencies`, this.datavmCurrency)
          .then((response) => {
            if (response.status) {
              this.$refs["modalFormCurrency"].hideDialog();
              this.getCurrencies();
            }
          })
          .catch((error) => {});
      } else {
        putData(
          `${APP.BASEURL}/api/currencies`,
          this.datavmCurrency.id,
          this.datavmCurrency
        )
          .then((response) => {
            if (response.status) {
              this.$refs["modalFormCurrency"].hideDialog();
              this.getCurrencies();
            }
          })
          .catch((error) => {});
      }
    },
    async save(values) {
      postData(`${APP.BASEURL}/api/${this.module}`, this.datavm)
        .then((response) => {
          if (response.status) {
            location.href = `${APP.ADMINBASEURL}/${this.module}`;
          }
        })
        .catch((error) => {});
    },
  },
}).mount(`#app-${config.module}`);
