import { createApp } from "vue";
import * as yup from "yup";
import { getData, postData, putData } from "../../util/fetchhandler";
import DataGrid from "../../../components/Datagrid.vue";
import DynamicModalForm from "../../../components/DynamicModalForm.vue";

const config = {
  title: "Monedas y divisas",
  module: "currencies",
};

createApp({
  data() {
    return {
      actions: [],
      title: config.title,
      module: config.module,
      urlRequest: `api/${config.module}`,
      writeUrlRequest: true,
      defaultParameters: {},
      datavm: {},
      loaded: false,
      vschema: yup.object({
        currency: yup.string().required(),
        iso_code: yup.string().required(),
        exchange_rate: yup.number().required(),
      }),
      formSchema: {
        fields: {
          id: {
            name: "id",
            as: "input",
            type: "hidden",
          },
          currency: {
            name: "currency",
            label: "Moneda",
            as: "select",
            type: "select",
            children: [
              {
                tag: "option",
                value: "",
                text: "",
              },
              {
                tag: "option",
                value: "Peso Argentino (ARS)",
                text: "Peso Argentino (ARS)",
              },
              {
                tag: "option",
                value: "US Dollar (USD)",
                text: "US Dollar (USD)",
              },
              {
                tag: "option",
                value: "Euro (EUR)",
                text: "Euro (EUR)",
              },
            ],
          },
          name: {
            label: "Nombre",
            name: "name",
            as: "input",
            type: "text",
            disabled: true,
          },
          iso_code: {
            label: "ISO Code",
            name: "iso_code",
            as: "input",
            type: "text",
          },
          exchange_rate: {
            label: "Tasa de cambio",
            name: "exchange_rate",
            as: "input",
            type: "text",
          },
        },
      },
    };
  },
  components: {
    datagrid: DataGrid,
    modalform: DynamicModalForm,
  },
  created() {
    getData(`/api/permissions?module=${config.module}`).then((data) => {
      this.actions = data;
    });
  },
  mounted() {
    const table = document.getElementById(`${this.title}-datatable`);
    const _this = this;
    if (table != null) {
      table.addEventListener(
        "CREATE",
        function (e) {
          _this.fetchDataRecord(0);
        },
        false
      );
      table.addEventListener(
        "EDIT",
        function (e) {
          _this.fetchDataRecord(e.detail.Id);
        },
        false
      );
      table.addEventListener(
        "CELLROWCHANGED",
        function (e) {
          if (e.detail.cellName == "active") {
            _this.toggleActive(e.detail.rowData.Id, e.detail.rowData);
          }
        },
        false
      );
    }
  },
  updated() {},
  computed: {},
  methods: {
    refreshDataGrid() {
      this.$refs.datagrid.get();
    },
    fetchDataRecord(id) {
      this.loaded = false;
      getData(`${APP.BASEURL}/api/${this.module}/${id == 0 ? "new" : id}`).then(
        (response) => {
          this.datavm = response;
          this.openModal();
          this.loaded = true;
        }
      );
    },
    toggleActive(id, data) {
      putData(`${APP.BASEURL}/api/${this.module}`, id, data).then((result) => {
        this.refreshDataGrid();
      });
    },
    async save(values) {
      let _this = this;
      if (this.datavm.id == 0) {
        postData(`${APP.BASEURL}/api/${this.module}`, this.datavm)
          .then((response) => {
            if (response.status) {
              this.$refs["modalForm"].hideDialog();
              this.refreshDataGrid();
            }
          })
          .catch((error) => {});
      } else {
        putData(
          `${APP.BASEURL}/api/${this.module}`,
          this.datavm.id,
          this.datavm
        )
          .then((response) => {
            if (response.status) {
              this.$refs["modalForm"].hideDialog();
              this.refreshDataGrid();
            }
          })
          .catch((error) => {});
      }
    },
    async openModal() {
      this.$refs["modalForm"].showDialog();
    },
  },
}).mount(`#app-datatable-${config.module}`);
