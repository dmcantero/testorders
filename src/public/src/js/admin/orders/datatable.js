import { createApp, ref } from "vue";
import { getData } from "../../util/fetchhandler";
import moment from "moment";
import { Field, Form, ErrorMessage } from "vee-validate";
import * as yup from "yup";
import DataGrid from "../../../components/Datagrid.vue";
import TextInput from "../../../components/TextInput.vue";
import GenericSelect from "../../../components/GenericSelect.vue";

const config = {
  title: "Notas de pedido",
  module: "orders",
};

createApp({
  data() {
    return {
      actions: [],
      title: config.title,
      urlRequest: `api/${config.module}`,
      writeUrlRequest: true,
      defaultParameters: {
        date_from: moment(new Date()).format("YYYY-MM-DD"),
        date_until: moment(new Date()).format("YYYY-MM-DD"),
        currency: "USD",
        client_id: 0,
        status: "",
      },
      vfilterschema: yup.object().shape(
        {
          date_from: yup.date().when("date_until", ([date_until], schema) =>
            date_until != null && date_until != undefined
              ? yup
                  .date()
                  .nullable()
                  .transform((v) => (v instanceof Date && !isNaN(v) ? v : null))
                  .max(date_until, "No puede ser mayor a fecha hasta")
              : yup
                  .date()
                  .nullable()
                  .transform((v) => (v instanceof Date && !isNaN(v) ? v : null))
          ),
          date_until: yup.date().when("date_from", ([date_from], schema) =>
            date_from != null && date_from != undefined
              ? yup
                  .date()
                  .nullable()
                  .transform((v) => (v instanceof Date && !isNaN(v) ? v : null))
                  .min(date_from, "No puede ser menor a fecha desde")
              : yup
                  .date()
                  .nullable()
                  .transform((v) => (v instanceof Date && !isNaN(v) ? v : null))
          ),
        },
        [["date_until", "date_from"]]
      ),
    };
  },
  components: {
    validationform: Form,
    validationerror: ErrorMessage,
    datagrid: DataGrid,
    textinput: TextInput,
    genericselect: GenericSelect,
  },
  created() {
    getData(`/api/permissions?module=${config.module}`).then((data) => {
      this.actions = data;
    });
  },
  mounted() {
    const table = document.getElementById(`${this.title}-datatable`);
    const _this = this;
    if (table != null) {
      table.addEventListener(
        "EDIT",
        function (e) {
          window.location.href = `${config.module}/${e.detail.Id}/edit`;
        },
        false
      );
      table.addEventListener(
        "DELETE",
        function (e) {
          window.location.href = `${config.module}/delete/${e.detail.Id}`;
        },
        false
      );
      table.addEventListener(
        "DOWNLOAD",
        function (e) {
          window.location.href = `/reports/order/${e.detail.Id}/${e.detail.Row.number}/true/${_this.defaultParameters.currency}`;
        },
        false
      );
      table.addEventListener(
        "DETAILS",
        function (e) {
          window.location.href = `${config.module}/delete/${e.detail.id}`;
        },
        false
      );
      table.addEventListener("CELLROWCHANGED", function (e) {}, false);
    }
  },
  updated() {},
  computed: {},
  methods: {
    filter(values, actions) {
      this.$refs.datatable.get();
    },
    setRowBackground(datarow) {
      switch (datarow.status) {
        case "CONFIRMED":
          return "table-success";
          break;
        default:
          return "jsgrid-row";
          break;
      }
    },
  },
}).mount(`#app-datatable-${config.module}`);
