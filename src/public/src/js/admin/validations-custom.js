import { addMethod, string, number, date} from 'yup';

addMethod(string, 'required_select', function (message, mapper = a => a) {
    return this.test('required_select', message, function (value) {
        const { path, createError } = this;
        if(value == "" || value == "0" || value == undefined || value == null){
            return createError({path, message})
        }
        return true;
    });
});


addMethod(string, 'decimal', function (message, mapper = a => a) {
    return this.test('decimal', message, function (value) {
        const { path, createError } = this;
        if(/^((-?[0-9]+)((\.|,)\d([0-9]{0,3}))?)\s?$/.test(value)){
            return true;
        }
        return createError({path, message})
    });
});
