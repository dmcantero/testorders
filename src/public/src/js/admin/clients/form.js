require("../validations-localize.js");
require("../validations-custom.js");
import { createApp } from "vue";
import {
  getData,
  postData,
  putData
} from "../../util/fetchhandler.js";
import { Field, Form, ErrorMessage } from "vee-validate";
import * as yup from "yup";
import GenericSelect from "../../../components/GenericSelect.vue";
import TextInput from "../../../components/TextInput.vue";
import Switch from "../../../components/Switch.vue";
import TextAreaEditor from "../../../components/TextAreaEditor.vue";
import MoneyInput from "../../../components/MoneyInput.vue";

let config = {
  module: "clients",
};
createApp({
  data() {
    return {
      module: config.module,
      id: 0,
      datavm: {},
      errorMessages: [],
      tableFeatures: {
        title: "Cliente",
        urlRequest: "api/clients/",
      },
      vschema: yup.object({
        first_name: yup.string().required(),
        last_name: yup.string().required(),
        email: yup.string().email().required(),
        business_name: yup.string().required()
      }),
    };
  },
  components: {
    validationform: Form,
    field: Field,
    validationerror: ErrorMessage,
    genericselect: GenericSelect,
    textinput: TextInput,
    customswitch: Switch,
    textareaeditor: TextAreaEditor,
    moneyinput: MoneyInput,
  },
  created() {},
  async mounted() {
    this.id = parseInt(document.getElementById("id").value);
    if (this.id != 0) {
      const dataResponse = await this.fetchAsyncData();
      this.datavm = dataResponse[0];
    }
  },
  computed: {
    title() {
      return this.id == 0 ? "Crear Cliente" : "Editar Cliente";
    },
    action() {
      return this.id == 0 ? "create" : "update";
    }
  },
  methods: {
    async fetchAsyncData() {
      const res = await Promise.all([
        getData(
          `${APP.BASEURL}/api/${this.module}/${this.id == 0 ? "new" : this.id}`
        ),
      ]);
      const data = res.map((res) => res);
      return data;
    },
    async validForm() {
      const valid = await this.$refs.ValidationObserver.validate();
      return valid;
    },
    async save(values) {
      let _this = this;
      if (this.id == 0) {
        postData(`${APP.BASEURL}/api/${this.module}`, this.datavm)
          .then((response) => {
            if (response.status) {
              location.href = `${APP.ADMINBASEURL}/${this.module}/edit/${response.data.id}`;
            } else {
              _this.processErrorMessages(response);
            }
          })
          .catch((error) => {});
      } else {
        putData(`${APP.BASEURL}/api/${this.module}`, this.id, this.datavm)
          .then((response) => {
            if (response.status) {
              location.reload();
            } else {
              _this.processErrorMessages(response);
            }
          })
          .catch((error) => {});
      }
    },
    async remove() {
      deleteData(`${APP.BASEURL}/api/${this.module}`, this.id)
        .then((response) => {
          if (response.status) {
            location.href = `${APP.ADMINBASEURL}/${this.module}`;
          }
        })
        .catch((error) => {});
    },
    processErrorMessages(response) {
      let errors = [];
      let index = 0;
      for (const [key, value] of Object.entries(response.messages)) {
        errors[index] = value;
        index++;
      }

      this.errorMessages = errors;
    },
  },
}).mount(`#app-form-${config.module}`);
