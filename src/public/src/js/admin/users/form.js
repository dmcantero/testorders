require("../validations-localize.js");
require("../validations-custom.js");
import { createApp } from "vue";
import {
  deleteData,
  getData,
  postData,
  putData,
} from "../../util/fetchhandler.js";
import { Form, ErrorMessage, Field } from "vee-validate";
import * as yup from "yup";
import GenericSelect from "../../../components/GenericSelect.vue";
import TextInput from "../../../components/TextInput.vue";
import Switch from "../../../components/Switch.vue";

let config = {
  module: "users",
};
createApp({
  data() {
    return {
      module: config.module,
      id: 0,
      datavm: {},
      showpassword: true,
      forceresetpassword: false,
      errorMessages: [],
      vschema: yup.object({
        username: yup.string().required().max(30).min(3),
        email: yup.string().required().email().max(254),
        group: yup.string().required_select("Seleccione un grupo"),
        password: yup.string().required(),
        password_confirm: yup
          .string()
          .required()
          .oneOf([yup.ref("password"), null], "Las contraseñas no coinciden"),
      }),
      vschemaedit: yup.object({
        username: yup.string().required().max(30).min(3),
        email: yup.string().required().email().max(254),
        group: yup.string().required_select("Seleccione un grupo")
      })
    };
  },
  components: {
    validationform: Form,
    validationerror: ErrorMessage,
    textinput: TextInput,
    customswitch: Switch,
    field: Field,
  },
  created() {},
  async mounted() {
    this.id = parseInt(document.getElementById("id").value);
    //Always true when is a new user
    if(this.id == 0){
        this.forceresetpassword = true;
    }
    this.fetchDataRecord().then((response) => {
      this.datavm = response;
    });
  },
  computed: {
    action() {
      return this.id == 0 ? "create" : "update";
    },
  },
  methods: {
    fetchDataRecord() {
      return getData(
        `${APP.BASEURL}/api/${this.module}/${this.id == 0 ? "new" : this.id}`
      );
    },
    async save(values) {
      if (this.id == 0) {
        postData(`${APP.BASEURL}/api/${this.module}`, values)
          .then((response) => {
            if (response.status) {
              location.href = `${APP.ADMINBASEURL}/${this.module}/edit/${response.data.id}`;
            } else {
              this.processErrorMessages(response);
            }
          })
          .catch((error) => {});
      } else {
        values.forceresetpassword = this.forceresetpassword;
        putData(`${APP.BASEURL}/api/${this.module}`, this.id, values)
          .then((response) => {
            if (response.status) {
              location.reload();
            } else {
              this.processErrorMessages(response);
            }
          })
          .catch((error) => {});
      }
    },
    processErrorMessages(response) {
      let errors = [];
      let index = 0;
      for (const [key, value] of Object.entries(response.messages)) {
        errors[index] = value;
        index++;
      }

      this.errorMessages = errors;
    },
  },
}).mount(`#app-form-${config.module}`);
