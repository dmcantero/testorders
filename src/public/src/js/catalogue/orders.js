require("../admin/validations-localize.js");
require("../admin/validations-custom.js");
import { createApp } from "vue";
import { getData } from "../util/fetchhandler.js";
import { Field, Form, ErrorMessage } from "vee-validate";
import GenericSelect from "../../components/GenericSelect.vue";
import TextInput from "../../components/TextInput.vue";
import CartResumenSideBar from "../../components/Cart/CartResumenSideBar.vue";
import { useSettingsStore } from "../../stores/settings.js";

createApp({
  data() {
    return {
      loaded: false,
      orders: [],
      filters: {
        date_from: "",
        date_until: "",
        client_id: 0,
        status: ""
      },
      pagination: {
        pageSize: Number.MAX_SAFE_INTEGER,
        pageIndex: 1,
        totalRecords: 0,
        filteredRecords: 0,
      },
      orderBy: "",
      settingsStorage: null
    };
  },
  components: {
    genericselect: GenericSelect,
    textinput: TextInput,
    "cart-resumen-sidebar": CartResumenSideBar
  },
  created() {
    this.settingsStorage = useSettingsStore;
  },
  async mounted() {
    this.filters.client_id = document.getElementById('clientId').value;
    await this.settingsStorage.getData();
    this.filters.currency = this.settingsStorage.getCurrency.currency;
    this.getOrders();
  },
  watch: {
    checkedCategories: {
      handler(newval, oldval) {
        this.filters.categoryId = newval
          .map((item) => {
            return item.id;
          })
          .join(",");

        this.getProducts();
      },
    }
  },
  computed: {
  },
  methods: {
    async getOrders() {
      let _this = this;
      let params = new URLSearchParams(this.filters);
      params.set("pageIndex", this.pagination.pageIndex);
      params.set("pageSize", this.pagination.pageSize);
      params.set("orderBy", this.orderBy);
      getData(`${APP.BASEURL}/api/orders/${this.filters.client_id}/historyclient?${params.toString()}`, false).then(
        (response) => {
          _this.orders = response;
          _this.loaded = true;
        }
      );
    },
    clearFilters() {
    },
  },
}).mount("#app");
