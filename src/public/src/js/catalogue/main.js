require("../admin/validations-localize.js");
require("../admin/validations-custom.js");
import { createApp } from "vue";
import { piniaInstance } from "../global.js";
import { rawurldecode } from '../util/utils.js';
import { getData } from "../util/fetchhandler.js";
import { Field, Form, ErrorMessage } from "vee-validate";
import GenericSelect from "../../components/GenericSelect.vue";
import TextInput from "../../components/TextInput.vue";
import MoneyInput from "../../components/MoneyInput.vue";
import CategoriesTree from "../../components/CategoriesTreeCatalogue.vue";
import ProductCatalogue from "../../components/ProductCatalogue.vue";
import CartResumenSideBar from "../../components/Cart/CartResumenSideBar.vue";
import CartDetails from "../../components/Cart/CartDetails.vue";
import CartResumen from "../../components/Cart/CartResumen.vue";
import useCartStore from "../../stores/cart";
import { useSettingsStore } from "../../stores/settings.js";

createApp({
  data() {
    return {
      loaded: false,
      modeView: "grid",
      products: [],
      checkedCategories: [],
      filters: {
        categoryId: "",
        currency: null,
        search: "",
      },
      pagination: {
        pageSize: Number.MAX_SAFE_INTEGER,
        pageIndex: 1,
        totalRecords: 0,
        filteredRecords: 0,
      },
      orderBy: "",
      cartStorage: useCartStore,
      settingsStorage: null,
    };
  },
  components: {
    validationform: Form,
    validationerror: ErrorMessage,
    genericselect: GenericSelect,
    textinput: TextInput,
    moneyinput: MoneyInput,
    productcatalogue: ProductCatalogue,
    categorieslist: CategoriesTree,
    "cart-resumen-sidebar": CartResumenSideBar,
    "cart-details": CartDetails,
    "cart-resumen": CartResumen,
  },
  created() {
    this.settingsStorage = useSettingsStore;
  },
  async mounted() {
    await this.settingsStorage.getData();
    this.filters.currency = this.settingsStorage.getCurrency.currency;
    //check if category exists in url
    let params = new URLSearchParams(document.location.search);
    let category = params.get("category");
    if (category !== null && category !== "") {
      category = rawurldecode(category).split('-');
      let categoryId = category[0];
      let categoryName = category[1];
      this.checkedCategories= [{'id': categoryId, 'label': categoryName}];
      return;
    }
    this.getProducts();
  },
  watch: {
    checkedCategories: {
      handler(newval, oldval) {
        this.filters.categoryId = newval
          .map((item) => {
            return item.id;
          })
          .join(",");

        this.getProducts();
      },
    },
  },
  computed: {},
  methods: {
    async getProducts() {
      let params = new URLSearchParams(this.filters);
      params.set("pageIndex", this.pagination.pageIndex);
      params.set("pageSize", this.pagination.pageSize);
      params.set("orderBy", this.orderBy);
      getData(`${APP.BASEURL}/api/catalogue?${params.toString()}`, false).then(
        (response) => {
          this.products = response.Data;
          this.pagination.totalRecords = response.TotalRecords;
          this.pagination.filteredRecords = response.FilteredRecords;
          this.loaded = true;
        }
      );
    },
    clearFilters() {
      this.checkedCategories = [];
      $(".grtvn-self-checkbox").prop("checked", false);
    },
    getOrders() { },
  },
}).mount("#app");
