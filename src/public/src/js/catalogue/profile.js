require("../admin/validations-localize.js");
require("../admin/validations-custom.js");
import { createApp } from "vue";
import * as yup from 'yup';
import { getData, postData } from "../util/fetchhandler.js";
import { Field, Form, ErrorMessage } from "vee-validate";
import GenericSelect from "../../components/GenericSelect.vue";
import TextInput from "../../components/TextInput.vue";
import CartResumenSideBar from "../../components/Cart/CartResumenSideBar.vue";
import { useSettingsStore } from "../../stores/settings.js";

createApp({
  data() {
    return {
      loaded: false,
      datavm: {},
      vschema: yup.object({
        first_name: yup.string().required(),
        last_name: yup.string().required(),
        business_name: yup.string().required(),
        username: yup.string().required().max(30).min(3),
        email: yup.string().required().email().max(254),
      }),
    };
  },
  components: {
    validationform: Form,
    field: Field,
    validationerror: ErrorMessage,
    genericselect: GenericSelect,
    textinput: TextInput,
    "cart-resumen-sidebar": CartResumenSideBar,
  },
  created() {
    this.settingsStorage = useSettingsStore;
  },
  async mounted() {
    await this.settingsStorage.getData();
    await this.getDataProfile();
  },
  computed: {},
  methods: {
    async getDataProfile() {
      let _this = this;
      getData(`${APP.BASEURL}/api/users/profile`, false).then((response) => {
        _this.datavm = response;
        _this.loaded = true;
      });
    },
    async save(values) {
      postData(`${APP.BASEURL}/api/users/profilepost`, this.datavm)
        .then((response) => {
          if (response.status) {
            location.reload();
          }
        })
        .catch((error) => {
          alert("Ha ocurrido un error.");
        });
    },
  },
}).mount("#app-profile");
