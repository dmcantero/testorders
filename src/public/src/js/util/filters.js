import {createApp} from 'vue';

let app = createApp({});

app.config.globalProperties.$filters = {
  toCurrency(value) {
    if (typeof value == "string") {
      value = parseFloat(value);
    }
    // if (typeof value !== "number") {
    //   return value;
    // }
    var formatter = new Intl.NumberFormat("es-AR", {
      style: "currency",
      currency: "ARS",
    });
    return formatter.format(value);
  },
  capitalize(value){
    if (!value) return "";
    value = value.toString();
    return value.charAt(0).toUpperCase() + value.slice(1);
  }
}


// Vue.filter("toCurrency", function (value) {
//   if (typeof value == "string") {
//     value = parseFloat(value);
//   }
//   // if (typeof value !== "number") {
//   //   return value;
//   // }
//   var formatter = new Intl.NumberFormat("es-AR", {
//     style: "currency",
//     currency: "ARS",
//   });
//   return formatter.format(value);
// });

// Vue.filter("capitalize", function (value) {
//   if (!value) return "";
//   value = value.toString();
//   return value.charAt(0).toUpperCase() + value.slice(1);
// });
