﻿import { localize, extend } from 'vee-validate';
// import { required, email, integer, between, min, max, min_value} from 'vee-validate/dist/vee-validate';
import { postData } from '../util/fetchhandler';
import moment from 'moment/moment';

// extend('required', required);
// extend('email', email);
// extend('integer', integer);
// extend('between', between);
// extend('min', min);
// extend('max', max);
// extend('min_value', min_value);

// const localizeEs = {
//   es: {
//     messages: {
//       required: "Campo requerido",
//       requiredIf: "Campo requerido",
//       email: "Ingrese un email válido",
//       excluded: "Valor no permitido",
//       confirmed: "Los valores no coinciden",
//       date_format: (_, { format }) =>
//         `La fecha debe tener el formato ${format}`,
//       numeric: "Solo se permiten números",
//       min: (_, { length }) => `Mínimo ${length} caracteres`,
//       max: (_, { length }) => `Máximo ${length} caracteres`,
//       min_value: (_, { min }) => `No puede ser menor a ${min}`,
//       between: (_, { min, max }) =>
//         `El valor debe estar entre ${min} y ${max}`,
//       uniqueemail: (field, params, data) =>
//         `Este email ya se encuentra registrado`,
//       rangedates: (field, params, data) => "Valor incorrecto",
//       isdecimal: (field, params, data) => `Valor incorrecto`,
//       select2required: (field, params, data) => `Campo requerido`,
//       greaterthanzero: (field, params, data) => `No puede ser 0`
//     },
//   },
// };

// const isValidRangeDates = (a, { b, origen }) => {
//   if (a && b) {
//     let isValid = true;
//     let startDate = origen == "desde" ? moment(a) : moment(b);
//     let endDate = origen == "desde" ? moment(b) : moment(a);

//     isValid = startDate <= endDate && endDate >= startDate;
//     return {
//       valid: isValid,
//     };
//   }
// };

// const isUniqueEmail = (value, { id }) => {
//   return postData("/api/users/validateemail", { email: value, id: id }, false)
//     .then((response) => {
//       // Notice that we return an object containing both a valid property and a data property.
//       return {
//         valid: response.data.valid,
//         data: {
//           message: response.data.message,
//         },
//       };
//     });
// };

// localize("es", localizeEs.es);

// extend("selectrequired", {
//   validate(value, [args]) {
//     if (Number(value) > 0 && value !== "" && value != null) {
//       return true;
//     }

//     return `Seleccione un valor`;
//   },
//   getMessage(field) {
//     return `Seleccione un valor`;
//   },
// });

// extend("select2required", {
//   validate(value, [args]) {
//     if (value == "-1") return true;
//     let valid =
//       value.id != "" &&
//       value.id != null &&
//       value.id != "00000000-0000-0000-0000-000000000000";
//     console.log(valid);
//     return valid;
//   },
//   getMessage(field) {
//     return `Seleccione un valor`;
//   },
// });

// // The messages getter may also accept a third parameter that includes the data we returned earlier.
// extend("isdecimal", {
//   validate(value, [args]) {
//     return /^((-?[0-9]+)((\.|,)\d([0-9]{0,3}))?)\s?$/.test(value);
//   },
//   getMessage: (field, params, data) => {
//     return data.message;
//   },
//   hasTarget: false,
// });

// extend("greaterthanzero", {
//   validate(value, [args]) {
//     if (typeof value == "string") {
//       if (value.includes(",")) {
//         value = parseFloat(value.replace(",", "").replace(".", ""));
//       }
//     }
//     return value > 0;
//   },
//   getMessage: (field, params, data) => {
//     return data.message;
//   },
//   hasTarget: false,
// });

// // The messages getter may also accept a third parameter that includes the data we returned earlier.
// extend("uniqueemail", {
//   params: ["id"],
//   validate: isUniqueEmail,
//   getMessage: (field, params, data) => {
//     return data.message;
//   },
//   hasTarget: true,
// });

// extend("rangedates", {
//   params: ["b", "origen"],
//   validate: isValidRangeDates,
//   getMessage: (field, params, data) => {
//     return data.message;
//   },
//   hasTarget: true,
// });
