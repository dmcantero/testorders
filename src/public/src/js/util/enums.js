﻿const MESSAGESATUS = {
  Success: 1,
  Error: 2,
};

const ICONSACTIONSGRIDROW = {
  CREATE: null,
  EDIT: "bi bi-pencil",
  DETAILS: "bi bi-eye",
  DELETE: "bi bi-trash",
  CHANGEPASSWORD: "bi bi-key",
  ACTIVATE: "bi bi-unlock",
  DOWNLOAD: "bi bi-download",
  ORDER_COLUMN_UP: "bi bi-sort-alpha-up",
  ORDER_COLUMN_DOWN: "bi bi-sort-alpha-down",
};

export { MESSAGESATUS, ICONSACTIONSGRIDROW };
