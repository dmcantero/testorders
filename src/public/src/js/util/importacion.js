require("../../components/_globals.js");
import Vue from "vue";

import { errorMessage } from "./notifications.js";

new Vue({
    el: "#appImportacion",
    created() {
    },
    mounted() {
    },
    data: {
        numeroFila: 2,
        fileData: null,
        MAXFILESIZEMB: 1
    },
    watch: {
    },
    computed: {},
    methods: {
        async validForm() {
            const valid = await this.$refs.ValidationObserver.validate();
            return valid;
        },
        importData(e) {
            try {
                let submitBtn = $(e.currentTarget);
                let form = $('#import_form');

                submitBtn.prop("disabled", true);
                submitBtn.text("Procesando...");

                form.submit();
            } catch (error) {
                errorMessage("Ha ocurrido un error");
            }
        },
        async processFile(e) {
            let valid = await this.validForm();
            if (valid) {
                let form = $(e.currentTarget);
                let submitBtn = $(form).find(":submit");

                submitBtn.prop("disabled", true);
                submitBtn.text("Procesando...");

                form.submit();
            }
        },
        validateFileSize(file) {
            let fileSize = file.size;
            let fileMb = fileSize / 1024 ** 2;
            if (fileMb >= this.MAXFILESIZEMB) {
                errorMessage(
                    "",
                    `El tamaño del archivo no puede superar ${this.MAXFILESIZEMB} MB.`,
                    4000
                );
                return false;
            }

            return true;
        },
        async validateFile(e) {
            this.fileData = null;
            let { valid } = await this.$refs.providerFile.validate(e)
            if (valid) {
                let file = this.$refs.file.files[0]
                let validSize = this.validateFileSize(file);
                if (validSize) {
                    this.fileData = file;
                }
            }
        }
    },
});