<?php

namespace App\Models;

use CodeIgniter\Model;

class ListasPreciosProductosModel extends Model
{
    protected $DBGroup          = 'default';
    protected $table            = 'listas_precios_productos';
    protected $primaryKey       = 'id';
    protected $useAutoIncrement = true;
    protected $insertID         = 0;
    protected $returnType       = 'array';
    protected $useSoftDeletes   = true;
    protected $protectFields    = true;
    protected $allowedFields    = ['id', 'producto_id', 'lista_precio_id', 'stock', 'precio_venta', 'eliminado', 'created_by', 'updated_by', 'deleted_by', 'multiplo', 'margen_ganancia'];

    // Dates
    protected $useTimestamps = true;
    protected $dateFormat    = 'datetime';
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    // Validation
    protected $validationRules      = [];
    protected $validationMessages   = [];
    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = true;
    protected $beforeInsert   = ['setCreatedBy'];
    protected $afterInsert    = [];
    protected $beforeUpdate   = ['setUpdatedBy'];
    protected $afterUpdate    = ['afterUpdateListasPreciosProductosUpdatePedidosDetalles'];
    protected $beforeFind     = [];
    protected $afterFind      = [];
    protected $beforeDelete   = ['setDeletedBy'];
    protected $afterDelete    = [];

    /**
     * Ejecuta la llamada el procedimiento almacenado para bucar los registros en la base de datos.
     * @return array
     */
    public function buscar($lista_id = '')
    {
        $query = "CALL BuscarProductosPorListasPrecios('$lista_id',"
            . "@totalRecords);";
        $result = $this->query($query)->getResult();
        //El sp no devolvio filas
        // if (count($result) == 1) {
        //     $data["Rows"] = [];
        // } else {
        //     $data["Rows"] = $result[0]; //filas devueltas por el sp
        // }
        // $data["TotalRecords"] = (int)($result[1][0])->totalRecords;

        return $result;
    }

    protected function setCreatedBy(array $data)
    {
        $data['data']['created_by'] = user()->username;
        return $data;
    }

    protected function setUpdatedBy(array $data)
    {
        if ($data['data']['eliminado'] == 1) {
            $data['data']['deleted_by'] = user()->username;
        } else {
            $data['data']['updated_by'] = user()->username;
        }
        return $data;
    }

    protected function setDeletedBy(array $data)
    {
        $data['data']['deleted_by'] = user()->username;
        return $data;
    }

    public function catalogoProductos($palabras = '', $cliente_id = 0, $lista_id = 0, $destacado = FALSE, $nuevo = FALSE, $oferta = FALSE, $en_carrito = FALSE, $todos = FALSE, $orden = '', $pageIndex = 1, $pageSize = 10)
    {
        $query = "CALL CatalogoProductos('$palabras',"
            . "$cliente_id,"
            . "$lista_id,"
            . "'$destacado',"
            . "'$nuevo',"
            . "$oferta,"
            . "$en_carrito,"
            . "$todos,"
            . "'$orden',"
            . "$pageIndex,"
            . "$pageSize,"
            . "@totalRecords,@firtsRecords,@lastRecords);";
        $result = $this->multiple_result($query);
        //El sp no devolvio filas
        if (count($result) == 2) {
            $data["Rows"] = [];
        } else {
            $data["Rows"] = $result[0]; //filas devueltas por el sp
        }
        $data["Properties"] = $result[1]; //propiedades de columnas
        $data["TotalRecords"] = (int)($result[1][0])->totalRecords;
        $data["Lista"] = $result[2][0];

        return $data;
    }

    public function productosQueNoEstanEnLista($palabras, $lista_id)
    {
        try {
            $query = $this->db->query("CALL ProductosQueNoEstanEnLista('$palabras'," . "$lista_id);")->getResult();
            return $query;
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    protected function afterUpdateListasPreciosProductosUpdatePedidosDetalles($data)
    {
        // $id = $data['id'][0];
        $lista_precio_id = $data['data']['lista_precio_id'];
        $producto_id = $data['data']['producto_id'];

        $query = "CALL AfterUpdateProductosUpdatePedidosDetalles($lista_precio_id,$producto_id);";

        $result = $this->query($query)->getResult();
        $message = $result[0]->message;

        if ($message != "EXITO") {
            log_message("error", "AfterUpdateProductosUpdatePedidosDetalles: " . $message);
        }

        return $result;
    }
}
