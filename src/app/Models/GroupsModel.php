<?php

namespace App\Models;


use Myth\Auth\Authorization\GroupModel as MythModel;

class GroupsModel extends MythModel
{
    protected $DBGroup          = 'default';
    protected $primaryKey       = 'id';
    protected $useAutoIncrement = true;
    protected $insertID         = 0;
    protected $useSoftDeletes   = false;
    protected $protectFields    = true;

    // Dates
    protected $useTimestamps = false;
    protected $dateFormat    = 'datetime';
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    // Validation
    protected $validationRules      = [];
    protected $validationMessages   = [];
    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = true;
    protected $beforeInsert   = [];
    protected $afterInsert    = [];
    protected $beforeUpdate   = [];
    protected $afterUpdate    = [];
    protected $beforeFind     = [];
    protected $afterFind      = [];
    protected $beforeDelete   = [];
    protected $afterDelete    = [];

    /**
     * Ejecuta la llamada el procedimiento almacenado para bucar los registros en la base de datos.
     * @return array
     */
    public function buscar($nombre = '', $orden = '', $pageIndex = 1, $pageSize = 10)
    {
        $query = "CALL BuscarGrupos('$nombre',"
            . "'$orden',"
            . "$pageIndex,"
            . "$pageSize,"
            . "@totalRecords,@firtsRecords,@lastRecords);";
        $result = $this->multiple_result($query);
        //El sp no devolvio filas
        if (count($result) == 2) {
            $data["Rows"] = [];
        } else {
            $data["Rows"] = $result[0]; //filas devueltas por el sp
        }
        $data["Properties"] = $this->SetPropertiesDataGrid($result[1]); //propiedades de columnas
        $data["TotalRecords"] = (int)($result[2][0])->totalRecords;

        return $data;
    }

    private function SetPropertiesDataGrid($properties){
        $result = [];
        foreach ($properties as $key => $value) {
            $value->Filtrable = $value->Filtrable == "1" ? true : false;
            $value->Ordenable = $value->Ordenable == "1" ? true : false;
            array_push($result, $value);
        }
        return $result;
    }
}
