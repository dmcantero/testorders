<?php

namespace App\Models;

use CodeIgniter\Model;

class ListasPreciosModel extends Model
{
    protected $DBGroup          = 'default';
    protected $table            = 'listas_precios';
    protected $primaryKey       = 'id';
    protected $useAutoIncrement = true;
    protected $insertID         = 0;
    protected $returnType       = 'array';
    protected $useSoftDeletes   = true;
    protected $protectFields    = true;
    protected $allowedFields    = ['id', 'nombre', 'descripcion', 'porcentaje', 'porcentaje_anterior', 'eliminado', 'mayorista', 'created_by', 'updated_by', 'deleted_by'];

    // Dates
    protected $useTimestamps = true;
    protected $dateFormat    = 'datetime';
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    // Validation
    protected $validationRules      = [];
    protected $validationMessages   = [];
    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = true;
    protected $beforeInsert   = ['setCreatedBy'];
    protected $afterInsert    = [];
    protected $beforeUpdate   = ['setUpdatedBy'];
    protected $afterUpdate    = ['afterUpdateListasPrecios'];
    protected $beforeFind     = [];
    protected $afterFind      = [];
    protected $beforeDelete   = ['setDeletedBy'];
    protected $afterDelete    = [];

    /**
     * Ejecuta la llamada el procedimiento almacenado para bucar los registros en la base de datos.
     * @return array
     */
    public function buscar($nombre = '', $mostrarEliminados = FALSE, $orden = '', $pageIndex = 1, $pageSize = 10)
    {
        $query = "CALL BuscarListasPrecios('$nombre',"
            . "$mostrarEliminados,"
            . "'$orden',"
            . "$pageIndex,"
            . "$pageSize,"
            . "@totalRecords,@firtsRecords,@lastRecords);";
        $result = $this->multiple_result($query);
        //El sp no devolvio filas
        if (count($result) == 2) {
            $data["Rows"] = [];
        } else {
            $data["Rows"] = $result[0]; //filas devueltas por el sp
        }
        $data["Properties"] = $result[1]; //propiedades de columnas
        $data["TotalRecords"] = (int)($result[2][0])->totalRecords;

        return $data;
    }

    protected function setCreatedBy(array $data)
    {
        $data['data']['created_by'] = user()->username;
        return $data;
    }

    protected function setUpdatedBy(array $data)
    {
        if($data['data']['eliminado'] == 1){
            $data['data']['deleted_by'] = user()->username;
        }else{
            $data['data']['updated_by'] = user()->username;
        }
        return $data;
    }

    protected function setDeletedBy(array $data)
    {
        $data['data']['deleted_by'] = user()->username;
        return $data;
    }

    /**
     * Primero actualiza el campo precio_venta de los registros de la tabla listas_precios_productos con el nuevo porcentaje para la lista
     * Luego actualiza los campos precio_unitario y subtotal de los pedidos detalles relacionados con la lista de precio.
     */
    protected function afterUpdateListasPrecios($data)
    {
        $id = $data['id'][0];
        $porcentaje = $data['data']['porcentaje'];
        $porcentaje_anterior = $data['data']['porcentaje_anterior'];

        $query = "CALL AfterUpdateListasPrecios($id,$porcentaje,$porcentaje_anterior);";

        $result = $this->query($query)->getResult();
        $message = $result[0]->message;

        if ($message != "EXITO") {
            log_message("error", "AfterUpdateListasPrecios: " . $message);
        }

        $query2 = "CALL AfterUpdateListasPreciosUpdatePedidosDetalles($id);";

        $resultPedidosDetalles = $this->query($query2)->getResult();
        $message = $resultPedidosDetalles[0]->message;

        if ($message != "EXITO") {
            log_message("error", "AfterUpdateListasPreciosUpdatePedidosDetalles: " . $message);
        }

        return $resultPedidosDetalles;
    }

    public function buscarProductosListas($codigo = '', $nombre = '', $marca_id = 0, $proveedor_id = 0, $orden = '', $pageIndex = 1, $pageSize = 10)
    {
        $query = "CALL BuscarProductosListas('$codigo',"
            . "'$nombre',"
            . "$marca_id,"
            . "$proveedor_id,"
            . "'$orden');";
        $result = $this->multiple_result($query);
        //El sp no devolvio filas
        // if (count($result) == 1) {
        //     $data["Rows"] = [];
        // } else {
        //     $data["Rows"] = $result[0]; //filas devueltas por el sp
        // }
        // $data["Properties"] = $result[1]; //propiedades de columnas
        // $data["TotalRecords"] = (int)($result[2][0])->totalRecords;
        $data["Rows"] = $result[0];
        $data["Listas"] = $result[1]; //propiedades de las listas

        return $data;
    }
}
