<?php

namespace App\Models;

use CodeIgniter\Model;

class PedidosDetallesModel extends Model
{
    protected $DBGroup          = 'default';
    protected $table            = 'pedidos_detalles';
    protected $primaryKey       = 'id';
    protected $useAutoIncrement = true;
    protected $insertID         = 0;
    protected $returnType       = 'array';
    protected $useSoftDeletes   = true;
    protected $protectFields    = true;
    protected $allowedFields    = ['id', 'pedido_id', 'producto_id', 'cantidad', 'cantidad_confirmada', 'precio_unitario', 'subtotal', 'subtotal_confirmado', 'porcentaje_descuento', 'created_by', 'updated_by', 'deleted', 'pedido_enlazado_id'];

    // Dates
    protected $useTimestamps = true;
    protected $dateFormat    = 'datetime';
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    // Validation
    protected $validationRules      = [];
    protected $validationMessages   = [];
    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = true;
    protected $beforeInsert   = ['setCreatedBy'];
    protected $afterInsert    = [];
    protected $beforeUpdate   = [];
    protected $afterUpdate    = [];
    protected $beforeFind     = [];
    protected $afterFind      = [];
    protected $beforeDelete   = [];
    protected $afterDelete    = [];

    /**
     * Ejecuta la llamada el procedimiento almacenado para bucar los registros en la base de datos.
     * @return array
     */
    public function buscar($nombre = '', $mostrarEliminados = FALSE, $orden = '', $pageIndex = 1, $pageSize = 10)
    {
        $query = "CALL BuscarListasPrecios('$nombre',"
            . "$mostrarEliminados,"
            . "'$orden',"
            . "$pageIndex,"
            . "$pageSize,"
            . "@totalRecords,@firtsRecords,@lastRecords);";
        $result = $this->multiple_result($query);
        //El sp no devolvio filas
        if (count($result) == 2) {
            $data["Rows"] = [];
        } else {
            $data["Rows"] = $result[0]; //filas devueltas por el sp
        }
        $data["Properties"] = $result[1]; //propiedades de columnas
        $data["TotalRecords"] = (int)($result[2][0])->totalRecords;

        return $data;
    }

    protected function setCreatedBy(array $data)
    {
        $data['data']['created_by'] = user()->username;
        return $data;
    }

    protected function setUpdatedBy(array $data)
    {
        // if($data['data']['eliminado'] == 1){
        //     $data['data']['deleted_by'] = user()->username;
        // }else{
        //     $data['data']['updated_by'] = user()->username;
        // }
        // return $data;
    }

    public function productosQueNoEstanEnPedido($palabras, $lista_id, $pedido_id)
    {
        try {
            $query = $this->db->query("CALL ProductosQueNoEstanEnPedido('$palabras'," . "$lista_id," . "$pedido_id);")->getResult();
            return $query;
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function deleteDetallePedido($data)
    {
        $producto_id = $data->producto_id;
        $pedido_id = $data->pedido_id;
        $cantidad_confirmada = $data->cantidad_confirmada;
        //restable el stock confirmado al stock del producto
        $query = "CALL DeleteDetallePedido($producto_id," . "$pedido_id," . "$cantidad_confirmada);";

        $result = $this->query($query)->getResult();
        $message = $result[0]->message;

        if ($message != "EXITO") {
            log_message("error", "Error al restablecer el stock: " .  $message);
        }

        return $result;
    }
}
