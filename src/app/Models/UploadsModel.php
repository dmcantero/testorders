<?php

namespace App\Models;

use App\Models\BaseModel;

class UploadsModel extends BaseModel
{
    protected $DBGroup          = 'default';
    protected $table            = 'uploads';
    protected $primaryKey       = 'id';
    protected $useAutoIncrement = true;
    protected $returnType       = \App\Entities\UploadEntity::class;
    protected $useSoftDeletes   = false;
    protected $protectFields    = true;
    protected $allowedFields    = ['id', 'original_name', 'new_name', 'description', 'extension', 'path', 'type', 'id_type', 'company_id', 'created_by', 'updated_by', 'deleted_by'];

    // Dates
    protected $useTimestamps = false;
    protected $dateFormat    = 'datetime';
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    // Validation
    protected $validationRules      = [];
    protected $validationMessages   = [];
    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = true;
    protected $beforeInsert   = [];
    protected $afterInsert    = [];
    protected $beforeUpdate   = [];
    protected $afterUpdate    = [];
    protected $beforeFind     = [];
    protected $afterFind      = [];
    protected $beforeDelete   = [];
    protected $afterDelete    = [];

    public function __construct()
    {
        parent::__construct($this->beforeInsert, $this->afterInsert, $this->beforeUpdate, $this->afterUpdate, $this->beforeDelete, $this->afterDelete, $this->beforeFind, $this->afterFind);
    }

    
}
