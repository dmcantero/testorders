<?php

namespace App\Models;

use App\Models\BaseModel;

class ProductsModel extends BaseModel
{
    protected $DBGroup          = 'default';
    protected $table            = 'products';
    protected $primaryKey       = 'id';
    protected $useAutoIncrement = true;
    protected $returnType       = \App\Entities\ProductEntity::class;
    protected $useSoftDeletes   = false;
    protected $protectFields    = true;
    protected $allowedFields    = ['id', 'code', 'name', 'description', 'purchase_price', 'sale_price', 'sale_price_unit', 'category_id', 'brand_id', 'supplier_id', 'min_package', 'image_url', 'active', 'stock', 'min_stock', 'bulk_quantity', 'company_id', 'created_by', 'updated_by', 'deleted_by'];

    // Dates
    protected $useTimestamps = false;
    protected $dateFormat    = 'datetime';
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    // Validation
    protected $validationRules      = [];
    protected $validationMessages   = [];
    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = true;
    protected $beforeInsert   = [];
    protected $afterInsert    = [];
    protected $beforeUpdate   = [];
    protected $afterUpdate    = [];
    protected $beforeFind     = [];
    protected $afterFind      = [];
    protected $beforeDelete   = [];
    protected $afterDelete    = [];

    public function __construct()
    {
        parent::__construct($this->beforeInsert, $this->afterInsert, $this->beforeUpdate, $this->afterUpdate, $this->beforeDelete, $this->afterDelete, $this->beforeFind, $this->afterFind);
    }


    /**
     * Ejecuta la llamada el procedimiento almacenado para bucar los registros en la base de datos.
     * @return array
     */
    public function search($brandId = 0, $categoryId = 0, $supplierId = 0, $name = '', $code = '', $orderBy = '', $pageIndex = 1, $pageSize = 10, $companyId = 1)
    {
        $companyId = session('company_id');
        $query = "CALL GetProducts($companyId,"
            . "$categoryId,"
            . "$supplierId,"
            . "$brandId,"
            . "'$name',"
            . "'$code',"
            . "'$orderBy',"
            . "$pageIndex,"
            . "$pageSize,"
            . "@totalRecords,@firtsRecords,@lastRecords);";
        $result = $this->multiple_result($query);
        //El sp no devolvio filas
        if (count($result) == 2) {
            $data["Rows"] = [];
        } else {
            $data["Rows"] = $result[0]; //filas devueltas por el sp
        }
        $data["Properties"] = $result[1]; //propiedades de columnas
        $data["TotalRecords"] = (int)($result[2][0])->totalRecords;

        return $data;
    }


    /**
     * Ejecuta la llamada el procedimiento almacenado para bucar los registros en la base de datos.
     * @return array
     */
    public function catalogueClient($categoryId = 0, $currency, $search = '', $available = 0, $offer = 0, $minPrice = 0, $maxPrice = 0,  $orderBy = '', $pageIndex = 1, $pageSize = 10, $companyId = 1)
    {
        $companyId = session('company_id');
        $query = "CALL GetCatalogueClient($companyId,"
            . "'$currency',"
            . "'$categoryId',"
            . "'$search',"
            . "$minPrice,"
            . "$maxPrice,"
            . "$offer,"
            . "$available,"
            . "'$orderBy',"
            . "$pageIndex,"
            . "$pageSize,"
            . "@totalRecords,@firtsRecords,@lastRecords);";
        $result = $this->multiple_result($query);

        $data['Data'] = $result[0] ?? [];
        $data["TotalRecords"] = (int)($result[1][0])->totalRecords;
        $data['FilteredRecords'] = sizeof($data['Data']);

        return $data;
    }

    /**
     * Ejecuta la llamada el procedimiento almacenado para bucar los registros en la base de datos.
     * @return array
     */
    public function getProductFeatures($productId, $companyId = 1)
    {
        try {
            $query = "CALL GetProductFeatures($companyId,$productId);";
            $result = $this->query($query)->getResult();
            return $result;
        } catch (\Throwable $th) {
            throw $th;
        }
    }
}
