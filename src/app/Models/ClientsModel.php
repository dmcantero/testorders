<?php

namespace App\Models;

use App\Models\BaseModel;

class ClientsModel extends BaseModel
{
    protected $DBGroup          = 'default';
    protected $table            = 'clients';
    protected $primaryKey       = 'id';
    protected $useAutoIncrement = true;
    protected $returnType       = \App\Entities\ClientEntity::class;
    protected $useSoftDeletes   = false;
    protected $protectFields    = true;
    protected $allowedFields    = ['id', 'user_id', 'identity_number', 'identity_type', 'business_name', 'contact_person', 'phone_number', 'email', 'address', 'billing_address', 'province_id', 'city_id', 'billing_province_id', 'billing_city_id', 'bonus', 'shipping_id', 'notes', 'has_required_data', 'company_id', 'created_by', 'updated_by', 'deleted_by'];

    // Dates
    protected $useTimestamps = false;
    protected $dateFormat    = 'datetime';
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    // Validation
    protected $validationRules      = [];
    protected $validationMessages   = [];
    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = true;
    protected $beforeInsert   = [];
    protected $afterInsert    = [];
    protected $beforeUpdate   = [];
    protected $afterUpdate    = [];
    protected $beforeFind     = [];
    protected $afterFind      = ['afterFind'];
    protected $beforeDelete   = [];
    protected $afterDelete    = [];

    public function __construct()
    {
        parent::__construct($this->beforeInsert, $this->afterInsert, $this->beforeUpdate, $this->afterUpdate, $this->beforeDelete, $this->afterDelete, $this->beforeFind, $this->afterFind);
    }

    /**
     * Ejecuta la llamada el procedimiento almacenado para bucar los registros en la base de datos.
     * @return array
     */
    public function search($username = '', $businessName = '', $email = '', $contactPerson = '', $orderBy = '', $pageIndex = 1, $pageSize = 10, $companyId = 1)
    {
        $query = "CALL GetClients($companyId,"
            . "'$username',"
            . "'$businessName',"
            . "'$email',"
            . "'$contactPerson',"
            . "'$orderBy',"
            . "$pageIndex,"
            . "$pageSize,"
            . "@totalRecords,@firtsRecords,@lastRecords);";
        $result = $this->multiple_result($query);
        //El sp no devolvio filas
        if (count($result) == 2) {
            $data["Rows"] = [];
        } else {
            $data["Rows"] = $result[0]; //filas devueltas por el sp
        }
        $data["Properties"] = $result[1]; //propiedades de columnas
        $data["TotalRecords"] = (int)($result[2][0])->totalRecords;

        return $data;
    }

    public function getByUserId($userId)
    {
        try {
            $query = $this->select('clients.id, clients.user_id, users.username, users.first_name, users.last_name, clients.business_name, clients.phone_number, clients.email, clients.address, clients.city_id, clients.province_id, clients.billing_province_id, clients.billing_city_id, clients.billing_address, clients.shipping_id')
                ->join('users', 'clients.user_id = users.id')
                ->where('clients.user_id', $userId)->first();

            return $query;
        } catch (\Throwable $th) {
            log_message('error', $th->__toString());
            throw $th;
        }
    }

    /**
     * Check if a user has required data in database
     */
    public function hasRequiredData($userId) : bool
    {
        try {
            $query = $this->select('*')
                ->join('users', 'clients.user_id = users.id')
                ->where(['clients.user_id' => $userId, 'clients.has_required_data' => 1])->countAllResults();

            return $query > 0;
        } catch (\Throwable $th) {
            log_message('error', $th->__toString());
            throw $th;
        }
    }

    public function afterFind(array $data)
    {
        $modelUsers = new UsersModel();
        $user = $modelUsers->where(['id' => $data['data']->user_id])->first();

        $data['data']->first_name = $user->first_name;
        $data['data']->last_name = $user->last_name;
        $data['data']->user_username = $user->username;
        $data['data']->user_email = $user->email;

        return $data;
    }
}
