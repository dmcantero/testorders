<?php

declare(strict_types=1);

namespace App\Models;

use CodeIgniter\Shield\Models\UserModel as ShieldUserModel;

class UsersModel extends ShieldUserModel
{
    protected function initialize(): void
    {
        parent::initialize();

        $this->allowedFields = [
            'username',
            'first_name',
            'last_name',
            'status',
            'status_message',
            'active',
            'last_active',
            'deleted_at',
            'company_id'
        ];
    }

    public function search($username, $email, $orderBy = '', $pageIndex = 1, $pageSize = 10, $companyId = 1)
    {
        $query = "CALL GetUsers($companyId,"
            . "'$username',"
            . "'$email',"
            . "'$orderBy',"
            . "$pageIndex,"
            . "$pageSize,"
            . "@totalRecords,@firtsRecords,@lastRecords);";
        $result = $this->multiple_result($query);
        //El sp no devolvio filas
        if (count($result) == 2) {
            $data["Rows"] = [];
        } else {
            $data["Rows"] = $result[0]; //filas devueltas por el sp
        }
        $data["Properties"] = $result[1]; //propiedades de columnas
        $data["TotalRecords"] = (int)($result[2][0])->totalRecords;

        return $data;
    }

    private  function multiple_result($sql)
    {
        if (empty($sql))
            return NULL;

        $i = 0;
        $set = [];


        if (mysqli_multi_query($this->db->connID, $sql)) {
            do {
                mysqli_next_result($this->db->connID);

                if (FALSE != $result = mysqli_store_result($this->db->connID)) {
                    $row_id = 0;

                    while ($row = $result->fetch_object()) {
                        $set[$i][$row_id] = $row;
                        $row_id++;
                    }
                }

                $i++;
            } while (mysqli_more_results($this->db->connID));
        }

        return $set;
    }
}
