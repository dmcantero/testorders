<?= $this->extend('Email/Layout/Main') ?>
<?= $this->section('content') ?>
<div>
    <table style="width: 100%; padding: 0px">
        <tr>
            <td style="text-align: center; width: 100%">
                <h3>¡Hemos recibido su orden de pedido!</h3>
            </td>
        </tr>
        <tr>
            <td style="text-align: center; width: 100%">
                <p> Hola, <?= $order->client_fullname ?> </p> <br>
                <p> Datos de su pedido </p>
                <p>
                    <ul style="list-style: none;">
                        <li><b>Número: </b> <?= $order->number ?></li>
                        <li><b>Items: </b> <?= $order->items ?></li>
                        <li><b>Total: </b> <?= $order->total_net ?></li>
                    </ul>
                </p>
            </td>
        </tr>
        <tr>
            <td style="text-align: center; width: 100%">
                <p> En breve lo estaremos procesando y nos pondremos en contacto para confirmar las cantidades solicitadas. </p>
                <p> Muchas gracias por confiar en nosotros.</p>
            </td>
        </tr>
    </table>
</div>
<?= $this->endSection() ?>