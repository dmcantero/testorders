<?= $this->extend('Catalogue/Layout/Main') ?>
<?= $this->section('content') ?>
<main class="main">
    <?= $this->include('/Catalogue/Home/_Banner') ?>
    <!-- End PageHeader -->
    <div class="page-content mb-10 pb-2">
        <div class="container">
            <div class="row main-content-wrap gutter-lg" id="app-myorders">
                <div class="col-lg-12 main-content">
                    <!-- Start filters -->
                    <nav class="toolbox sticky-toolbox sticky-content fix-top">
                        <input type="hidden" value="<?= $clientId ?>" id="clientId" name="clientId" />
                        <div class="row">
                            <div class="col-xl-3 col-lg-3 col-12">
                                <textinput name="date_from" type="date" label="Fecha desde" v-model="filters.date_from"></textinput>
                            </div>
                            <div class="col-xl-3 col-lg-3 col-12">
                                <textinput name="date_until" type="date" label="Fecha hasta" v-model="filters.date_until"></textinput>
                            </div>
                            <div class="col-xl-3 col-lg-3 col-12">
                                <label for="status" class="col-form-label">Estado</label>
                                <select name="status" class="form-control" v-model="filters.status">
                                    <option value="">--TODOS--</option>
                                    <option value="INPROGRESS">EN PROGRESO</option>
                                    <option value="ISSUED">EMITIDO</option>
                                    <option value="CONFIRMED">CONFIRMADO</option>
                                    <option value="CANCELLED">CANCELADO</option>
                                </select>
                            </div>
                            <div class="col-xl-3 col-lg-3 col-12">
                                <label for="currency" class="col-form-label">Moneda</label>
                                <select name="currency" class="form-control" v-model="filters.currency">
                                    <option value="ARS">ARS</option>
                                    <option value="USD">USD</option>
                                </select>
                            </div>
                            <div class="col-xl-3 col-lg-3 col-12">
                                <a href="#!" @click="getOrders" class="btn btn-outline btn-block btn-secondary btn-rounded btn-icon-right">
                                    <i class="fa fa-filter"></i> Filtrar
                                </a>
                            </div>
                        </div>
                    </nav>
                    <!-- End filters  -->
                    <!-- Start Orders list -->
                    <ul class="responsive-table" v-if="orders.length > 0">
                        <li class="table-header">
                            <div class="col table-col-2">Número</div>
                            <div class="col table-col-2">Fecha</div>
                            <div class="col table-col-1">Items</div>
                            <div class="col table-col-3">Total</div>
                            <div class="col table-col-3">Estado</div>
                            <div class="col table-col-1"></div>
                        </li>
                        <li class="table-row" v-for="(item, index) in orders" :key="index">
                            <div class="col table-col-2" data-label="Numero">
                                {{ item.number }}
                            </div>
                            <div class="col table-col-2 text-center" data-label="Fecha">{{ item.date }}</div>
                            <div class="col table-col-1 text-center" data-label="Items">{{ item.items }}</div>
                            <div class="col table-col-3 text-center" data-label="Total">{{ item.total_net }}</div>
                            <div class="col table-col-3 text-center" data-label="Estado">{{ item.status_name }}</div>
                            <div class="col table-col-1 text-center" data-label="">
                                <a :href="`/reports/order/${item.Id}/${item.number}/true`" title="Descargar nota de pedido">
                                    <i class="fa fa-download"></i>
                                </a>
                                <a :href="`/reports/order/${item.Id}/${item.number}`" target="_blank" title="Abrir nota de pedido">
                                    <i class="fa fa-eye"></i>
                                </a>
                            </div>
                        </li>
                    </ul>
                    <div v-else>
                        <h2 class="text-center"><small>No hay ordenes para mostrar</small></p>
                    </div>
                    <!-- End Orders list -->
                </div>
            </div>
        </div>
    </div>
</main>
<!-- End of Main Content -->

<?= $this->endSection() ?>

<?= $this->section('scripts') ?>
<script type="text/javascript" src="<?= base_url("dist/js/catalogue/orders.js"); ?>"></script>
<?= $this->endSection('scripts') ?>