<?= $this->extend('Catalogue/Layout/Main') ?>
<?= $this->section('content') ?>

<main class="main cart-page">
    <nav class="breadcrumb-nav">
        <div class="container">
            <ul class="breadcrumb">
                <li><a href="demo1.html"><i class="d-icon-home"></i></a></li>
                <li>catalogue</li>
            </ul>
        </div>
    </nav>
    <div class="page-content">
        <div class="container">
            <div class="main-content">
                <div class="row d-flex justify-content-center">
                    <div class="col-12 d-flex justify-content-center">
                        <div class="alert alert-message alert-light alert-primary alert-link mb-4">
                            <h4 class="alert-title">Pedido recibido correctamente</h4>
                            <h5 class="alert-title">N° de pedido:  <?= $numero; ?> </h5>
                            <p>Muchas gracias por su pedido. A la brevedad nos pondremos en contacto para finalizar el proceso y coordinar el envío/retiro del mismo. </p>
                            <p> Puede hacer el seguimiento del mismo ingresando en
                                <a href="<?= base_url('catalogue/myorders') ?>">mis pedidos</a>.
                            </p>
                            <button type="button" class="btn btn-link btn-close">
                                <i class="d-icon-times"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<!-- End of Main Content -->
<?= $this->endSection() ?>