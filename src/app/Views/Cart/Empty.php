<?= $this->extend('Catalogue/Layout/Main') ?>
<?= $this->section('content') ?>

<main class="main cart-page">
    <nav class="breadcrumb-nav">
        <div class="container">
            <ul class="breadcrumb">
                <li><a href="demo1.html"><i class="d-icon-home"></i></a></li>
                <li>cart</li>
            </ul>
        </div>
    </nav>
    <div class="page-content cart-empty">
        <div class="container">
            <div class="main-content">
                <i class="d-icon-bag cart-icon"></i>
                <h2 class="cart-descri">No products added to the cart</h2>
                <a class="btn btn-primary btn-rounded" href="shop.html">
                    GO SHOP
                </a>
            </div>
        </div>
    </div>
</main>

<!-- End of Main Content -->
<?= $this->endSection() ?>