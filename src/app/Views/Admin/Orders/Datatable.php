<div id="app-datatable-orders" class="container-fluid">
    <div class="card">
        <div class="card-header main">
            <div class="row">
                <div class="col-xl-4 col-lg-4 col-md-4 col-9">
                    <h5>{{title}}</h5>
                </div>
                <div class="col-xl-8 col-lg-8 col-md-8 col-3 d-flex justify-content-end">
                    <div class="btn-group">
                        <button type="button" class="btn btn-default dropdown-toggle text-white" data-toggle="dropdown">
                            <i class="bi bi-three-dots-vertical"></i>
                        </button>
                        <ul class="dropdown-menu pull-left" role="menu">
                            <li>
                                <a class="dropdown-item" href="Exportar">
                                    <i class="bi bi-download"></i> Exportar
                                </a>
                            </li>
                            <li>
                                <a class="dropdown-item" href="Importar">
                                    <i class="bi bi-upload"></i> Importar
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="card mb-2" id="order-filters">
                <div class="card-body">
                    <ValidationForm @submit="filter" :validation-schema="vfilterschema" v-slot="{ errors, dirty, valid, invalid,  isSubmitting }" id="formFiltersOrders">
                        <div class="row">
                            <div class="col-xl-2 col-lg-2 col-md-2 col-12">
                                <textinput name="date_from" type="date" label="Fecha desde" v-model="defaultParameters.date_from"></textinput>
                            </div>
                            <div class="col-xl-2 col-lg-2 col-md-3 col-12">
                                <textinput name="date_until" type="date" label="Fecha hasta" v-model="defaultParameters.date_until"></textinput>
                            </div>
                            <div class="col-xl-2 col-lg-2 col-md-3 col-12">
                                <label for="currency" class="col-form-label">Moneda</label>
                                <select name="currency" class="form-control" v-model="defaultParameters.currency">
                                    <option value="USD">USD</option>
                                    <option value="ARS">ARS</option>
                                </select>
                            </div>
                            <div class="col-xl-2 col-lg-2 col-md-3 col-12">
                                <label for="status" class="col-form-label">Estado</label>
                                <select name="status" class="form-control" v-model="defaultParameters.status">
                                    <option value="">--TODOS--</option>
                                    <option value="INPROGRESS">EN PROGRESO</option>
                                    <option value="ISSUED">EMITIDO</option>
                                    <option value="CONFIRMED">CONFIRMADO</option>
                                    <option value="CANCELLED">CANCELADO</option>
                                </select>
                            </div>
                            <div class="col-xl-4 col-lg-4 col-4 col-12">
                                <genericselect name="client_id" v-model="defaultParameters.client_id" label="Cliente" url-path="/api/clients/select"></genericselect>
                            </div>
                        </div>
                        <div class="row d-flex justify-content-end">
                            <div class="col-4 d-flex justify-content-end">
                                <button class="btn btn-primary" type="submit">
                                    <i class="bi bi-filter"></i> Filtrar
                                </button>
                            </div>
                        </div>
                    </ValidationForm>
                </div>
            </div>
            <datagrid ref="datatable" :urlrequest="urlRequest" :title="title" :defaultParameters="defaultParameters" :writeurlrequest="writeUrlRequest" :actions="actions" :stylesbyrowcallback="setRowBackground">
            </datagrid>
        </div>
    </div>
</div>
<?= $this->section('scripts') ?>
<script type="text/javascript" src='<?= base_url("dist/js/admin/orders/datatable.js"); ?>'></script>
<?= $this->endSection('scripts') ?>