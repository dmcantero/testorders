<?php
$router = service('router');
$data['controller']  = $router->controllerName();
$data['method'] = $router->methodName();
?>
<?= $this->extend('Admin/Layout/Main') ?>
<?= $this->section('content') ?>
<div class="row" id="app-categories">
    <div class="col-xl-4 col-lg-4 col-md-12 col-12" data-aos="fade-up">
        <?= view('Admin/Categories/Form', ['id' => 0]) ?>
    </div>
    <div class="col-xl-8 col-lg-8 col-md-12 col-12" data-aos="fade-up">
        <?= view('Admin/Categories/Datatable', $data) ?>
    </div>
    <dialogconfirmation :id="'modalDelete'" ref="modalDelete">
        <template v-slot:title> Eliminar categoría</template>
        <template v-slot:body> ¿Desea eliminar esta categoría?.</template>
    </dialogconfirmation>
</div>
<?= $this->endSection(); ?>

<?= $this->section('scripts') ?>
<script type="text/javascript" src="<?= base_url("dist/js/admin/categories/datatable.js"); ?>"></script>
<?= $this->endSection('scripts') ?>