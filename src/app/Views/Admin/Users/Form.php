<?= $this->extend('Admin/Layout/Main') ?>
<?= $this->section('content') ?>
<div id="app-form-users" class="container-fluid" data-aos="fade-up">
    <div class="card">
        <div class="card-header main">
            <h5 class="card-title">{{ id == 0 ? "Crear Usuario" : "Editar Usuario"}}</h5>
        </div>
        <ValidationForm @submit="save" :validation-schema="action == 'create' ? vschema : vschemaedit" v-slot="{ errors, dirty, valid, invalid,  isSubmitting }">
            <div class="card-body">
                <div v-if="errorMessages.length" class="alert alert-danger">
                    <ul>
                        <li v-for="(error, key) in errorMessages">{{error}}</li>
                    </ul>
                </div>
                <input type="hidden" id="id" name="id" value="<?= $id ?>">
                <div class="row">
                    <div class="col-xl-3 col-lg-3 col-md-4 col-12">
                        <textinput type="text" name="first_name" label="Nombre" v-model="datavm.first_name"></textinput>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-4 col-12">
                        <textinput type="text" name="last_name" label="Apellido" v-model="datavm.last_name"></textinput>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-4 col-12">
                        <textinput type="email" name="email" label="Email" v-model="datavm.email"></textinput>
                    </div>
                </div>
                <div class="row">
                    <template v-if="action == 'create'">
                        <div class="col-xl-3 col-lg-3 col-md-3 col-12">
                            <textinput type="text" name="username" label="Nombre de usuario" v-model="datavm.username"></textinput>
                        </div>
                        <div class="col-xl-3 col-lg-3 col-md-3 col-12">
                            <textinput :type="`${showpassword ? 'text' : 'password'}`" name="password" label="Contraseña" v-model="datavm.password"></textinput>
                            <div class="row">
                                <div class="col-xl-4 col-lg-4 col-md-4 col-12">
                                    <a href="#!" class="small" @click="showpassword = !showpassword">{{ showpassword ? 'Ocultrar' : 'Mostrar'}}</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-3 col-md-3 col-12">
                            <textinput type="password" name="password_confirm" label="Reingrese Contraseña" v-model="datavm.password_confirm"></textinput>
                        </div>
                    </template>
                    <template v-else>
                        <div class="col-xl-3 col-lg-3 col-md-3 col-12">
                            <textinput type="text" name="username" label="Nombre de usuario" v-model="datavm.username" disabled="true"></textinput>
                        </div>
                    </template>
                    <div class="col-xl-3 col-lg-3 col-md-3 col-12">
                        <label for="group" class="col-form-label">Rol</label>
                        <field v-model="datavm.group" id="group" name="group" class="form-control" as="select" v-scop="{errorMessage}" :disabled="action == 'update'">
                            <option value="admin">Administrador</option>
                            <option value="user">Cliente</option>
                        </field>
                        <validationerror as="span" name="group" class="d-inline-block invalid-feedback"></validationerror>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-2 col-lg-2 col-md-2 col-6" v-if="action == 'update'">
                        <customswitch name="active" label="Activo" v-model="datavm.active"></customswitch>
                    </div>
                    <div class="col-xl-2 col-lg-2 col-md-2 col-6" v-if="action == 'create'">
                        <customswitch name="notify" label="Notificar por email" v-model="datavm.notify"></customswitch>
                    </div>
                    <div class="col-xl-2 col-lg-2 col-md-2 col-6">
                        <label for="forceresetpassword" class="col-form-label">
                            <input type="checkbox" name="forceresetpassword" id="forceresetpassword" v-model="forceresetpassword" v-bind="field" :disabled="action == 'create'" />
                            Forzar cambio de contraseña
                        </label>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <div class="row">
                    <div class="col-6 d-flex justify-content-start">
                        <a href="#!" onclick="history.back()">Volver</a>
                    </div>
                    <div class="col-6 d-flex justify-content-end">
                        <button type="submit" :disabled="isSubmitting" :class="{'ld-ext-right': true, 'btn btn-success': true, 'running': isSubmitting }">
                            <i class="bi bi-check-lg"></i> Guardar
                            <div class="ld">
                                <img src="<?= base_url('dist/images/loading.gif'); ?>" style="font-size:1em" />
                            </div>
                        </button>
                    </div>
                </div>
            </div>
        </ValidationForm>
    </div>
</div>
<?= $this->endSection(); ?>

<?= $this->section('scripts') ?>
<script type="text/javascript" src='<?= base_url("dist/js/admin/users/form.js"); ?>'></script>
<?= $this->endSection('scripts') ?>