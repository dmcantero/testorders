<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-dark sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?= base_url('admin'); ?>">
        <img src="<?= base_url('dist/images/logo.jpg') ?>" alt="Logo" class="img-fluid" width="200">
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item active">
        <a class="nav-link" href="<?= base_url("admin"); ?>">
            <i class="bi bi-house"></i>
            <span>Inicio</span>
        </a>
    </li>
    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
        Catálogo
    </div>
    <!-- Nav Item - Charts -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseProducts" aria-expanded="true" aria-controls="collapseProducts">
            <i class=""></i>
            <span>Productos</span>
        </a>
        <div id="collapseProducts" class="collapse" aria-labelledby="headingPages" data-parent="#collapseProducts">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href="<?= base_url('admin/products') ?>">Todos los productos</a>
                <a class="collapse-item" href="<?= base_url('admin/products/new') ?>">Crear nuevo</a>
                <a class="collapse-item" href="<?= base_url('admin/categories') ?>">Categorias</a>
                <a class="collapse-item" href="<?= base_url('admin/features') ?>">Características</a>
            </div>
        </div>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="<?= base_url("admin/clients") ?>">
            <i class=""></i>
            <span>Clientes</span>
        </a>
    </li>
    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
        Pedidos
    </div>
    <!-- Nav Item - Charts -->
    <li class="nav-item">
        <a class="nav-link" href="<?= base_url("admin/orders") ?>">
            <i class=""></i>
            <span>Notas de pedido</span>
        </a>
    </li>
    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
        Administración
    </div>
    <li class="nav-item">
        <a class="nav-link" href="<?= base_url("admin/settings") ?>">
            <i class=""></i>
            <span>Ajustes</span>
        </a>
    </li>
    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUsuarios" aria-expanded="true" aria-controls="collapsePages">
            <i class=""></i>
            <span>Usuarios</span>
        </a>
        <div id="collapseUsuarios" class="collapse" aria-labelledby="headingPages" data-parent="#collapseUsuarios">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href="<?= base_url('admin/users') ?>">Listado</a>
                <a class="collapse-item" href="<?= base_url('admin/users/new') ?>">Crear nuevo</a>
            </div>
        </div>
    </li>
    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle">
        </button>
    </div>
</ul>
<!-- End of Sidebar -->