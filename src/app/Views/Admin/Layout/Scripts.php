<!-- Custom scripts for all pages-->
<script src="<?= base_url('dist/js/admin/vendor.js') ?>"></script>

<?= $this->section('scripts') ?>

<script>
    $(document).on({
        'show.bs.modal': function() {
            var zIndex = 1040 + (10 * $('.modal:visible').length);
            $(this).css('z-index', zIndex);
            setTimeout(function() {
                $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
            }, 0);
        },
        'shown.bs.modal': function(e) {
            $('input:text:visible:first', e.target).focus();
        },
        'hidden.bs.modal': function() {
            if ($('.modal:visible').length > 0) {
                // restore the modal-open class to the body element, so that scrolling works
                // properly after de-stacking a modal.
                setTimeout(function() {
                    $(document.body).addClass('modal-open');
                }, 0);
            }
        }
    }, '.modal');

    var body = $('body');
    $(document).ready(function() {
        $(document).ajaxStart(function() {
            body.addClass("loading");
        });
        $(document).ajaxStop(function() {
            body.removeClass("loading");
        });
    });
</script>
<?= $this->endSection(); ?>