<!doctype html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title><?= getenv("TITLE_PROJECT") ?></title>
    <?= $this->include('Admin/Layout/Css') ?>
    <style>
        #loading {
            display: none;
            position: fixed;
            z-index: 999999;
            top: 0;
            left: 0;
            height: 100%;
            width: 100%;
            opacity: 0.5;
            background: #000 url('<?php echo base_url("dist/images/loading.gif"); ?>') no-repeat;
            background-position: center center;
            background-size: 5%;
        }

        /* When the body has the loading class, we turn
               the scrollbar off with overflow:hidden */
        body.is_loading {
            overflow: hidden;
        }

        /* Anytime the body has the loading class, our
               modal element will be visible */
        body.is_loading #loading {
            display: block;
        }
    </style>
    <script>
        var APP = APP || {};
        (function(app) {
            app.BASEURL = '<?= base_url(); ?>';
            app.ADMINBASEURL = '<?= base_url("admin"); ?>';
        })(APP);
    </script>
</head>

<body>
    <div id="loading"></div>
    <div id="wrapper">
        <?= $this->include('Admin/Layout/Sidebar') ?>
        <div id="content-wrapper" class="d-flex flex-column">
            <?= $this->include('Admin/Layout/Header') ?>
            {{breadcrumbs}}
            <?= $this->renderSection('content') ?>
            <?= $this->include('Admin/Layout/Footer') ?>
        </div>
        <?= $this->include('Admin/Layout/Scripts') ?>
        <?= $this->renderSection('scripts') ?>
    </div>
</body>

</html>