<?= $this->extend('Admin/Layout/Main') ?>
<?= $this->section('content') ?>
<div id="app-form-clients" class="container-fluid">
    <div class="card" data-aos="fade-up">
        <div class="card-header main">
            <h5 class="card-title">{{title}}</h5>
        </div>
        <ValidationForm @submit="save" :validation-schema="vschema" v-slot="{ errors, dirty, valid, invalid,  isSubmitting }">
            <div class="card-body">
                <div v-if="errorMessages.length" class="alert alert-danger">
                    <ul>
                        <li v-for="(error, key) in errorMessages">{{error}}</li>
                    </ul>
                </div>
                <input type="hidden" id="id" name="id" value="<?= $id ?>">
                <textinput id="user_id" name="user_id" v-model="datavm.user_id" type="hidden"></textinput>
                <div class="row">
                    <div class="col-xl-3 col-lg-3 col-md-6 col-12">
                        <textinput id="first_name" name="first_name" v-model="datavm.first_name" label="Nombre"></textinput>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-6 col-12">
                        <textinput id="last_name" name="last_name" v-model="datavm.last_name" label="Apellido"></textinput>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-6 col-12">
                        <textinput id="business_name" name="business_name" v-model="datavm.business_name" label="Nombre comercial"></textinput>
                    </div>
                    <div class="col-xl-2 col-lg-2 col-md-3 col-12">
                        <label for="identity_type" class="col-form-label">Tipo identificación</label>
                        <select id="identity_type" name="identity_type" v-model="datavm.identity_type" class="form-control">
                            <option value="">--Seleccione--</option>
                            <option value="DNI">DNI</option>
                            <option value="CUIT">CUIT</option>
                        </select>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-12">
                        <textinput id="identity_number" name="identity_number" v-model="datavm.identity_number" label="Num. identificación"></textinput>
                    </div>
                </div>
                <fieldset>
                    <legend>Datos de contacto</legend>
                    <div class="row">
                        <div class="col-xl-4 col-lg-4 col-md-4 col-12">
                            <textinput id="contact_person" name="contact_person" v-model="datavm.contact_person" label="Persona contacto"></textinput>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-4 col-12">
                            <textinput id="phone_number" name="phone_number" v-model="datavm.phone_number" label="Teléfono"></textinput>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-4 col-12">
                            <textinput id="email" name="email" v-model="datavm.email" label="Email de contacto"></textinput>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xl-4 col-lg-4 col-md-4 col-12">
                            <genericselect id="province_id" name="province_id" v-model="datavm.province_id" label="Provincia" url-path="api/province/select"></genericselect>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-4 col-12">
                            <genericselect id="city_id" name="city_id" v-model="datavm.city_id" label="Ciudad" :url-path="`api/province/cities/${datavm.province_id}`"></genericselect>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-4 col-12">
                            <textinput id="address" name="address" v-model="datavm.address" label="Dirección"></textinput>
                        </div>
                    </div>
                </fieldset>
                <fieldset>
                    <legend>Datos de facturación</legend>
                    <div class="row">
                        <div class="col-xl-2 col-lg-4 col-md-4 col-12">
                            <genericselect id="billing_province_id" name="billing_province_id" v-model="datavm.billing_province_id" label="Provincia" url-path="api/province/select"></genericselect>
                        </div>
                        <div class="col-xl-2 col-lg-4 col-md-4 col-12">
                            <genericselect id="billing_city_id" name="billing_city_id" v-model="datavm.billing_city_id" label="Ciudad" :url-path="`api/province/cities/${datavm.billing_province_id}`"></genericselect>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-4 col-12">
                            <textinput id="billing_address" name="billing_address" v-model="datavm.billing_address" label="Dirección"></textinput>
                        </div>
                        <div class="col-xl-2 col-lg-4 col-md-4 col-12">
                            <genericselect id="shipping_id" name="shipping_id" v-model="datavm.shipping_id" label="Expreso de envío" url-path="api/shipping/select"></genericselect>
                        </div>
                        <div class="col-xl-2 col-lg-4 col-md-4 col-12">
                            <moneyinput id="bonus" name="bonus" v-model="datavm.bonus" label="Bonificación" symbol="%"></moneyinput>
                        </div>
                    </div>
                </fieldset>
                <fieldset>
                    <legend>Datos de usuario</legend>
                    <div class="row">
                        <div class="col-xl-4 col-lg-4 col-md-4 col-12">
                            <textinput id="user_email" name="user_email" v-model="datavm.user_email" label="Email de acceso" disabled="disabled"></textinput>
                        </div>
                        <!-- <div class="col-xl-4 col-lg-4 col-md-4 col-12" v-if="action == 'create'">
                            <textinput id="password_user" name="password_user" v-model="datavm.password_user" label="Contraseña" type="password" autocomplete="password_user" ></textinput>
                        </div> -->
                        <div class="col-xl-4 col-lg-4 col-md-4 col-12">
                            <textinput id="user_username" name="user_username" v-model="datavm.user_username" label="Nombre de usuario" disabled="disabled"></textinput>
                        </div>
                    </div>
                </fieldset>
                <div class="row">
                    <div class="col-12">
                        <textareaeditor v-model="datavm.notes" name="notes" label="Notas"></textareaeditor>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <div class="row">
                    <div class="col-6 d-flex justify-content-start">
                        <a href="<?= base_url('admin/clients'); ?>">Volver al listado</a>
                    </div>
                    <div class="col-6 d-flex justify-content-end">
                        <button type="submit" :disabled="isSubmitting" :class="{'ld-ext-right': true, 'btn btn-success': true, 'running': isSubmitting }">
                            <i class="bi bi-check-lg"></i> Guardar y volver
                            <div class="ld">
                                <img src="<?= base_url('dist/images/loading.gif'); ?>" style="font-size:1em" />
                            </div>
                        </button>
                    </div>
                </div>
            </div>
        </ValidationForm>
    </div>
</div>
<?= $this->endSection(); ?>

<?= $this->section('scripts') ?>
<script type="text/javascript" src="<?= base_url("dist/js/admin/clients/form.js"); ?>"></script>
<?= $this->endSection('scripts') ?>