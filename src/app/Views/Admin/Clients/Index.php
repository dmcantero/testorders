<?php
$router = service('router');
$data['controller']  = $router->controllerName();
$data['method'] = $router->methodName();
$data['module'] = "Clients";
$data['area']   = strtolower("Admin");
?>
<?= $this->extend('Admin/Layout/Main') ?>
<?= $this->section('content') ?>
<div data-aos="fade-up">
    <?= view('Admin/Clients/Datatable', $data) ?>
</div>
<?= $this->endSection(); ?>