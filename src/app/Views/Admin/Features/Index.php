<?php
$router = service('router');
$data['controller']  = $router->controllerName();
$data['method'] = $router->methodName();
$data['module'] = "features";
$data['area']   = strtolower("Admin");
?>
<?= $this->extend('Admin/Layout/Main') ?>
<?= $this->section('content') ?>
<div class="row" id="app-features">
    <div class="col-xl-4 col-lg-4 col-md-12 col-12" data-aos="fade-up">
        <?= view('Admin/Features/Form') ?>
    </div>
    <div class="col-xl-8 col-lg-8 col-md-12 col-12" data-aos="fade-up">
        <?= view('Admin/Features/Datatable', $data) ?>
    </div>
    <dialogconfirmation :id="'modalDelete'" ref="modalDelete">
        <template v-slot:title> Eliminar atributo</template>
    </dialogconfirmation>
    <dynamicmodalform :schema="formValueSchema" :vschema="vschemaValue" :formdata="formValueData" idmodal="ModalFeatureValue" ref="modalValue" @submit="saveFeatureValue">
        <template v-slot:title>{{ formValueData.id == 0 ? 'Crear valor de atributo' : 'Editar valor de atributo' }}</template>
    </dynamicmodalform>
</div>
<?= $this->endSection(); ?>

<?= $this->section('scripts') ?>
<script type="text/javascript" src='<?= base_url("dist/js/admin/features/datatable.js"); ?>'></script>
<?= $this->endSection('scripts') ?>