<div id="app-form-features" class="container">
    <div class="card">
        <div class="card-header main">
            <h5 class="card-title">{{ id == 0 ? "Crear Atributo" : "Editar Atributo" }}</h5>
        </div>
        <ValidationForm @submit="save" :validation-schema="vschema" v-slot="{ errors, dirty, valid, invalid,  isSubmitting }">
            <div class="card-body">
                <input type="hidden" id="id" v-model="id" name="id">
                <div class="row">
                    <div class="col-12">
                        <textinput id="name" name="name" v-model="datavm.name" label="Nombre"></textinput>
                    </div>
                </div>
                <div class="row" v-if="id != 0">
                    <div class="col-12">
                        <div class="row">
                            <div class="col-12">
                                <button type="button" class="btn btn-primary" @click="openModalValue(0)">
                                    <i class="bi bi-plus-circle"></i> Nuevo valor
                                </button>
                            </div>
                        </div>
                        <table class="table table-bordered table-sm">
                            <thead>
                                <tr>
                                    <th style="width: 70%">Valor</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="(item, key) in datavm.values">
                                    <td>
                                        {{ item.value }}
                                    </td>
                                    <td>
                                        <div class="row justify-content-center">
                                            <div class="col-12 text-center">
                                                <button type="button" title="Editar" class="btn btn-sm" @click="openModalValue(item.id)">
                                                    <i class="bi bi-pencil"></i>
                                                </button>
                                            </div>
                                            <!-- <div class="col-6 text-center">
                                                <button type="button" title="Eliminar" class="btn btn-sm">
                                                    <i class="bi bi-trash text-danger"></i>
                                                </button>
                                            </div> -->
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <div class="row">
                    <div class="col-6 d-flex justify-content-start">
                        <a href="#!" onclick="history.back()">Volver</a>
                    </div>
                    <div class="col-6 d-flex justify-content-end">
                        <button type="submit" :disabled="isSubmitting" :class="{'ld-ext-right': true, 'btn btn-success': true, 'running': isSubmitting }">
                            <i class="bi bi-check-lg"></i> Guardar
                            <div class="ld">
                                <img src="<?= base_url('dist/images/loading.gif'); ?>" style="font-size:1em" />
                            </div>
                        </button>
                    </div>
                </div>
            </div>
        </ValidationForm>
    </div>
</div>