<header>
    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <a class="navbar-brand" href="<?= base_url(); ?>">
            <img src="<?= base_url('dist/images/logo.jpg') ?>" width="120" height="30" class="d-inline-block align-top" alt="">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="<?= base_url(); ?>">Home <span class="sr-only">(current)</span></a>
                </li>
                <?php if (auth()->loggedIn()) { ?>
                    <li class="nav-item dropdown nav-item dropdown ml-auto">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <?= auth()->user()->username; ?>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="<?= auth()->logout(); ?>">Salir</a>
                        </div>
                    </li>
                <?php } else { ?>
                    <li class="nav-item nav-item ml-auto">
                        <a class="nav-link" href="<?= base_url('login') ?>">Ingresar</a>
                    </li>
                <?php } ?>
            </ul>
        </div>
    </nav>
</header>