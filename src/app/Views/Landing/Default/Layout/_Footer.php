 <!-- FOOTER -->
 <footer class="container">
     <p class="float-right"><a href="#">Back to top</a></p>
     <p>© <?= date('Y', time()) . ' ' . env('TITLE_PROJECT'); ?> ·</p>
 </footer>