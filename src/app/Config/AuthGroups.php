<?php

declare(strict_types=1);

namespace Config;

use CodeIgniter\Shield\Config\AuthGroups as ShieldAuthGroups;

class AuthGroups extends ShieldAuthGroups
{
    /**
     * --------------------------------------------------------------------
     * Default Group
     * --------------------------------------------------------------------
     * The group that a newly registered user is added to.
     */
    public string $defaultGroup = 'user';

    /**
     * --------------------------------------------------------------------
     * Groups
     * --------------------------------------------------------------------
     * An associative array of the available groups in the system, where the keys
     * are the group names and the values are arrays of the group info.
     *
     * Whatever value you assign as the key will be used to refer to the group
     * when using functions such as:
     *      $user->addGroup('superadmin');
     *
     * @var array<string, array<string, string>>
     *
     * @see https://github.com/codeigniter4/shield/blob/develop/docs/quickstart.md#change-available-groups for more info
     */
    public array $groups = [
        'superadmin' => [
            'title'       => 'Super Admin',
            'description' => 'Complete control of the site.',
        ],
        'admin' => [
            'title'       => 'Admin',
            'description' => 'Day to day administrators of the site.',
        ],
        'developer' => [
            'title'       => 'Developer',
            'description' => 'Site programmers.',
        ],
        'user' => [
            'title'       => 'User',
            'description' => 'General users of the site. Often customers.',
        ],
        'beta' => [
            'title'       => 'Beta User',
            'description' => 'Has access to beta-level features.',
        ],
    ];

    /**
     * --------------------------------------------------------------------
     * Permissions
     * --------------------------------------------------------------------
     * The available permissions in the system.
     *
     * If a permission is not listed here it cannot be used.
     */
    public array $permissions = [
        'admin.access'          => 'Can access the sites admin area',
        'admin.settings'        => 'Can access the main site settings',
        'users.manage-admins'   => 'Can manage other admins',
        'users.create'          => 'Can create new non-admin users',
        'users.edit'            => 'Can edit existing non-admin users',
        'users.delete'          => 'Can delete existing non-admin users',
        'beta.access'           => 'Can access beta-level features',
        //categories
        'categories.access'     => 'Can access to categories list',
        'categories.create'     => 'Can create new category',
        'categories.edit'       => 'Can edit existing category',
        'categories.delete'     => 'Can delete existing category',
        //products
        'products.access'       => 'Can access to products list',
        'products.create'       => 'Can create a new product',
        'products.edit'         => 'Can edit existing product',
        //orders
        'orders.access'       => 'Can access to orders list',
        'orders.edit'         => 'Can edit existing order',
        'orders.details'      => 'Can see existing order\'s details',
        'orders.download'      => 'Can download pdf order\'s report',
        //clients
        'clients.access'       => 'Can access to clientes list',
        'clients.edit'          => 'Can edit existing client',
        //product_features
        'features.access'       => 'Can edit existing product_features',
        'features.create'       => 'Can edit existing product_features',
        'features.edit'         => 'Can edit existing product_features',
        //currencies
        'currencies.access'       => 'Can access to currencies list inside settings menu',
        'currencies.create'       => 'Can create new currencies',
        'currencies.edit'         => 'Can edit existing currencies',
    ];

    /**
     * --------------------------------------------------------------------
     * Permissions Matrix
     * --------------------------------------------------------------------
     * Maps permissions to groups.
     *
     * This defines group-level permissions.
     */
    public array $matrix = [
        'superadmin' => [
            'admin.*',
            'users.*',
            'beta.*',
            'orders.*'
        ],
        'admin' => [
            'admin.access',
            'users.create',
            'users.edit',
            'users.delete',
            'beta.access',
            'categories.*',
            'products.*',
            'orders.*',
            'clients.*',
            'features.*',
            'currencies.*'
        ],
        'developer' => [
            'admin.access',
            'admin.settings',
            'users.create',
            'users.edit',
            'beta.access',
        ],
        'user' => [],
        'beta' => [
            'beta.access',
        ],
    ];
}
