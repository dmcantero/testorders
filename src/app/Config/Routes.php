<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

/*
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
// The Auto Routing (Legacy) is very dangerous. It is easy to create vulnerable apps
// where controller filters or CSRF protection are bypassed.
// If you don't want to define all routes, please use the Auto Routing (Improved).
// Set `$autoRoutesImproved` to true in `app/Config/Feature.php` and set the following to true.
// $routes->setAutoRoute(false);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'Home::index');
$routes->get('/cart', 'Cart::index');
$routes->get('/cart/success', 'Cart::success');
$routes->get('forcereset', 'Auth::forceResetPassword');
$routes->post('forcereset', 'Auth::forceResetPasswordAction');

service('auth')->routes($routes);

$routes->group('admin', ['namespace' => 'App\Controllers\Admin', 'filter' => 'group:admin'], function ($routes) {
    $routes->get('/',               'HomeController::index');

    $routes->get('categories',              'CategoriesController::index');
    $routes->get('categories/new',          'CategoriesController::new');
    $routes->get('categories/edit/(:num)',  'CategoriesController::edit/$1');
    $routes->get('categories/delete/(:num)', 'CategoriesController::delete/$1');

    $routes->get('features',                'FeaturesController::index');
    $routes->get('features/new',            'FeaturesController::new');
    $routes->get('features/edit/(:num)',    'FeaturesController::edit/$1');

    $routes->get('products',                'ProductsController::index');
    $routes->get('products/new',            'ProductsController::new');
    $routes->get('products/edit/(:num)',    'ProductsController::edit/$1');
    $routes->get('products/delete/(:num)',  'ProductsController::delete/$1');

    $routes->get('clients',                 'ClientsController::index');
    $routes->get('clients/edit/(:num)',     'ClientsController::edit/$1');
    $routes->get('clients/delete/(:num)',   'ClientsController::delete/$1');

    $routes->get('users',                   'UsersController::index');
    $routes->get('users/new',               'UsersController::new');
    $routes->get('users/edit/(:num)',       'UsersController::edit/$1');

    $routes->get('settings',                'SettingsController::index');

    $routes->resource('orders', ['controller' => 'OrdersController']);
});

$routes->group('catalogue', ['namespace' => 'App\Controllers\Catalogue', 'filter' => 'group:user, admin'], function ($routes) {
    $routes->get('/',                   'HomeController::index');
    $routes->addRedirect('category', 'catalogue');
    $routes->get('category/(:alphanum)', 'HomeController::category/$1');
    $routes->get('product',             'ProductController::index');
    $routes->get('myorders',            'HomeController::myorders');
    $routes->get('profile',             'HomeController::profile');
});

$routes->group('api/admin', ['namespace' => 'App\api\admin', 'filter' => 'group:admin'], function ($routes) {
    /** primero se deben escribir los get antes de los resource */
    // $routes->get('patients/searchpatients', 'patients::searchpatients');
    // $routes->get('doctors/searchdoctors', 'doctors::searchdoctors');
    $routes->resource('categories');
    $routes->resource('products');
    $routes->resource('clients');
    $routes->resource('orders');
});

$routes->group('api', ['namespace' => 'App\Api', 'filter' => 'group:admin, user'], function ($routes) {
    /** primero se deben escribir los get antes de los resource */
    $routes->get('categories/select', 'categories::select');
    $routes->get('categories/treeview', 'categories::treeview');

    $routes->post('orders/create', 'Orders::create');
    $routes->get('orders/(:num)/historyclient', 'Orders::historyclient/$1');

    $routes->get('users/profile', 'Users::profile');
    $routes->post('users/profilepost', 'Users::profilepost');

    $routes->post('products/activate', 'Products::activate');
    $routes->post('products/desactivate', 'Products::desactivate');
    $routes->post('products/uploadPhotos', 'Products::uploadPhotos');
    $routes->get('products/getfeatures/(:num)', 'Products::getfeatures/$1');
    $routes->get('products/getphotos/(:num)', 'Products::getphotos/$1');
    $routes->get('products/removephoto/(:num)', 'Products::removePhoto/$1');

    $routes->get('brands/select', 'Brands::select');
    $routes->get('suppliers/select', 'Suppliers::select');

    $routes->get('features/select', 'Features::select');
    $routes->get('featuresvalues/select/(:num)', 'FeaturesValues::select/$1');
    $routes->get('bulkpricings/getbyproduct/(:num)', 'BulkPricings::getbyproduct/$1');
    $routes->get('province/cities/(:num)', 'Province::cities/$1');
    $routes->get('province/select', 'Province::select');
    $routes->get('shipping/select', 'Shippings::select');
    $routes->get('clients/select', 'Clients::select');
    $routes->get('currencies/select', 'Currencies::select');
    $routes->get('settings/defaults', 'Settings::defaults');

    $routes->resource('clients',            ['controller' => 'Clients']);
    $routes->resource('permissions');
    $routes->resource('categories',         ['controller' => 'Categories']);
    $routes->resource('brands',             ['controller' => 'Brands']);
    $routes->resource('suppliers',          ['controller' => 'Suppliers']);
    $routes->resource('products',           ['controller' => 'Products']);
    $routes->resource('features',           ['controller' => 'Features']);
    $routes->resource('featuresvalues',     ['controller' => 'FeaturesValues']);
    $routes->resource('productsfeatures',   ['controller' => 'ProductsFeatures']);
    $routes->resource('bulkpricings',       ['controller' => 'BulkPricings']);
    $routes->resource('province',           ['controller' => 'Province']);
    $routes->resource('shippings',          ['controller' => 'Shippings']);
    $routes->resource('users',              ['controller' => 'Users']);
    $routes->resource('currencies',         ['controller' => 'Currencies']);
    $routes->resource('orders',             ['controller' => 'Orders']);
    $routes->resource('settings',           ['controller' => 'Settings']);


    $routes->get('catalogue', 'Catalogue::index');

    $routes->get('shoppingcart', 'ShoppingCart::index');
    $routes->post('shoppingcart/add', 'ShoppingCart::add');
    $routes->post('shoppingcart/update/(:any)', 'ShoppingCart::update/$1');
    $routes->delete('shoppingcart/delete/(:any)', 'ShoppingCart::delete/$1');
    $routes->post('shoppingcart/clear', 'ShoppingCart::clear');

    $routes->get('file/(:segment)', 'Files::showFile/$1');
    $routes->delete('file/(:num)', 'Files::deleteFile/$1');
});


$routes->get('reports/order/(:any)', 'ReportController::order/$1', ['filter' => 'group:admin, user']);

/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (is_file(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
    require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
