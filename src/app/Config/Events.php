<?php

namespace Config;

use App\Api\Orders;
use App\Entities\Enums\OrderStatusEnum;
use CodeIgniter\Events\Events;
use CodeIgniter\Exceptions\FrameworkException;
use CodeIgniter\Shield\Entities\User;
use CodeIgniter\I18n\Time;
use CodeIgniter\Shield\Exceptions\LogicException;
use CodeIgniter\Shield\Exceptions\RuntimeException;
use App\Libraries\DocumentGenerator;
use App\Models\OrdersModel;
use stdClass;

/*
 * --------------------------------------------------------------------
 * Application Events
 * --------------------------------------------------------------------
 * Events allow you to tap into the execution of the program without
 * modifying or extending core files. This file provides a central
 * location to define your events, though they can always be added
 * at run-time, also, if needed.
 *
 * You create code that can execute by subscribing to events with
 * the 'on()' method. This accepts any form of callable, including
 * Closures, that will be executed when the event is triggered.
 *
 * Example:
 *      Events::on('create', [$myInstance, 'myMethod']);
 */

Events::on('pre_system', static function () {
    if (ENVIRONMENT !== 'testing') {
        if (ini_get('zlib.output_compression')) {
            throw FrameworkException::forEnabledZlibOutputCompression();
        }

        while (ob_get_level() > 0) {
            ob_end_flush();
        }

        ob_start(static fn ($buffer) => $buffer);
    }

    /*
     * --------------------------------------------------------------------
     * Debug Toolbar Listeners.
     * --------------------------------------------------------------------
     * If you delete, they will no longer be collected.
     */
    if (CI_DEBUG && !is_cli()) {
        Events::on('DBQuery', 'CodeIgniter\Debug\Toolbar\Collectors\Database::collect');
        Services::toolbar()->respond();
    }
});


Events::on('login', function (User $user) {
    $session = \Config\Services::session();
    $session->set('company_id', $user->company_id);
});

/**
 * Inserta el cliente enlazado al usuario registrado.
 */
Events::on('register', function (User $dataUser) {
    $client = new \App\Entities\ClientEntity();
    $model = new \App\Models\ClientsModel();

    $data = [
        'user_id'       => $dataUser->id,
        'business_name' => $dataUser->first_name . ' ' . $dataUser->last_name,
        'contact_person' => $dataUser->first_name . ' ' . $dataUser->last_name,
        'email'         => $dataUser->getEmailIdentity()->secret,
        'company_id'    => $dataUser->company_id
    ];

    $client->fill($data);
    $result = $model->insert($client, true);

    return $result;
});


// notice I am passing two variables from the controller $user and $tmpPass
// I will force the user to change password on first login
Events::on('newUser', static function (User $user, $tmpPass, bool $notify = true) {
    try {
        if ($notify) {
            if ($user->email === null) {
                throw new LogicException(
                    'Email Activation needs user email address. user_id: ' . $user->id
                );
            }

            $date      = Time::now()->toDateTimeString();

            $from = '';
            $fromName = '';
            $to = '';
            if (env('CI_ENVIRONMENT') == 'production') {
                $from = setting('Email.fromEmail');
                $fromName = setting('Email.fromName');
                $to =  $user->email;
            } else {
                $from = setting('EmailDev.fromEmail');
                $fromName = setting('EmailDev.fromName');
                $to = setting('EmailDev.fromEmail');
            }

            // Send the email
            $email = \Config\Services::email();
            $email->setTo($to);
            $email->setFrom($from, $fromName);
            $email->setSubject("Cuenta creada");
            $email->setMessage(view('Email/EmailNewUserNotification', ['userName' => $user->username, 'tmpPass' => $tmpPass, 'date' => $date]));

            if ($email->send(false) === false) {
                throw new RuntimeException('Cannot send email for user: ' . $user->email . "\n" . $email->printDebugger(['headers']));
            }

            // Clear the email
            $email->clear();

            return true;
        }
        return true;
    } catch (\Throwable $th) {
        throw $th;
    }
});


Events::on('emailOrder', static function (int $orderId, bool $notify = true) {
    if ($notify) {
        $orderModel = new OrdersModel();
        $config = new SettingsApp();
        $default_currency = $config->get('default_currency')['value'];

        $dataOrder =  $orderModel->search($orderId, 0, '', '', '', '', $default_currency, '', 1, 1)['Rows'][0];
        if (!$dataOrder->client_email) {
            log_message('error', "El cliente no tiene una cuenta de email asignada. No es posible enviar el correo del pedido.");
            return false;
            // throw new LogicException(
            //     'El cliente no tiene una cuenta de email asignada.'
            // );
        }
        $from = '';
        $fromName = '';
        $to = '';
        if (env('CI_ENVIRONMENT') == 'production') {
            $from = setting('Email.fromEmail');
            $fromName = setting('Email.fromName');
            $to =  [$dataOrder->client_email, setting('Email.adminEmail')];
        } else {
            $from = setting('EmailDev.fromEmail');
            $fromName = setting('EmailDev.fromName');
            $to = [setting('EmailDev.fromEmail'), setting('Email.adminEmail')];
        }
        // Send the email
        $email = \Config\Services::email();
        $email->setTo($to);
        $email->setFrom($from, $fromName);

        switch ($dataOrder->status) {
            case OrderStatusEnum::ISSUED:
                $dataView['order'] = $dataOrder;
                $email->setSubject(env('TITLE_WEBSITE') . ' - Pedido recibido');
                $viewHtml = view('Email/EmailOrderIssued',  $dataView);
                break;
            case OrderStatusEnum::CONFIRMED:
                $dataView['order'] = $dataOrder;
                $email->setSubject(env('TITLE_WEBSITE') . ' - Pedido confirmado');
                $viewHtml = view('Email/EmailOrderConfirmed',  $dataView);
                break;
            default:
                # code...
                break;
        }

       
        $email->setMessage($viewHtml);
        $pdfAttach = DocumentGenerator::OrderPdfAttach($dataOrder->Id, $dataOrder->number, $default_currency);

        $email->attach($pdfAttach, 'inline', 'Order-' . $dataOrder->number);

        if ($email->send(false) === FALSE) {
            log_message('error', "Error al enviar correo de pedido: " + $email->printDebugger(['headers']));
            return FALSE;
            // throw new RuntimeException('Cannot send email order issued: ' . "\n" . );
        }
        // Clear the email
        $email->clear();
        return TRUE;
    }
});

