<@php
$router = service('router');
$data['controller']  = $router->controllerName();
$data['method'] = $router->methodName();
$data['module'] = "{module}";
$data['area']   = strtolower("{area}");
@>
<@= $this->extend('Admin/Layout/Main') @>
<@= $this->section('content') @>
<div data-aos="fade-up">
    <@= view('Admin/{folderViewModule}/Datatable', $data) @>
</div>
<@= $this->endSection(); @>