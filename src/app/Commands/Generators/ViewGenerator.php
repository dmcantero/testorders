<?php

/**
 * This file is part of CodeIgniter 4 framework.
 *
 * (c) CodeIgniter Foundation <admin@codeigniter.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace App\Commands\Generators;

use CodeIgniter\CLI\BaseCommand;
use CodeIgniter\CLI\GeneratorTrait;
use CodeIgniter\CLI\CLI;


/**
 * Generates a skeleton controller file.
 */
class ViewGenerator extends BaseCommand
{

    use GeneratorTrait;

    /**
     * The Command's Group
     *
     * @var string
     */
    protected $group = 'Generators';

    /**
     * The Command's Name
     *
     * @var string
     */
    protected $name = 'make:view';

    /**
     * The Command's Description
     *
     * @var string
     */
    protected $description = 'Generates a new view file for an area and a module. If the module or area folder doesn\'t exist then create it first';

    /**
     * The Command's Usage
     *
     * @var string
     */
    protected $usage = 'make:view <name> [options]';

    /**
     * The Command's Arguments
     *
     * @var array
     */
    protected $arguments = [
        'name' => 'The view file name.',
    ];

    /**
     * The Command's Options
     *
     * @var array
     */
    protected $options = [
        '--force'   => 'Force overwrite existing file.',
        '--module'  => 'Module\'s name',
        '--area'    => 'Area\'s name', //=> "Admin", "Landing", "" 
        '--tpl'     => 'Template\'s name'
    ];

    /**
     * Actually execute a command.
     */
    public function run(array $params)
    {
        $this->params = $params;
        
        helper("filesystem");
        $areaFolder = "";
        $folderViewModule = "";
        $viewName = $this->converUCFirst($params['name']) . ".php";

        //has --area option
        if ($this->getOption('area')) {
            $areaFolder = $this->converUCFirst($this->getOption('area'));

            if (!$this->checkAreaDirectory($areaFolder)) {
                CLI::write("Error al crear el directorio para el área", 'red');
                return EXIT_ERROR;
            }
        }

        //has --module option
        if ($this->getOption('module')) {
            $folderViewModule = $this->converUCFirst($this->getOption('module'));

            if (!$this->checkModuleDirectory($areaFolder, $folderViewModule)) {
                CLI::write("Error al crear el directorio para las vistas del módulo", 'red');
                return EXIT_ERROR;
            }
        }

        $finalPathFile = APPPATH . "Views";

        if ($areaFolder != "") {
            $finalPathFile .= "\\" . $areaFolder;
        }

        if ($folderViewModule != "") {
            $finalPathFile .= "\\" . $folderViewModule;
        }

        $viewContent = $this->parseTemplate(
            ['{area}', '{module}', '{folderViewModule}'],
            [$areaFolder, $this->getOption('module'), $folderViewModule],
            []
        );

        if (!write_file("{$finalPathFile}\\{$viewName}", $viewContent)) {
            CLI::write("Error creating view {$viewName}", 'red');
        } else {
            CLI::write("File {$finalPathFile}/{$viewName} created successfully", 'green');
        }
    }

    private function converUCFirst($string)
    {
        return ucfirst(strtolower($string));
    }

    private function checkModuleDirectory(string $area, string $module): bool
    {
        try {
            $result = true;
            $path = APPPATH . ($area != "" ? "Views\\{$area}\\{$module}" : "Views\\{$module}");
            if (!is_dir($path)) {
                $result = mkdir($path, 0777, TRUE);
            }
            return $result;
        } catch (\Throwable $th) {
            $this->showError($th);
            return false;
        }
    }

    private function checkAreaDirectory(string $area): bool
    {
        try {
            $result = true;
            $path = APPPATH . "Views\\{$area}";
            if (!is_dir($path)) {
                $result = mkdir($path, 0777, TRUE);
            }
            return $result;
        } catch (\Throwable $th) {
            $this->showError($th);
            return false;
        }
    }



    /**
     * Performs pseudo-variables contained within view file.
     */
    protected function parseTemplate(array $search = [], array $replace = [], array $data = []): string
    {
        $search[]  = '<@php';
        $search[]  = '@>';
        $search[]  = '<@=';
        $replace[] = '<?php';
        $replace[] = '?>';
        $replace[] = '<?=';

        return str_replace($search, $replace, $this->renderTemplate($data));
    }

    protected function renderTemplate(array $data = []): string
    {
        try {
            if ($this->getOption('tpl')) {
                return view(config('Generators')->views[$this->getOption('tpl')], $data, ['debug' => false]);
            }
            return "<@php //vista autogenerada " . date('Y-d-m H:i:s', time()) . " @>";
        } catch (\Throwable $e) {
            log_message('error', (string) $e);
            return view("CodeIgniter\\Commands\\Generators\\Views\\{$this->template}", $data, ['debug' => false]);
        }
    }

    // /**
    //  * Prepare options and do the necessary replacements.
    //  */
    // protected function prepare(string $class): string
    // {





    //     return $this->parseTemplate(
    //         $class,
    //         ['{useStatement}', '{extends}'],
    //         [$useStatement, $extends],
    //         ['type' => $rest]
    //     );
    // }
}
