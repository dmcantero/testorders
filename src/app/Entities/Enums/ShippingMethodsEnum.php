<?php

namespace app\Entities\Enums;

enum ShippingMethodsEnum
{
    const PICKUP            =   'PICKUP';
    const DELIVERY          =   'DELIVERY';
    const DELIVERY_THIRD    =   'DELIVERY_THIRD';
}