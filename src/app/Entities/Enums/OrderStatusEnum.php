<?php
namespace App\Entities\Enums;

enum OrderStatusEnum
{
    const INPROGRESS    =   'INPROGRESS';
    const ISSUED        =   'ISSUED';
    const CONFIRMED     =   'CONFIRMED';
    const CANCELLED     =   'CANCELLED';
}