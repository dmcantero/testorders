<?php namespace App\Entities;

use CodeIgniter\Shield\Entities\User as ShieldUser;

class CurrentUser extends ShieldUser
{
    /**
     * Default attributes.
     * @var array
     */
    protected $attributes = [
    	'firstname' => 'Guest',
    	'lastname'  => 'User',
    ];

	/**
	 * Returns a full name: "first last"
	 *
	 * @return string
	 */
	public function getName()
	{
		return trim(trim($this->attributes['firstname']) . ' ' . trim($this->attributes['lastname']));
	}

    // public function getEmail()
    // {
    //     return trim(trim($this->attributes['email']));
    // }
}