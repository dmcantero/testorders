<?php

namespace App\Entities;

use CodeIgniter\Entity\Entity;
use CodeIgniter\I18n\Time;
use stdClass;

class BulkPricingEntity extends Entity
{
    protected $attributes = [
        'id'                => 0,
        'product_id'        => null,
        'min_bulk'          => 0,
        'max_bulk'          => 0,
        'discount'          => 0,
        'sale_price'        => 0,
        'unit_sale_price'  => 0,
        'valid_from'        => '',
        'valid_until'       => '',
        'active'            => true,
        'allow_select_qty'  => true,
        'company_id'        => null,
        'created_by'        => null,
        'updated_by'        => null,
        'deleted_by'        => null
    ];

    protected $datamap = [];
    protected $dates   = ['created_at', 'updated_at', 'deleted_at'];
    protected $casts   = [
        'active' => 'boolean',
        'allow_select_qty' => 'boolean'
    ];

    // Bind the type to the handler
    //  protected $castHandlers = [
    //     'datetime' => \App\Entities\Cast\CastDatetime::class,
    // ];


    public function setValidfrom($value)
    {
        if ($value == null) {
            $this->attributes['valid_from'] = null;
        } else {
            $this->attributes['valid_from'] = $value;
        }
    }

    // public function getValidfrom()
    // {
    //     if ($this->attributes['valid_from'] == null || $this->attributes['valid_from'] == "") {
    //         return null;
    //     }
    //     return $this->attributes['valid_from'];
    // }

    // public function getValidUntil()
    // {
    //     if ($this->attributes['valid_until'] == null || $this->attributes['valid_from'] == "") {
    //         return "";
    //     }
    //     return $this->attributes['valid_until'];
    // }

    public function setValidUntil($value)
    {
        if ($value == null) {
            $this->attributes['valid_until'] = null;
        } else {
            $this->attributes['valid_until'] = $value;
        }
    }

    public function setActive($value)
    {
        $this->attributes['active'] = $value ? "1" : "0";
    }

    public function setAllowSelectQty($value)
    {
        $this->attributes['allow_select_qty'] = $value ? "1" : "0";
    }
}
