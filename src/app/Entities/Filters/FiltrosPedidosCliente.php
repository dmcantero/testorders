<?php

namespace App\Entities\Filtros;

use CodeIgniter\Entity\Entity;

class FiltrosPedidosCliente extends Entity
{
    protected $attributes = [
        'cliente_id'        => 0,
        'fecha_desde'       => NULL,
        'fecha_hasta'       => NULL,
        'estado_id'         => 0,
        //Utilizados para el paginado y ordenación
        'PageIndex'         => 1,
        'PageSize'          => 10,
        'Order'           => 'send_at DESC'
    ];
}
