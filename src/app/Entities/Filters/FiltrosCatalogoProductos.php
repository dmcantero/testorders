<?php

namespace App\Entities\Filtros;

use CodeIgniter\Entity\Entity;

class FiltrosCatalogoProductos extends Entity
{
    protected $attributes = [
        'palabras'          => '',
        'cliente_id'        => 0,
        'lista_id'          => 0,
        'destacado'         => 0,
        'nuevo'             => 0,
        'oferta'            => 0,
        'en_carrito'        => 0,
        'todos'             => 0,
        //Utilizados para el paginado y ordenación
        'PageIndex'         => 1,
        'PageSize'          => 10,
        'Ordenar'           => 'nombre ASC'
    ];

    public function destacado()
    {
        if ($this->attributes["destacado"] == "false" || $this->attributes["destacado"] == "" || $this->attributes["destacado"] == 0)
            return 0;
        else
            return 1;
    }

    public function nuevo()
    {
        if ($this->attributes["nuevo"] == "false" || $this->attributes["nuevo"] == "" || $this->attributes["nuevo"] == 0)
            return 0;
        else
            return 1;
    }

    public function oferta()
    {
        if ($this->attributes["oferta"] == "false" || $this->attributes["oferta"] == "" || $this->attributes["oferta"] == 0)
            return 0;
        else
            return 1;
    }

    public function en_carrito()
    {
        if ($this->attributes["en_carrito"] == "false" || $this->attributes["en_carrito"] == "" || $this->attributes["en_carrito"] == 0)
            return 0;
        else
            return 1;
    }

    public function todos()
    {
        if ($this->attributes["todos"] == "false" || $this->attributes["todos"] == "" || $this->attributes["todos"] == 0)
            return 0;
        else
            return 1;
    }
}
