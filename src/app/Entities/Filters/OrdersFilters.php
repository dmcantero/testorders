<?php

namespace App\Entities\Filters;

use CodeIgniter\Entity\Entity;

class OrdersFilters extends Entity
{

    protected $attributes = [
        'client_id'              => 0,
        'status'                => '',
        'number'                => '',
        'date_from'             => '',
        'date_until'            => '',
        'currency'              => '',
        //Utilizados para el paginado y ordenación
        'PageIndex'             => 1,
        'PageSize'              => 10,
        'Ordenar'               => 'send_at DESC'
    ];

    public function getDateFrom()
    {
        return (strlen($this->attributes["date_from"]) == 0) ?  '' : date("Y-m-d 00:00:00", strtotime($this->attributes['date_from']));
    }

    public function getDateUntil()
    {
        return (strlen($this->attributes["date_until"]) == 0) ?  '' : date("Y-m-d 23:59:59", strtotime($this->attributes['date_until']));
    }
}
