<?php

namespace App\Entities\Filtros;

use CodeIgniter\Entity\Entity;

class FiltrosClientes extends Entity
{
    protected $attributes = [
        'nombre'            => '',
        'username'          => '',
        'razonSocial'       => '',
        'listaPrecioId'     => 0,
        'MostrarEliminados' => 0,
        //Utilizados para el paginado y ordenación
        'PageIndex'         => 1,
        'PageSize'          => 10,
        'Ordenar'           => 'nombre ASC'
    ];

    public function MostrarEliminados()
    {
        if (($this->MostrarEliminados == "false") || ($this->MostrarEliminados == "") || ($this->MostrarEliminados === 0))
            return 0;
        else
            return 1;
    }
}
