<?php

namespace App\Entities\Filtros;

use CodeIgniter\Entity\Entity;

class FiltrosListasPrecios extends Entity
{

    protected $attributes = [
        'nombre'            => '',
        //Utilizados para el paginado y ordenación
        'PageIndex'         => 1,
        'PageSize'          => 10,
        'MostrarEliminados' => 0,
        'Ordenar'             => 'nombre ASC'
    ];

    public function MostrarEliminados()
    {
        if ($this->attributes["MostrarEliminados"] == "false" || $this->attributes["MostrarEliminados"] == "" || $this->attributes["MostrarEliminados"] == 0)
            return 0;
        else
            return 1;
    }
}
