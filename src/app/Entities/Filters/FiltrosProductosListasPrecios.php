<?php

namespace App\Entities\Filtros;

use CodeIgniter\Entity\Entity;

class FiltrosProductosListasPrecios extends Entity
{

    protected $attributes = [
        'codigo'            => '',
        'nombre'            => '',
        'marca_id'          => 0,
        'proveedor_id'      => 0,
        //Utilizados para el paginado y ordenación
        'PageIndex'         => 1,
        'PageSize'          => 10,
        'Ordenar'           => 'nombre ASC'
    ];
}
