<?php

namespace App\Entities\Filtros;

use CodeIgniter\Entity\Entity;

class FiltrosGroups extends Entity
{
    protected $attributes = [
        'nombre'            => '',
        //Utilizados para el paginado y ordenación
        'PageIndex'         => 1,
        'PageSize'          => 10,
        'Ordenar'           => 'nombre ASC'
    ];
}
