<?php

namespace App\Entities\Filters;

use CodeIgniter\Entity\Entity;

class ProductsFilters extends Entity
{
    protected $attributes = [
        'name'                  => '',
        'code'                  => '',
        'category_id'           => 0,
        'brand_id'              => 0,
        'supplier_id'           => 0,
        //Utilizados para el paginado y ordenación
        'PageIndex'             => 1,
        'PageSize'              => 10,
        'OrderBy'               => 'nombre ASC'
    ];

    // public function MostrarEliminados()
    // {
    //     if ($this->attributes["MostrarEliminados"] == "false" || $this->attributes["MostrarEliminados"] == "" || $this->attributes["MostrarEliminados"] == 0)
    //         return 0;
    //     else
    //         return 1;
    // }
}
