<?php

namespace App\Entities;

use CodeIgniter\Entity\Entity;

class CategoryEntity extends Entity
{
    protected $datamap = [];
    protected $dates   = ['created_at', 'updated_at', 'deleted_at'];
    protected $attributes = [
        'id'                => 0,
        'name'              => null,
        'description'       => null,
        'father_id'         => 0,
        'show_in_landing'   => null,
        'show_in_menu'      => null,
        'company_id'        => null,
        'created_by'        => null,
        'updated_by'        => null,
        'deleted_by'        => null
    ];

    protected $casts   = [
        'show_in_menu' => 'boolean',
        'show_in_landing'   => 'boolean'
    ];

    public function setShowInMenu($value)
    {
        $this->attributes['show_in_menu'] = $value ? 1 : 0;
    }

    public function setShowInLanding($value)
    {
        $this->attributes['show_in_landing'] = $value ? 1 : 0;
    }
}
