<?php

namespace App\Entities;

use CodeIgniter\Entity\Entity;

class FeaturesValueEntity extends Entity
{
    protected $datamap = [];
    protected $dates   = ['created_at', 'updated_at', 'deleted_at'];
    protected $casts   = [];

    protected $attributes = ['id', 'feature_id', 'value', 'company_id', 'created_by', 'updated_by', 'deleted_by'];
}
