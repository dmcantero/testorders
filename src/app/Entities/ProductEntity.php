<?php

namespace App\Entities;

use CodeIgniter\Entity\Entity;

class ProductEntity extends Entity
{
    protected $attributes = [
        'id'            => 0,
        'code'          => "",
        'name'          => "",
        'description'   => "",
        'sale_price'    => 0,
        'sale_price_unit'   => 0,
        'purchase_price'=> 0,
        'category_id'   => null,
        'brand_id'      => null,
        'category_id'   => null,
        'supplier_id'   => null,
        'min_package'   => 1,
        'image_url'     => null,
        'active'        => null,
        'stock'         => 100,
        'min_stock'     => 10,
        'bulk_quantity' => 0,
        'company_id'    => null,
        'created_by'    => '',
        'updated_by'    => '',
        'deleted_by'    => ''
    ];


    protected $datamap = [];
    protected $dates   = ['created_at', 'updated_at', 'deleted_at'];
    protected $casts   = [
        'active' => 'boolean'
    ];

    public function setActive($value)
    {
        $this->attributes['active'] = $value ? 1 : 0;
    }
}
