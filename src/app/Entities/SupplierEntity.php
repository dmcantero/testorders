<?php

namespace App\Entities;

use CodeIgniter\Entity\Entity;

class SupplierEntity extends Entity
{
    protected $datamap = [];
    protected $dates   = ['created_at', 'updated_at', 'deleted_at'];
    protected $casts   = [];
    protected $attributes = ['id', 'name', 'identity_number', 'identity_type', 'business_name', 'contact_person', 'phone_number', 'email', 'notes', 'company_id', 'created_by', 'updated_by', 'deleted_by'];
}
