<?php

namespace App\Entities\ViewModels;

use CodeIgniter\Entity\Entity;
use Myth\Auth\Entities\User;
use App\Entities\Persona;
use App\Entities\Cliente;

class ClienteViewModel extends Entity
{
    protected $attributes = [
        'id'                => 0,
        'user_id'           => 0,
        'persona_id'        => 0,
        'nombre'            => '',
        'apellido'          => '',
        'email'             => '',
        'direccion'         => '',
        'celular'           => '',
        'telefono'          => '',
        'provincia'         => '',
        'localidad'         => '',
        'codigo_postal'     => '',
        'username'          => '',
        'password'          => '',
        'active'            => true,  
        'lista_precio_id'   => "0",
        'bonificacion'      => 0,  
        'razon_social'      => '',
        'eliminado'         => 0
    ];

    protected $casts   = [
        'active'    => 'boolean',
        'eliminado' => 'boolean'
    ];

    public $validationRules = [
        'email'         => 'required|valid_email|is_unique[users.email,id,{user_id}]',
        'username'      => 'required|alpha_numeric_space|min_length[3]|max_length[30]|is_unique[users.username,id,{user_id}]'
    ];

    protected $validationMessages = [];


    public function getUser() : User
    {
        $user = new User();
        $user->id = $this->user_id;
        $user->persona_id = $this->persona_id;
        $user->username = $this->username;
        $user->email = $this->email;
        $user->password_hash = $this->password_hash;
        $user->active = $this->active;

        return $user;
    }

    public function getPersona() : Persona
    {
        $persona = new Persona();
        $persona->id = $this->persona_id;
        $persona->nombre = $this->nombre;
        $persona->apellido = $this->apellido;
        $persona->razon_social = $this->razon_social;
        $persona->celular = $this->celular;
        $persona->telefono = $this->telefono;
        $persona->direccion = $this->direccion;
        $persona->provincia = $this->provincia;
        $persona->localidad = $this->localidad;
        $persona->codigo_postal = $this->codigo_postal;
        
        return $persona;
    }

    public function getCliente() : Cliente
    {
        $cliente = new Cliente();
        $cliente->id = $this->id;
        $cliente->user_id = $this->user_id;
        $cliente->lista_precio_id = $this->lista_precio_id;
        $cliente->bonificacion = $this->bonificacion;
        $cliente->eliminado = $this->eliminado;

        return $cliente;
    }
    
}


?>