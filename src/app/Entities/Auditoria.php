<?php

namespace App\Entities;

use CodeIgniter\Entity\Entity;

class Auditoria extends Entity
{
    protected $attributes = [
        'id'            => 0,
        'created_by'    => '',
        'entity'        => '',
        'action'        => ''
    ];


    protected $datamap = [];
    protected $dates   = ['created_at'];
    protected $casts   = [];
}
