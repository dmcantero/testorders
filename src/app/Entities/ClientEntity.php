<?php

namespace App\Entities;

use CodeIgniter\Entity\Entity;

class ClientEntity extends Entity
{
    protected $attributes = [
        'id'                    => 0,
        'user_id'               => 0,
        'username'              => null,
        'password'              => null,
        'identity_number'       => null,
        'identity_type'         => null,
        'business_name'         => null,
        'contact_person'        => null,
        'phone_number'          => null,
        'email'                 => null,
        'address'               => null,
        'billing_address'       => null,
        'billing_city_id'       => null,
        'province_id'           => null,
        'billing_province_id'   => null,
        'city_id'               => null,
        'bonus'                 => null,
        'shipping_id'           => null,
        'notes'                 => null,
        'has_required_data'     => null,
        'company_id'            => null,
        'created_by'            => '',
        'updated_by'            => '',
        'deleted_by'            => ''
    ];


    protected $datamap = [];
    protected $dates   = ['created_at', 'updated_at', 'deleted_at'];
    protected $casts   = [
        'has_required_data' => 'boolean'
    ];

    public function setHasRequiredData($value)
    {
        $this->attributes['has_required_data'] = $value ? 1 : 0;
    }
}
