<?php

namespace App\Entities;

use CodeIgniter\Entity\Entity;

class ProductsFeatureEntity extends Entity
{
    protected $datamap = [];
    protected $dates   = ['created_at', 'updated_at', 'deleted_at'];
    protected $casts   = [];
    protected $attributes =  [
        'id'            => 0,
        'product_id'    => null,
        'feature_id'    => null,
        'value_id'      => null,
        'custom_value'  => null,
        'company_id'    => null,
        'created_by'    => null,
        'updated_by'    => null,
        'deleted_by'    => null
    ];
}
