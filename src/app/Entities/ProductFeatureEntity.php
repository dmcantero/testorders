<?php

namespace App\Entities;

use CodeIgniter\Entity\Entity;
use CodeIgniter\I18n\Time;
use stdClass;

class ProductFeatureEntity extends Entity
{
    protected $attributes = [
        'id'            => 0,
        'product_id'    => null,
        'feature_id'    => "0",
        'value_id'      => "0",
        'custom_value'  => null,
        'company_id'    => null,
        'created_by'    => null,
        'updated_by'    => null,
        'deleted_by'    => null
    ];

    protected $datamap = [];
    protected $dates   = ['created_at', 'updated_at', 'deleted_at'];
    protected $casts   = [];

    public function setValueId($data)
    {
        if ($data == "0" || $data == "") {
            $this->attributes['value_id'] = null;
        }else{
            $this->attributes['value_id'] = $data;
        }
    }
}
