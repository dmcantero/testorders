<?php

namespace App\Entities;

use CodeIgniter\Entity\Entity;

class OrderDetailEntity extends Entity
{
    protected $attributes = [
        'id'                    => 0,
        'order_id'              => null,
        'date'                  => '', 
        'product_id'            => null, 
        'price_list_id'         => null,
        'bulk_items'            => 1,
        'bulk_price'            => 0,
        'bulk_unit_price'       => 0,
        'unit_price'            => 0,
        'quantity'              => 0,
        'subtotal'              => 0, 
        'confirmed_quantity'    => 0, 
        'confirmed_subtotal'    => 0,
        'discount'              => 0,
        'discount_percentage'   => 0,
        'company_id'            => null,
        'created_by'            => '',
        'updated_by'            => '',
        'deleted_by'            => ''
    ];


    protected $datamap = [];
    protected $dates   = ['created_at', 'updated_at', 'deleted_at'];
    protected $casts   = [
        'free_shipping' => 'boolean'
    ];

}
