<?php
namespace App\Libraries;

use App\Entities\Enums\OrderStatusEnum;
use App\Models\OrdersModel;
use Config\SettingsApp;
use Dompdf\Options;

class DocumentGenerator
{
    public static function OrderPdfAttach($id, $number, $currency = null)
    {
        try {
            if (is_null($id) || is_null($number)) {
                return redirect()->to('/');
            }

            if($currency == NULL){
                $config = new SettingsApp();
                $currency = $config->get('default_currency')['value'];
            }

            $modelOrder = new OrdersModel();
            $dataOrder['data'] =  $modelOrder->getWithDetails($id, $currency);

            if ($dataOrder['data']['number'] != $number) {
                return redirect()->to('/');
            }

            $dompdf = new \Dompdf\Dompdf();
            $filename = env('NAME_COMPANY') . " - {$dataOrder['data']['number']}.pdf";
            $viewHtml = '';

            if ($dataOrder['data']['status'] == OrderStatusEnum::CONFIRMED) {
                $viewHtml = view('Reports/OrderConfirmed', $dataOrder);
            } else {
                $viewHtml = view('Reports/Order', $dataOrder);
            }

            $dompdf->loadHtml($viewHtml);

            $dompdf->setPaper('A4', 'portrait');
            $options = new Options([
                'defaultMediaType' => 'all',
                'isFontSubsettingEnabled' => true,
                'isRemoteEnabled'   => true,
                'isPhpEnabled' => true,
                'enableCssFloat' => true
            ]);
            $dompdf->setOptions($options);
            $dompdf->render();

            $var = file_put_contents(WRITEPATH . 'Order-' . $number . '.pdf', $dompdf->output());
            $pdf_path = WRITEPATH . 'Order-' . $number . '.pdf';
            return $pdf_path;
        } catch (\Throwable $th) {
            log_message('error', $th->__toString());
            throw $th;
        }
    }

    // public static function ExcelDetallesPedidos($pedidoid)
    // {
    //     $total = 0;
    //     $totalConfirmado = 0;
    //     $result = $this->PedidosModel->ObtenerConDetallesExcel($pedidoid);
    //     $data['data'] = $this->PedidosModel->ObtenerConDetalles($pedidoid);

    //     $data['data']->pedido->descuento = round($data['data']->pedido->total - $data['data']->pedido->totaldescuento, 2);
    //     foreach ($result as $key => $value) {
    //         $total = $value->subtotal + $total;
    //         $totalConfirmado = $value->subtotalConfirmado + $totalConfirmado;
    //     }
    //     $resultado = new GridDatatableData(1, $result, 1, 1);
    //     $exportador = new Exportador();
    //     //Se crea un array con los nombre de los getters del objeto a exportar
    //     $metodos = array("codigo", "nombre", "cantidad", "preciounitario", "subtotal");
    //     //Se selecciona el titulo del reporte
    //     $fecha_titulo = date('d-m-Y');
    //     if ($data['data']->pedido->porcentajedescuento > 0) {
    //         $tituloReporte = "Pedido_NPW" . str_pad($pedidoid, 4, "0", STR_PAD_LEFT) . "Monto total_" . FormatearNumero($data['data']->pedido->totaldescuento) . $fecha_titulo;
    //     } else {
    //         $tituloReporte = "Pedido_NPW" . str_pad($pedidoid, 4, "0", STR_PAD_LEFT) . "Monto total_" . FormatearNumero($data['data']->pedido->total) . $fecha_titulo;
    //     }

    //     //Se agregan los nombres de las columnas
    //     $titulosColumnas = array("Codigo", "Nombre", "Cantidad", "Precio", "SubTotal");
    //     $datos['cliente'] =  $data['data']->pedido->nombre . ' ' . $data['data']->pedido->apellido;
    //     $datos['fecha'] = date('d/m/Y');
    //     $datos['pedido'] = "NPW" . str_pad($pedidoid, 4, "0", STR_PAD_LEFT);
    //     if ($data['data']->pedido->tipolista == 'normal') {
    //         $datos['tipo'] = "Stock Actual";
    //     } else {
    //         $datos['tipo'] = "Preventa";
    //     }
    //     $datos['estado'] = "Enviado a CKM";
    //     $datos['subtotal'] = FormatearNumero($data['data']->pedido->total);
    //     $datos['total'] = FormatearNumero($data['data']->pedido->totaldescuento);
    //     $datos['bonificaciones'] = FormatearNumero($data['data']->pedido->descuento);

    //     // $excel = $exportador->exportarExcelPedido($tituloReporte, $titulosColumnas, $resultado, $metodos, $datos);
    //     $excel = $exportador->crearExcelTemporal($tituloReporte, $titulosColumnas, $resultado, $metodos, $datos);
    //     return $excel;
    // }
}
