<?php

use App\Models\ClientsModel;

function permissions($current_module, $permissions) 
{
    $current_permissions = [];
    foreach ($permissions as $key => $permission) {
        $module = substr($permission, 0, strpos($permission, "."));
        if ($current_module == $module) {
            $permission = substr($permission, strpos($permission, ".") + 1);
            array_push($current_permissions, ['Nombre' => strtoupper($permission), 'Identificador' => strtoupper($current_module) .'.'. strtoupper($permission)]);
        }
    }
    return $current_permissions;
}

function checkUserHasRequiredData(){
    if(auth()->loggedIn()){
        $user = auth()->user();
        if ($user->inGroup('user')) {
           $clientModel = new ClientsModel();
           return $clientModel->hasRequiredData($user->id);
        }
    }
}