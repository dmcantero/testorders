<?php

namespace App\Controllers;

use App\Models\CategoriesModel;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use Psr\Log\LoggerInterface;
use Spatie\Menu\Link;
use Spatie\Menu\Menu as MenuMenu;
use Tatter\Menus\Breadcrumb;
use Tatter\Menus\Config\Menus;
use Tatter\Menus\Menu;
use Tatter\Menus\Menus\BreadcrumbsMenu;

class Home extends BaseController
{

    public function initController(RequestInterface $request, ResponseInterface $response, LoggerInterface $logger)
    {
        parent::initController($request, $response, $logger);
    }

    public function index()
    {
        $categoryModel = new CategoriesModel();
        $data['categories'] = $categoryModel->where('show_in_landing', 1)->orderBy('name', 'asc')->findAll();
        return view('Landing/' . env("NAME_COMPANY") . '/Home/Index', $data);
    }
}
