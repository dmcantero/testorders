<?php

namespace App\Controllers\Catalogue;

use App\Controllers\Catalogue\CatalogueBaseController;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use Psr\Log\LoggerInterface;

class ProductController extends CatalogueBaseController
{
    public function initController(RequestInterface $request, ResponseInterface $response, LoggerInterface $logger)
    {
        parent::initController($request, $response, $logger);
    }

    public function index()
    {
        return view('Catalogue/Product/Index', []);
    }
}
