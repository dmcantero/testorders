<?php

namespace App\Controllers\Catalogue;

use App\Controllers\Catalogue\CatalogueBaseController;
use App\Entities\ClientEntity;
use App\Entities\UserEntity;
use App\Models\CategoriesModel;
use App\Models\ClientsModel;
use App\Models\UsersModel;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use Psr\Log\LoggerInterface;

class HomeController extends CatalogueBaseController
{
    public function initController(RequestInterface $request, ResponseInterface $response, LoggerInterface $logger)
    {
        parent::initController($request, $response, $logger);
    }

    public function index()
    {
        if (!checkUserHasRequiredData()) {
            session()->setFlashdata('message', "Debe completar su información antes de continuar.");
            return redirect('catalogue/profile');
        }
        return view('Catalogue/Home/Index', []);
    }



    // public function category($name = null){
    //     try {
    //         if($name == "" || $name == NULL){
    //             return $this->response->redirect("/catalogue");
    //         }

    //         $name = strtolower(trim(rawurldecode($name)));

    //         $categoryModel = new CategoriesModel();

    //         $category = $categoryModel->where('LOWER(name)', $name)->first();

    //         if($category == NULL){
    //             return $this->response->redirect("/catalogue");
    //         }

    //         return view('Catalogue/Home/Index', []);
    //     } catch (\Throwable $th) {
    //         log_message('error', "Error: " . $th->__toString());
    //         throw $th;
    //     }
    // }

    public function myorders()
    {
        try {
            $modelClient = new ClientsModel();
            $userId = auth()->id();
            $data['clientId'] = $modelClient->where(['user_id' => $userId, 'company_id' => session('company_id')])->first()->id;
            return view('Catalogue/Home/MyOrders', $data);
        } catch (\Throwable $th) {
            log_message('error', "Error: " . $th->__toString());
            throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
        }
    }

    public function profile()
    {
        try {
            return view('Catalogue/Home/UserProfile');
        } catch (\Throwable $th) {
            log_message('error', "Error: " . $th->__toString());
            throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
        }
    }
}
