<?php

namespace App\Controllers\Admin;

use App\Controllers\BaseController;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use Psr\Log\LoggerInterface;

/**
 * Class BaseController
 *
 * BaseController provides a convenient place for loading components
 * and performing functions that are needed by all your controllers.
 * Extend this class in any new controllers:
 *     class Home extends BaseController
 *
 * For security be sure to declare any new methods as protected or private.
 */
abstract class AdminBaseController extends BaseController
{
    protected $auth;
    protected $user;
    protected $permissions;

    public function initController(RequestInterface $request, ResponseInterface $response, LoggerInterface $logger)
    {
        parent::initController($request, $response, $logger);
        $this->auth         = service('auth');
        $this->user         = $this->auth->user();
        $this->permissions  = $this->user->getPermissions();
        Config(\Config\SettingsApp::class);
        helper('permissions');

        // if($this->user->isBanned()){
        //     $url = config('Auth')->logoutRedirect();
        //     auth()->logout();
        //     return redirect()->to($url)->with('message', lang(lang('Auth.logOutBannedUser')));
        // }
    }
}
