<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use Psr\Log\LoggerInterface;
use CodeIgniter\HTTP\RedirectResponse;

class Cart extends BaseController
{
    protected $validation;
    protected $auth;

    public function initController(RequestInterface $request, ResponseInterface $response, LoggerInterface $logger)
    {
        parent::initController($request, $response, $logger);
        $this->auth         = service('auth');
        $this->validation = \Config\Services::validation();
    }

    public function index(){
        return view('Cart/Index');
    }

    public function success(){
        $data['numero'] = $this->request->getGet('NP');
        return View("Cart/Success", $data);
    }
}
