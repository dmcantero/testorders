<?php

namespace App\Api;

use CodeIgniter\HTTP\RequestInterface;
use Psr\Log\LoggerInterface;
use CodeIgniter\HTTP\ResponseInterface;
use App\Models\FeaturesValuesModel;
use App\Entities\FeaturesValueEntity;
use CodeIgniter\RESTful\ResourceController;
use CodeIgniter\API\ResponseTrait;
use Config\Services;

class FeaturesValues extends ResourceController
{
    use ResponseTrait;

    protected $request;
    protected $auth;
    protected $user;
    protected $permissions;


    protected $validation;


    /**
     * @var FeaturesValuesModel
     */
    protected $model;

    public function initController(RequestInterface $request, ResponseInterface $response, LoggerInterface $logger)
    {
        $this->response = Services::response();
        $this->request = \Config\Services::request();
        $this->model = new FeaturesValuesModel();
        $this->validation = \Config\Services::validation();

        $this->validation->setRules([
            'value'              => ['label' => 'Valor',             'rules'         => 'required|max_length[250]'],
            'feature_id'         => ['label' => 'Atributo',   'rules'          => 'required']
        ]);
    }

    /**
     * Return an array of resource objects, themselves in array format
     *
     * @return mixed
     */
    public function index()
    {
        try {
            $data = [];

            $requestData = $this->request->getGet();
            $data = $this->model->where(['feature_id' => $requestData['feature_id'], 'company_id' => 1])->findAll(); //TODO: Cambiar por la company_id de la session
            return $this->respond($data, 200);
        } catch (\Throwable $th) {
            log_message('error', '[ERROR] {exception}', ['exception' => $th]);
            return $this->respond(['messages' => ['' => $th->getMessage()], 'status' => false], 500);
        }
    }

    /**
     * Return the properties of a resource object
     *
     * @return mixed
     */
    public function show($id = null)
    {
        try {
            if ($id == 0) {
                $data = new FeaturesValueEntity();
            } else {
                $data = $this->model->find($id);
            }
            return $this->respond($data, 200);
        } catch (\Throwable $th) {
            log_message('error', '[ERROR] {exception}', ['exception' => $th]);
            return $this->respond(['messages' => ['' => $th->getMessage()], 'status' => false], 500);
        }
    }

    /**
     * Return a new resource object, with default properties
     *
     * @return mixed
     */
    public function new()
    {
        try {
            $data = new FeaturesValueEntity();
            return $this->respond($data, 200);
        } catch (\Throwable $th) {
            log_message('error', '[ERROR] {exception}', ['exception' => $th]);
            return $this->respond(['messages' => ['' => $th->getMessage()], 'status' => false], 500);
        }
    }

    /**
     * Create a new resource object, from "posted" parameters
     *
     * @return mixed
     */
    public function create()
    {
        try {
            $data = (array)$this->request->getJSON();

            if ($this->validation->run($data) == FALSE) {
                $response['status']    = false;
                $response['messages']   = $this->validation->getErrors(); //Show Error in Input Form

                return $this->respond($response, 422);
            }

            $entity = new FeaturesValueEntity($data);
            $result = $this->model->insert($entity, true);
            $response = [
                'status' => true,
                'data' => ['id' => $result]
            ];
            return $this->respond($response, 200);
        } catch (\Throwable $th) {
            log_message('error', '[ERROR] {exception}', ['exception' => $th]);
            return $this->respond(['messages' => ['' => $th->getMessage()], 'status' => false], 500);
        }
    }

    /**
     * Return the editable properties of a resource object
     *
     * @return mixed
     */
    public function edit($id = null)
    {
        //
    }

    /**
     * Add or update a model resource, from "posted" properties
     *
     * @return mixed
     */
    public function update($id = null)
    {
        try {
            $data = (array)$this->request->getJSON();

            if ($this->validation->run($data) == FALSE) {
                $response['status']    = false;
                $response['messages']   = $this->validation->getErrors(); //Show Error in Input Form

                return $this->respond($response, 422);
            }

            $entity = new FeaturesValueEntity();
            $entity->fill($data);
            $result = $this->model->update($id, $entity);
            $response = [
                'status' => true,
                'data' => ['id' => $id]
            ];
            return $this->respond($response, 200);
        } catch (\Throwable $th) {
            log_message('error', '[ERROR] {exception}', ['exception' => $th]);
            return $this->respond(['messages' => ['' => $th->getMessage()], 'status' => false], 500);
        }
    }

    /**
     * Delete the designated resource object from the model
     *
     * @return mixed
     */
    public function delete($id = null)
    {
        try {
            $original = $this->model->find($id);
            $result = $this->model->delete($id);
            $response = [
                'status' => true,
                'data' => ['id' => $id]
            ];
            return $this->respond($response, 200);
        } catch (\Throwable $th) {
            log_message('error', '[ERROR] {exception}', ['exception' => $th]);
            return $this->respond(['messages' => ['' => $th->getMessage()], 'status' => false], 500);
        }
    }

    /**
     * Get list of resources to select list
     * 
     * @return array
     */
    public function select($id)
    {
        try {
            $companyId = 1;
            $data = $this->model->select('id, value AS text')->where(['feature_id' => $id, 'company_id' => $companyId])->orderBy('value')->get()->getResult();

            return $this->respond($data, 200);
        } catch (\Throwable $th) {
            return $this->respond([], 500);
        }
    }
}
