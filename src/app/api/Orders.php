<?php

namespace App\Api;

use App\Entities\Enums\OrderStatusEnum;
use CodeIgniter\HTTP\RequestInterface;
use Psr\Log\LoggerInterface;
use CodeIgniter\HTTP\ResponseInterface;
use App\Models\OrdersModel;
use App\Entities\OrderEntity;
use CodeIgniter\RESTful\ResourceController;
use CodeIgniter\API\ResponseTrait;
use Config\Services;
use App\Entities\Filters\OrdersFilters;
use App\Models\OrdersDetailsModel;
use \CodeIgniter\Events\Events;
use Config\SettingsApp;

class Orders extends ResourceController
{
    use ResponseTrait;

    protected $request;
    protected $auth;
    protected $user;
    protected $permissions;


    protected $validation;


    /**
     * @var OrdersModel
     */
    protected $model;

    /**
     * @var OrdersDetailsModel
     */
    protected $modelDetails;

    public function initController(RequestInterface $request, ResponseInterface $response, LoggerInterface $logger)
    {
        $this->response = Services::response();
        $this->request = \Config\Services::request();
        $this->model = new OrdersModel();
        $this->modelDetails = new OrdersDetailsModel();

        $this->validation = \Config\Services::validation();
    }

    /**
     * Return an array of resource objects, themselves in array format
     *
     * @return mixed
     */
    public function index()
    {
        try {
            $data = [];
            // if ($this->request->isAJAX()) {
            $requestData = $this->request->getGet();
            $filter = new OrdersFilters($requestData);
            $data = $this->model->search(0, $filter->client_id, $filter->number, $filter->date_from, $filter->date_until, $filter->status, $filter->currency, $filter->OrderBy, $filter->PageIndex, $filter->PageSize);
            // }

            return $this->respond($data, 200);
        } catch (\Throwable $th) {
            log_message('error', '[ERROR] {exception}', ['exception' => $th]);
            return $this->respond(['messages' => ['' => $th->getMessage()], 'status' => false], 500);
        }
    }

    public function historyclient($id = NULL)
    {
        try {
            $data = [];
            if ($id <= 0) {
                return $this->respond(['status' => false, 'message' => "Id incorrecto"], 404);
            }
            // if ($this->request->isAJAX()) {
            $requestData = $this->request->getGet();
            $filter = new OrdersFilters($requestData);
            $data = $this->model->searchByClient($id, $filter->date_from, $filter->date_until, $filter->status, $filter->currency, $filter->OrderBy, $filter->PageIndex, $filter->PageSize);
            // }

            return $this->respond($data, 200);
        } catch (\Throwable $th) {
            log_message('error', '[ERROR] {exception}', ['exception' => $th]);
            return $this->respond(['messages' => ['' => $th->getMessage()], 'status' => false], 500);
        }
    }

    /**
     * Return the properties of a resource object
     *
     * @return mixed
     */
    public function show($id = null)
    {
        try {
            $config = new SettingsApp();
            $default_currency = $config->get('default_currency_admin')['value'];
            $dataOrder = $this->model->search($id, 0, '', '', '', '', $default_currency, '', 1, 1)["Rows"];

            if (sizeof($dataOrder) == 0) {
                return $this->failNotFound();
            }

            $data = $dataOrder[0];
            $data->details = $this->modelDetails->search($id, $default_currency);

            return $this->respond($data, 200);
        } catch (\Throwable $th) {
            log_message('error', '[ERROR] {exception}', ['exception' => $th]);
            return $this->respond(['messages' => ['' => $th->getMessage()], 'status' => false], 500);
        }
    }

    /**
     * Return a new resource object, with default properties
     *
     * @return mixed
     */
    public function new()
    {
        try {
            $data = new OrderEntity();
            $data->active = 1;
            return $this->respond($data, 200);
        } catch (\Throwable $th) {
            log_message('error', '[ERROR] {exception}', ['exception' => $th]);
            return $this->respond(['messages' => ['' => $th->getMessage()], 'status' => false], 500);
        }
    }

    /**
     * Create a new resource object, from "posted" parameters
     *
     * @return mixed
     */
    public function create()
    {
        try {
            $data = (array)$this->request->getJSON();
            $modelClient = new \App\Models\ClientsModel();
            $cart = \Config\Services::cart();
            // if ($this->validation->run($data) == FALSE) {
            //     $response['status']    = false;
            //     $response['messages']   = $this->validation->getErrors(); //Show Error in Input Form

            //     return $this->respond($response, 422);
            // }
            $cartContent = json_decode(json_encode($cart->contents()), true);

            //data from current user/client
            $userId = auth()->id();
            $clientId = $modelClient->where(['user_id' => $userId, 'company_id' => session('company_id')])->first()->id;
            $username = auth()->user()->username;

            //process new order
            $resultOrder = $this->model->process(0, $clientId, $userId, null, null, null, $data['shipping_method'], $data['free_shipping'], "", 0, 0, 0, 0, "", OrderStatusEnum::ISSUED, $username);

            //process order's details
            $orderDetails = array_map(
                fn ($item) =>
                [
                    'order_id'          => $resultOrder['id'],
                    'date'              => date('Y-m-d H:i:s', time()),
                    'product_id'        => $item['id'],
                    'bulk_items'        => $item['options']['bulk_items'],
                    'bulk_price'        => $item['options']['bulk_price'],
                    'bulk_unit_price'   => $item['options']['bulk_unit_price'],
                    'unit_price'        => $item['price'],
                    'quantity'          => $item['qty'],
                    'subtotal'          => $item['qty'] * $item['price'],
                    'company_id'        => session('company_id'),
                    'created_by'        => $username,
                    'created_at'        => date('Y-m-d H:i:s', time()),
                ],
                $cartContent
            );
            $resultDetails = $this->modelDetails->insertBatch($orderDetails);

            //$this->EmailOrderIssued($resultOrder);
            $sended             = Events::trigger('emailOrder', $resultOrder['id'], true);

            $response = [
                'status' => true,
                'data' => $resultOrder
            ];
            return $this->respond($response, 200);
        } catch (\Throwable $th) {
            log_message('error', '[ERROR] {exception}', ['exception' => $th]);
            return $this->respond(['messages' => ['' => $th->getMessage()], 'status' => false], 500);
        }
    }

    /**
     * Return the editable properties of a resource object
     *
     * @return mixed
     */
    public function edit($id = null)
    {
        //
    }

    /**
     * Add or update a model resource, from "posted" properties
     *
     * @return mixed
     */
    public function update($id = null)
    {
        try {
            $data = json_decode(json_encode($this->request->getJSON()), true);

            // if ($this->validation->run($data) == FALSE) {
            //     $response['status']    = false;
            //     $response['messages']   = $this->validation->getErrors(); //Show Error in Input Form

            //     return $this->respond($response, 422);
            // }
            $username = auth()->user()->username;

            $resultOrder = $this->model->process($data['Id'], $data['client_id'], $data['user_id'], $data['confirmation_date'], $data['delivery_date'], $data['shipping_date'], $data['shipping_method'], $data['free_shipping'], $data['client_fullname'], $data['estimate_id'], $data['discount_percentage'], $data['sale_condition_id'], $data['credit_term_id'], $data['notes'], $data['status'],  $username);

            //process order's details
            $orderDetails = array_map(
                fn ($item) =>
                [
                    'id'                    => $item['id'],
                    'order_id'              => $item['order_id'],
                    'date'                  => date('Y-m-d H:i:s', time()),
                    'product_id'            => $item['product_id'],
                    'bulk_items'            => $item['bulk_items'],
                    'bulk_price'            => $item['original_bulk_price'],
                    'bulk_unit_price'       => $item['original_bulk_unit_price'],
                    'unit_price'            => $item['original_unit_price'],
                    'quantity'              => $item['quantity'],
                    'subtotal'              => $item['original_subtotal'],
                    'confirmed_quantity'    => $item['confirmed_quantity'] ?? 0,
                    'confirmed_subtotal'    => $item['original_confirmed_subtotal'] ?? 0,
                    'company_id'            => session('company_id'),
                    'updated_by'            => $username,
                    'updated_at'            => date('Y-m-d H:i:s', time()),
                ],
                $data['details']
            );
            $resultDetails = $this->modelDetails->updateBatch($orderDetails, 'id');

            if ($resultOrder['@currentStatus'] != OrderStatusEnum::CONFIRMED && $resultOrder['orderStatus'] == OrderStatusEnum::CONFIRMED) {
                $sended  = Events::trigger('emailOrder', $data['Id'], true);
            }
            
            $response = [
                'status' => true,
                'data' => ['id' => $id]
            ];
            return $this->respond($response, 200);
        } catch (\Throwable $th) {
            log_message('error', '[ERROR] {exception}', ['exception' => $th]);
            return $this->respond(['messages' => ['' => $th->getMessage()], 'status' => false], 500);
        }
    }

    /**
     * Delete the designated resource object from the model
     *
     * @return mixed
     */
    public function delete($id = null)
    {
        try {
            $original = $this->model->find($id);
            $result = $this->model->delete($id);
            $response = [
                'status' => true,
                'data' => ['id' => $id]
            ];
            return $this->respond($response, 200);
        } catch (\Throwable $th) {
            log_message('error', '[ERROR] {exception}', ['exception' => $th]);
            return $this->respond(['messages' => ['' => $th->getMessage()], 'status' => false], 500);
        }
    }

    /**
     * Get list of resources to select list
     * 
     * @return array
     */
    public function select()
    {
        try {
            $data = $this->model->select('id, name AS text')->orderBy('name')->get()->getResult();

            return $this->respond($data, 200);
        } catch (\Throwable $th) {
            return $this->respond([], 500);
        }
    }
}
