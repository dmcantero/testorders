<?php

namespace App\Api;

use CodeIgniter\HTTP\RequestInterface;
use Psr\Log\LoggerInterface;
use CodeIgniter\HTTP\ResponseInterface;
use App\Models\ProductsModel;
use App\Entities\ProductEntity;
use CodeIgniter\RESTful\ResourceController;
use CodeIgniter\API\ResponseTrait;
use Config\Services;
use App\Entities\Filters\ProductsFilters;

class Catalogue extends ResourceController
{
    use ResponseTrait;

    protected $request;
    protected $auth;
    protected $user;
    protected $permissions;


    protected $validation;


    /**
     * @var ProductsModel
     */
    protected $model;

    public function initController(RequestInterface $request, ResponseInterface $response, LoggerInterface $logger)
    {
        $this->response = Services::response();
        $this->request = \Config\Services::request();
        $this->model = new ProductsModel();
        $this->validation = \Config\Services::validation();
    }

    /**
     * Return an array of resource objects, themselves in array format
     *
     * @return mixed
     */
    public function index()
    {
        try {
            $data = [];

            $requestData = $this->request->getGet();
            $categoryId = $requestData['categoryId'] ?? '';
            $currency   = $requestData['currency'];
            $search     = $requestData['search'] ?? '';
            $minPrice   = $requestData['minPrice'] ?? 0;
            $maxPrice   = $requestData['maxPrice'] ?? 0;
            $available  = $requestData['available'] ?? 0;
            $offer      = $requestData['offer'] ?? 0;

            $pageIndex  = $requestData['pageIndex'] ?? 1;
            $pageSize   = $requestData['pageSize'] ?? 12;
            $orderBy    = $requestData['orderBy'] ?? '';

            $data = $this->model->catalogueClient($categoryId, $currency, $search, $available, $offer, $minPrice, $maxPrice, $orderBy, $pageIndex, $pageSize, session('company_id'));


            return $this->respond($data, 200);
        } catch (\Throwable $th) {
            log_message('error', '[ERROR] {exception}', ['exception' => $th]);
            return $this->respond(['messages' => ['' => $th->getMessage()], 'status' => false], 500);
        }
    }

    /**
     * Return the properties of a resource object
     *
     * @return mixed
     */
    public function show($id = null)
    {
        try {
            if ($id == 0) {
                $data = new ProductEntity();
            } else {
                $data = $this->model->find($id);
            }
            return $this->respond($data, 200);
        } catch (\Throwable $th) {
            log_message('error', '[ERROR] {exception}', ['exception' => $th]);
            return $this->respond(['messages' => ['' => $th->getMessage()], 'status' => false], 500);
        }
    }

    /**
     * Return a new resource object, with default properties
     *
     * @return mixed
     */
    public function new()
    {
        try {
            $data = new ProductEntity();
            $data->active = 1;
            return $this->respond($data, 200);
        } catch (\Throwable $th) {
            log_message('error', '[ERROR] {exception}', ['exception' => $th]);
            return $this->respond(['messages' => ['' => $th->getMessage()], 'status' => false], 500);
        }
    }

    /**
     * Create a new resource object, from "posted" parameters
     *
     * @return mixed
     */
    public function create()
    {
        try {
            $data = (array)$this->request->getJSON();

            if ($this->validation->run($data) == FALSE) {
                $response['status']    = false;
                $response['messages']   = $this->validation->getErrors(); //Show Error in Input Form

                return $this->respond($response, 422);
            }

            $entity = new ProductEntity($data);
            $result = $this->model->insert($entity, true);
            $response = [
                'status' => true,
                'data' => ['id' => $result]
            ];
            return $this->respond($response, 200);
        } catch (\Throwable $th) {
            log_message('error', '[ERROR] {exception}', ['exception' => $th]);
            return $this->respond(['messages' => ['' => $th->getMessage()], 'status' => false], 500);
        }
    }

    /**
     * Return the editable properties of a resource object
     *
     * @return mixed
     */
    public function edit($id = null)
    {
        //
    }

    /**
     * Add or update a model resource, from "posted" properties
     *
     * @return mixed
     */
    public function update($id = null)
    {
        try {
            $data = (array)$this->request->getJSON();

            if ($this->validation->run($data) == FALSE) {
                $response['status']    = false;
                $response['messages']   = $this->validation->getErrors(); //Show Error in Input Form

                return $this->respond($response, 422);
            }

            $entity = new ProductEntity();
            $entity->fill($data);
            $result = $this->model->update($id, $entity);
            $response = [
                'status' => true,
                'data' => ['id' => $id]
            ];
            return $this->respond($response, 200);
        } catch (\Throwable $th) {
            log_message('error', '[ERROR] {exception}', ['exception' => $th]);
            return $this->respond(['messages' => ['' => $th->getMessage()], 'status' => false], 500);
        }
    }

    /**
     * Delete the designated resource object from the model
     *
     * @return mixed
     */
    public function delete($id = null)
    {
        try {
            $original = $this->model->find($id);
            $result = $this->model->delete($id);
            $response = [
                'status' => true,
                'data' => ['id' => $id]
            ];
            return $this->respond($response, 200);
        } catch (\Throwable $th) {
            log_message('error', '[ERROR] {exception}', ['exception' => $th]);
            return $this->respond(['messages' => ['' => $th->getMessage()], 'status' => false], 500);
        }
    }

    /**
     * Get list of resources to select list
     * 
     * @return array
     */
    public function select()
    {
        try {
            $data = $this->model->select('id, name AS text')->orderBy('name')->get()->getResult();

            return $this->respond($data, 200);
        } catch (\Throwable $th) {
            return $this->respond([], 500);
        }
    }

    /**
     * Get list of resources to select list
     * 
     * @return array
     */
    public function getfeatures($id)
    {
        try {
            $data = $this->model->getProductFeatures($id);

            return $this->respond(['data' => $data], 200);
        } catch (\Throwable $th) {
            log_message('error', '[ERROR] {exception}', ['exception' => $th]);
            return $this->respond(['message' => $th->getMessage()], 500);
        }
    }
}
