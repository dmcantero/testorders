<?php

namespace App\Api;

use CodeIgniter\HTTP\RequestInterface;
use Psr\Log\LoggerInterface;
use CodeIgniter\HTTP\ResponseInterface;
use App\Models\ProductsModel;
use App\Entities\ProductEntity;
use CodeIgniter\RESTful\ResourceController;
use CodeIgniter\API\ResponseTrait;
use Config\Services;
use App\Entities\Filters\ProductsFilters;
use App\Entities\UploadEntity;
use App\Models\UploadsModel;
use CodeIgniter\HTTP\Exceptions\HTTPException;

class Files extends ResourceController
{
    use ResponseTrait;

    protected $request;
    protected $auth;
    protected $user;
    protected $permissions;


    protected $validation;


    /**
     * @var UploadsModel
     */
    protected $model;

    public function initController(RequestInterface $request, ResponseInterface $response, LoggerInterface $logger)
    {
        $this->response = Services::response();
        $this->request = \Config\Services::request();
        $this->model = new UploadsModel();
        $this->validation = \Config\Services::validation();
    }

    public function showFile($filename)
    {
        helper("filesystem");
        $path = WRITEPATH . 'uploads/';

        $fullpath = $path . $filename;
        $file = new \CodeIgniter\Files\File($fullpath, true);
        $binary = readfile($fullpath);
        return $this->response
            ->setHeader('Content-Type', $file->getMimeType())
            ->setHeader('Content-disposition', 'inline; filename="' . $file->getBasename() . '"')
            ->setStatusCode(200)
            ->setBody($binary);
    }

    public function deleteFile($id)
    {
        try {
            $uploadsModel = new UploadsModel();
            $path = WRITEPATH . 'uploads/';
            $file = $uploadsModel->find($id);

            if ($file == NULL) {
                return $this->failNotFound();
            }

            $fullpath = $path . $file->new_name;
            $file = new \CodeIgniter\Files\File($fullpath, true);
            if (file_exists($file)) {
                unlink($file);
            }

            $removed = $uploadsModel->delete($id);
            return $this->respond(['status' => true], 200);
        } catch (\Throwable $th) {
            log_message('error', '[ERROR] {exception}', ['exception' => $th]);
            return $this->respond(['messages' => ['' => $th->getMessage()], 'status' => false], 500);
        }
    }
}
