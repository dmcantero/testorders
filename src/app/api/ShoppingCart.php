<?php

namespace App\Api;

use CodeIgniter\HTTP\RequestInterface;
use Psr\Log\LoggerInterface;
use CodeIgniter\HTTP\ResponseInterface;
use App\Models\ProductsModel;
use App\Entities\ProductEntity;
use CodeIgniter\RESTful\ResourceController;
use CodeIgniter\API\ResponseTrait;
use Config\Services;
use App\Entities\Filters\ProductsFilters;

class ShoppingCart extends ResourceController
{
    use ResponseTrait;

    protected $request;
    protected $auth;
    protected $user;
    protected $permissions;


    protected $validation;


    /**
     * @var ProductsModel
     */
    protected $model;

    public function initController(RequestInterface $request, ResponseInterface $response, LoggerInterface $logger)
    {
        $this->response = Services::response();
        $this->request = \Config\Services::request();
        $this->model = new ProductsModel();
        $this->validation = \Config\Services::validation();
    }

    /**
     * Return an array of resource objects, themselves in array format
     *
     * @return mixed
     */
    public function index()
    {
        try {
            $cart = \Config\Services::cart();
            $data =  json_decode(json_encode($cart->contents()), true);
            return $this->respond($data, 200);
        } catch (\Throwable $th) {
            log_message('error', '[ERROR] {exception}', ['exception' => $th]);
            return $this->respond(['messages' => ['' => $th->getMessage()], 'status' => false], 500);
        }
    }


    public function add()
    {
        try {
            $data =   (array)$this->request->getJSON();
            $cart = \Config\Services::cart();
            $cart->productNameSafe = false;
            // $exists = array_values(array_filter($contents, fn($item) => $item['id'] == $data['id']));

            // if (count($exists) == 1) {
            //     $rowid = $exists[0]['rowid'];

            //     $result = $cart->update(array(
            //         'rowid'   => $rowid,
            //         'id'      => $data['id'],
            //         'qty'     => $data['quantity'],
            //         'price'   => $data['unit_price'],
            //         'name'    => $data['name'],
            //         'options' => array('bulk_price_id' => $data['bulk_price_id'], 'bulk_price_id' => $data['bulk_price_qty'])
            //     ));
            // } else {
            $result = $cart->insert(array(
                'id'      => $data['id'],
                'qty'     => $data['quantity'],
                'price'   => $data['unit_price'],
                'name'    => $data['name'],
                'options' => array(
                    'bulk_items'        => $data['bulk_items'],
                    'bulk_price'        => $data['bulk_price'],
                    'bulk_unit_price'   => $data['bulk_unit_price'],
                    'image'             => $data['image']
                )
            ));
            // }

            $response = [
                'status' => true,
                'data' => $result
            ];
            return $this->respond($response, 200);
        } catch (\Throwable $th) {
            log_message('error', '[ERROR] {exception}', ['exception' => $th]);
            return $this->respond(['messages' => ['' => $th->getMessage()], 'status' => false], 500);
        }
    }

    /**
     * Add or update a model resource, from "posted" properties
     *
     * @return mixed
     */
    public function update($id = null)
    {
        try {
            $data = (array)$this->request->getJSON();

            $cart = \Config\Services::cart();
            $cart->productNameSafe = false;
            
            $result = $cart->update(array(
                'rowid'   => $id,
                'qty'     => $data['qty']
            ));

            $response = [
                'status' => true,
                'data' => $result
            ];
            return $this->respond($response, 200);
        } catch (\Throwable $th) {
            log_message('error', '[ERROR] {exception}', ['exception' => $th]);
            return $this->respond(['messages' => ['' => $th->getMessage()], 'status' => false], 500);
        }
    }

    /**
     * Delete the designated resource object from the model
     *
     * @return mixed
     */
    public function delete($id = null)
    {
        try {
            $cart = \Config\Services::cart();
            $cart->remove($id);
            $response = [
                'status' => true
            ];
            return $this->respond($response, 200);
        } catch (\Throwable $th) {
            log_message('error', '[ERROR] {exception}', ['exception' => $th]);
            return $this->respond(['messages' => ['' => $th->getMessage()], 'status' => false], 500);
        }
    }

    /**
     * Clear the shopping cart
     */
    public function clear()
    {
        try {
            $cart = \Config\Services::cart();
            $cart->destroy();
            $response = [
                'status' => true
            ];
            return $this->respond($response, 200);
        } catch (\Throwable $th) {
            log_message('error', '[ERROR] {exception}', ['exception' => $th]);
            return $this->respond(['messages' => ['' => $th->getMessage()], 'status' => false], 500);
        }
    }
}
