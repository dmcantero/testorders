<?php

namespace App\Api;

use App\Entities\ClientEntity;
use CodeIgniter\HTTP\RequestInterface;
use Psr\Log\LoggerInterface;
use CodeIgniter\HTTP\ResponseInterface;
use App\Models\UsersModel;
use App\Entities\UserEntity;
use App\Models\ClientsModel;
use CodeIgniter\RESTful\ResourceController;
use CodeIgniter\API\ResponseTrait;
use Config\Services;
use \CodeIgniter\Events\Events;
use \CodeIgniter\Config\Factories;

class Users extends ResourceController
{
    use ResponseTrait;

    protected $request;
    protected $auth;
    protected $user;
    protected $permissions;


    protected $validation;


    /**
     * @var UsersModel
     */
    protected $model;

    public function initController(RequestInterface $request, ResponseInterface $response, LoggerInterface $logger)
    {
        $this->response = Services::response();
        $this->request = \Config\Services::request();
        $this->model = new UsersModel();
        $this->validation = \Config\Services::validation();

        $this->validation->setRules([
            'username'  =>   [
                'label' => "Nombre de usuario",
                'rules' => 'required|max_length[30]|min_length[3]|is_unique[users.username]'
            ],
            'email'     =>   [
                'label' => "Email",
                'rules' => 'required|max_length[100]|valid_email|is_unique[auth_identities.secret]'
            ],
            'first_name'     =>   [
                'label' => "Nombre",
                'rules' => 'max_length[100]'
            ],
            'last_name'     =>   [
                'label' => "Apellido",
                'rules' => 'max_length[100]'
            ],
            'password' => [
                'label'  => 'Auth.password',
                'rules' => [
                    'required',
                    'max_byte[72]',
                ],
                'errors' => [
                    'max_byte' => 'Auth.errorPasswordTooLongBytes',
                ]
            ],
            'password_confirm' => [
                'label' => 'Auth.passwordConfirm',
                'rules' => 'required|matches[password]',
            ],
            'group'            => [
                'label' => 'Grupo',
                'rules' => 'required'
            ]
        ]);
    }

    /**
     * Return an array of resource objects, themselves in array format
     *
     * @return mixed
     */
    public function index()
    {
        try {
            $data = [];
            if ($this->request->isAJAX()) {
                $requestData = $this->request->getGet();
                $data = $this->model->search($requestData['username'] ?? '', $requestData['email'] ?? '', $requestData['OrderBy'],  $requestData['PageIndex'], $requestData['PageSize']);
            }

            return $this->respond($data, 200);
        } catch (\Throwable $th) {
            log_message('error', '[ERROR] {exception}', ['exception' => $th]);
            return $this->respond(['messages' => ['' => $th->getMessage()], 'status' => false], 500);
        }
    }

    /**
     * Return the properties of a resource object
     *
     * @return mixed
     */
    public function show($id = null)
    {
        try {
            if ($id == 0) {
                $data = new UserEntity();
            } else {
                $user = $this->model->findById($id);
                if ($user == null) {
                    return $this->failNotFound();
                }
                $data = [
                    'id'            => $user->id,
                    'first_name'    => $user->first_name,
                    'last_name'     => $user->last_name,
                    'username'      => $user->username,
                    'email'         => $user->getEmail(),
                    'group'         => $user->getGroups()[0],
                    'active'        => $user->active,
                ];
            }
            return $this->respond($data, 200);
        } catch (\Throwable $th) {
            log_message('error', '[ERROR] {exception}', ['exception' => $th]);
            return $this->respond(['messages' => ['' => $th->getMessage()], 'status' => false], 500);
        }
    }

    /**
     * Return a new resource object, with default properties
     *
     * @return mixed
     */
    public function new()
    {
        try {
            $data = new UserEntity();
            $data->username = null;
            $data->email = "";
            $data->first_name = null;
            $data->last_name = null;
            $data->password = "";
            $data->group = config(\Config\AuthGroups::class)->defaultGroup;
            $data->notify = true;

            return $this->respond($data, 200);
        } catch (\Throwable $th) {
            log_message('error', '[ERROR] {exception}', ['exception' => $th]);
            return $this->respond(['messages' => ['' => $th->getMessage()], 'status' => false], 500);
        }
    }

    /**
     * Create a new resource object, from "posted" parameters
     *
     * @return mixed
     */
    public function create()
    {
        try {
            if (!auth()->user()->hasPermission('users.create')) {
                $response['status']        = false;
                $response['messages']       = lang("App.invalid_permission");
                return $this->failForbidden();
            }

            $data = (array)$this->request->getJSON();
            $response = array();


            if ($this->validation->run($data) == FALSE) {
                $response['status']    = false;
                $response['messages']   = $this->validation->getErrors(); //Show Error in Input Form

                return $this->respond($response, 422);
            } else {
                $users                          = auth()->getProvider();
                $user                           = new UserEntity([
                    'company_id'                => session('company_id'),
                    'username'                  => $data['username'],
                    'email'                     => $data['email'],
                    'password'                  => $data['password'],
                    'first_name'                => $data['first_name'],
                    'last_name'                 => $data['last_name'],
                    'status'                    => 'OFF',
                    'status_message'            => 'New User',
                ]);
                // save the user with the above information
                $users->insert($user);
                // get the new ID as we still have work to do
                $user                           = $users->findById($users->getInsertID());
                // set the flag to make user change password on first login
                $user->forcePasswordReset();
                // make sure this is the only group(s) for the user
                $user->syncGroups($data['group']);

                // Additional work done here..
                // $actionClass                    = setting('Auth.actions')['register'] ?? null;
                // $action                         = Factories::actions($actionClass)->createIdentity($user);
                // $code                           = $action; // do not need this yet though it is set
                $tmpPass                        = $data['password'];
                $notify                         = $data['notify'];

                //check if is user client
                if ($data['group'] == Config(\Config\AuthGroups::class)->defaultGroup) {
                    $resultClient = Events::trigger('register', $user);
                }

                // trigger our new Event and send the two variables 
                $confirm                        = Events::trigger('newUser', $user, $tmpPass, $notify);
                // if eveything went well, notifuy the Admin the user has been added and email sent
                if ($confirm) {
                    $response = [
                        'status' => true,
                        'messages'  => lang("App.insert-success"),
                        'data' => ['id' => $user->id]
                    ];
                } else {
                    $response = [
                        'status' => false,
                        'messages'  => lang("App.insert-error")
                    ];
                }
            }
            return $this->respond($response, 200);
        } catch (\Throwable $th) {
            log_message('error', '[ERROR] {exception}', ['exception' => $th]);
            return $this->respond(['messages' => ['' => $th->getMessage()], 'status' => false], 500);
        }
    }

    /**
     * Return the editable properties of a resource object
     *
     * @return mixed
     */
    public function edit($id = null)
    {
    }

    /**
     * Add or update a model resource, from "posted" properties
     *
     * @return mixed
     */
    public function update($id = null)
    {
        try {
            if (!auth()->user()->hasPermission('users.edit')) {
                $response['status']        = false;
                $response['messages']       = lang("App.invalid_permission");
                return $this->failForbidden();
            }

            $this->validation->setRules([
                'id'        => [
                    'label' => 'Id',
                    'rules' => 'required'
                ],
                'username'  =>   [
                    'label' => "Nombre de usuario",
                    'rules' => 'required|max_length[30]|min_length[3]|is_unique[users.username,id,{id}]'
                ],
                'email'     =>   [
                    'label' => "Email",
                    'rules' => 'required|max_length[100]|valid_email|is_unique[auth_identities.secret,user_id,{id}]'
                ],
                'first_name'     =>   [
                    'label' => "Nombre",
                    'rules' => 'max_length[100]'
                ],
                'last_name'     =>   [
                    'label' => "Apellido",
                    'rules' => 'max_length[100]'
                ],
                'group'            => [
                    'label' => 'Grupo',
                    'rules' => 'required'
                ]
            ]);

            $data = (array)$this->request->getJSON();
            $data['id'] = $id;
            $response = array();


            if ($this->validation->run($data) == FALSE) {
                $response['status']    = false;
                $response['messages']   = $this->validation->getErrors(); //Show Error in Input Form

                return $this->respond($response, 422);
            } else {
                $detailFields = [
                    'username'      => $data['username'],
                    'first_name'    => $data['first_name'],
                    'last_name'     => $data['last_name'],
                    'email'         => $data['email'],
                    'active'        => $data['active']
                ];

                $forceResetPassword = $data['forceresetpassword'];

                $users   = auth()->getProvider();
                // Get the User Provider (UserModel by default)
                $user = $users->findById($id);

                $user->fill($detailFields);

                if ($forceResetPassword) {
                    $user->forcePasswordReset();
                    $user->active = false;
                    $user->status_message = "User not active. Must reset password";
                }

                if (!$data['active']) {
                    $user->ban(lang('Auth.bannedUser'));
                } else {
                    $user->unBan();
                }

                if ($users->save($user)) {
                    $response = [
                        'status' => true,
                        'messages'  => lang("App.insert-success"),
                        'data' => ['id' => $user->id]
                    ];
                } else {
                    $response = [
                        'status' => false,
                        'messages'  => lang("App.insert-error")
                    ];
                }
            }
            return $this->respond($response, 200);
        } catch (\Throwable $th) {
            log_message('error', '[ERROR] {exception}', ['exception' => $th]);
            return $this->respond(['messages' => ['' => $th->getMessage()], 'status' => false], 500);
        }
    }

    /**
     * Delete the designated resource object from the model
     *
     * @return mixed
     */
    public function delete($id = null)
    {
        try {
            $original = $this->model->find($id);
            $result = $this->model->delete($id);
            $response = [
                'status' => true,
                'data' => ['id' => $id]
            ];
            return $this->respond($response, 200);
        } catch (\Throwable $th) {
            log_message('error', '[ERROR] {exception}', ['exception' => $th]);
            return $this->respond(['messages' => ['' => $th->getMessage()], 'status' => false], 500);
        }
    }

    /**
     * Get list of resources to select list
     * 
     * @return array
     */
    public function select()
    {
        try {
            $data = $this->model->select('id, username AS text')->orderBy('username')->get()->getResult();

            return $this->respond($data, 200);
        } catch (\Throwable $th) {
            return $this->respond([], 500);
        }
    }

    public function profile()
    {
        try {
            $clientsModel = new ClientsModel();
            $userId = auth()->user()->id;
            $data = $clientsModel->getByUserId($userId);

            return $this->respond($data, 200);
        } catch (\Throwable $th) {
            return $this->respond([], 500);
        }
    }

    public function profilepost()
    {
        try {
            $validation = \Config\Services::validation();

            $validation->setRules([
                'first_name'                => ['label' => 'Nombre',            'rules'          => 'required'],
                'last_name'                 => ['label' => 'Apellido',          'rules'          => 'required'],
                'business_name'             => ['label' => 'Nombre comercial',  'rules'          => 'required'],
                'email'                     => ['label' => 'Email',             'rules'          => 'required|valid_email'],
                'username'                  => ['label' => 'Nombre de usuario', 'rules'          => 'required|max_length[30]|min_length[3]'],
            ]);

            $data = (array)$this->request->getJSON();

            if ($validation->run($data) == FALSE) {
                $response['status']    = false;
                $response['messages']   = $validation->getErrors(); //Show Error in Input Form
                return $this->respond($response, 400);
            }

            $usersModel = new UsersModel();
            $clientsModel = new ClientsModel();

            $clientEntity = new ClientEntity($data);
            $clientEntity->has_required_data = 1;
            $userEntity   = new UserEntity($data);
            $userEntity->id = $clientEntity->user_id;

            $clientsModel->save($clientEntity);
            $usersModel->save($userEntity);


            $response = [
                'status' => true,
                'messages' => "Procesado correctamente"
            ];

            return $this->respond($response, 200);
        } catch (\Throwable $th) {
            log_message('error', '[ERROR] {exception}', ['exception' => $th]);
            $response = ['messages' => ['' => $th->getMessage()], 'status' => false];
            return $this->respond($response, 500);
        }
    }
}
