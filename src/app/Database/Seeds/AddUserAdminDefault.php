<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;
use CodeIgniter\CLI\CLI;

class AddUserAdminDefault extends Seeder
{
    /**
     * 1) Crea un registro en la tabla user para un usuario de prueba
     * 2) Crea un registro en la tabla auth_identities para el usuario creado en el paso 1
     * 3) Crea un registro en la tabla auth_groups_users para asignar el rol de admin al usuario creado en el paso 1
     * 4) Asigna los permisos de admin al usuario creado en el paso 1
     */

    public function run()
    {
        //1)
        $dataUser = [
            'username'      => 'admin',
            'active'        => 1,
            'company_id'    => 1,
            'created_at'    =>  date("Y-m-d H:i:a", time())
        ];

        $this->db->table('users')->insert($dataUser);

        $userId = $this->db->insertID();

        //2)
        $dataIdentity = [
            'user_id'       => $userId,
            'name'          => 'Administrator',
            'type'          => 'email_password',
            'secret'        => 'admin@comprabox.com',
            'secret2'        => '$2y$10$HdQg2qGovYYLyT29kVc9oOAV/I85lYu8vw2fXSoLv9.RLnGKnNzIq', //qwe123RT$%
            'force_reset'   => 0,
            'created_at'    =>  date("Y-m-d H:i:a", time())
        ];

        $this->db->table('auth_identities')->insert($dataIdentity);

        //3)
        $dataUserGroup = [
            'user_id'   => $userId,
            'group'     => 'admin',
            'created_at' =>  date("Y-m-d H:i:a", time())
        ];

        $this->db->table('auth_groups_users')->insert($dataUserGroup);

        //4)
        $dataPermissionsUser = [];
        $config = config(\Config\AuthGroups::class);

        $permissionsGroup = $config->matrix['admin']; //permisos del grupo
        $permissionsConfig = array_keys($config->permissions); //todos los permisos definidos

        for ($i = 0; $i < sizeof($permissionsGroup); $i++) {
            $permissionExplode =  explode('.', $permissionsGroup[$i]);
            $module = $permissionExplode[0];
            $permission = $permissionExplode[1];

            if ($permission == "*") {
                $permissionsModule = array_values(array_filter($permissionsConfig, function ($v, $k) use ($module) {
                    return explode('.', $v)[0] == $module;
                }, ARRAY_FILTER_USE_BOTH));

                for ($j = 0; $j < sizeof($permissionsModule); $j++) {
                    $dataPermissionsUser[] = [
                        'user_id'       => $userId,
                        'permission'    => $permissionsModule[$j],
                        'created_at'    => date("Y-m-d H:i:a", time())
                    ];
                }
            } else {
                $dataPermissionsUser[] = [
                    'user_id'       => $userId,
                    'permission'    => $permissionsGroup[$i],
                    'created_at'    => date("Y-m-d H:i:a", time())
                ];
            }
        }

        if (sizeof($dataPermissionsUser) > 0)
            $this->db->table('auth_permissions_users')->insertBatch($dataPermissionsUser);


        CLI::write("user_id created: {$userId}", 'green');
    }
}
