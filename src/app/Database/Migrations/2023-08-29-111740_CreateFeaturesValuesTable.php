<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class CreateFeaturesValuesTable extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'INT',
                'unsigned' => true,
                'auto_increment' => true
            ],
            'feature_id' => [
                'type' => 'INT',
                'unsigned' => true
            ],
            'value' => [
                'type' => 'VARCHAR',
                'constraint' => 250,
                'null' => false
            ],
            'company_id' => [
                'type'  => 'INT',
                'null'  => false
            ],
            'created_by' => [
                'type' => 'VARCHAR',
                'null' => false,
                'constraint' => 100
            ],
            'updated_by' => [
                'type' => 'VARCHAR',
                'constraint' => 100,
                'null'  => true
            ],
            'deleted_by' => [
                'type' => 'VARCHAR',
                'constraint' => 100,
                'null'  => true
            ],
            'created_at datetime default current_timestamp',
            'updated_at datetime default current_timestamp on update current_timestamp',
            'deleted_at datetime'
        ]);
        $this->forge->addKey('id', true);
        $this->forge->addKey('feature_id');
        $this->forge->addForeignKey('feature_id', 'features', 'id', '', 'CASCADE', 'fk_features_values');
        $this->forge->createTable('features_values');
    }

    public function down()
    {
        $this->forge->dropTable('features_values');
    }
}
