<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class CreateClientsTable extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'INT',
                'unsigned' => true,
                'auto_increment' => true
            ],
            'user_id' => [
                'type' => 'INT',
                'unsigned' => true
            ],
            'identity_number' => [
                'type' => 'VARCHAR',
                'constraint' => 20,
                'null' => true
            ],
            'identity_type' => [
                'type' => 'VARCHAR',
                'constraint' => 5,
                'null' => true
            ],
            'business_name' => [
                'type' => 'VARCHAR',
                'constraint' => 100
            ],
            'contact_person' => [
                'type' => 'VARCHAR',
                'constraint' => 100,
                'null' => true
            ],
            'phone_number' => [
                'type' => 'VARCHAR',
                'constraint' => 50,
                'null' => true
            ],
            'email' => [
                'type' => 'VARCHAR',
                'constraint' => 100,
                'null' => true
            ],
            'address' => [
                'type' => 'VARCHAR',
                'constraint' => 250,
                'null' => true
            ],
            'billing_address' => [
                'type' => 'VARCHAR',
                'constraint' => 250,
                'null' => true
            ],
            'province_id'  => [
                'type' => 'INT',
                'null' => true
            ],
            'city_id'  => [
                'type' => 'INT',
                'null' => true
            ],
            'billing_province_id'  => [
                'type' => 'INT',
                'null' => true
            ],
            'billing_city_id'  => [
                'type' => 'INT',
                'null' => true
            ],
            'bonus' => [
                'type' => 'DECIMAL',
                'null' => true,
                'constraint' => '10,4'
            ],
            'shipping_id' => [
                'type' => 'INT',
                'null' => true,
                'unsigned' => true
            ],
            'notes' => [
                'type' => 'VARCHAR',
                'null' => true,
                'constraint' => 250
            ],
            'has_required_data' => [
                'type' => 'TINYINT',
                'null' => false,
                'default' => 0
            ],
            'company_id' => [
                'type'  => 'INT',
                'null'  => false
            ],
            'created_by' => [
                'type' => 'VARCHAR',
                'null' => false,
                'constraint' => 100
            ],
            'updated_by' => [
                'type' => 'VARCHAR',
                'constraint' => 100,
                'null'  => true
            ],
            'deleted_by' => [
                'type' => 'VARCHAR',
                'constraint' => 100,
                'null'  => true
            ],
            'created_at datetime default current_timestamp',
            'updated_at datetime default current_timestamp on update current_timestamp',
            'deleted_at datetime'
        ]);
        $this->forge->addKey('id', true);
        $this->forge->addKey('shipping_id');
        $this->forge->addForeignKey('shipping_id', 'shippings', 'id', '', 'CASCADE', 'fk_clients_shippings');
        $this->forge->createTable('clients');
    }

    public function down()
    {
        $this->forge->dropTable('clients');
    }
}
