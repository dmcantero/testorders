<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddFieldCompanyidTableUsers extends Migration
{
    public function up()
    {
        $fields = [
            'company_id' => [
                'type'  => 'INT',
                'null'  => false,
                'after' => 'last_active'
            ]
        ];
        $this->forge->addColumn('users', $fields);
    }

    public function down()
    {
        //
    }
}
