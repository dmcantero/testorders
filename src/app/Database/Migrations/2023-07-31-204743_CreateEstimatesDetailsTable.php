<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class CreateEstimateDetailsTable extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'INT',
                'unsigned' => true,
                'auto_increment' => true
            ],
            'estimate_id' => [
                'type' => 'INT',
                'unsigned' => true,
            ],
            'date datetime default current_timestamp',
            'product_id' => [
                'type' => 'INT',
                'unsigned' => true
            ],
            'price_list_id' => [
                'type' => 'INT',
                'unsigned' => true
            ],
            'bulk_princing_id' => [
                'type' => 'INT',
                'unsigned' => true
            ],
            'unit_price' => [
                'type' => 'DECIMAL',
                'constraint' => '10,4'
            ],
            'quantity' => [
                'type' => 'DECIMAL',
                'constraint' => '10,4',
                'default' => 0
            ],
            'subtotal' => [
                'type' => 'DECIMAL',
                'constraint' => '10,4',
                'default' => 0
            ],
            'confirmed_quantity' => [
                'type' => 'DECIMAL',
                'constraint' => '10,4',
                'default'   => 0
            ],
            'confirmed_subtotal' => [
                'type' => 'DECIMAL',
                'constraint' => '10,4',
                'default'   => 0
            ],
            'discount' => [
                'type' => 'DECIMAL',
                'constraint' => '10,4',
                'default'   => 0
            ],
            'discount_percentage' => [
                'type' => 'DECIMAL',
                'constraint' => '10,4',
                'default'   => 0
            ],
            'company_id' => [
                'type'  => 'INT',
                'null'  => false
            ],
            'created_by' => [
                'type' => 'VARCHAR',
                'null' => false,
                'constraint' => 100
            ],
            'updated_by' => [
                'type' => 'VARCHAR',
                'constraint' => 100,
                'null'  => true
            ],
            'deleted_by' => [
                'type' => 'VARCHAR',
                'constraint' => 100,
                'null'  => true
            ],
            'created_at datetime default current_timestamp',
            'updated_at datetime default current_timestamp on update current_timestamp',
            'deleted_at datetime'
        ]);
        $this->forge->addKey('id', true);
        $this->forge->addKey('estimate_id');
        $this->forge->addForeignKey('estimate_id', 'estimates', 'id', '', 'CASCADE', 'fk_estimate_details_estimates');
        $this->forge->addKey('product_id');
        $this->forge->addForeignKey('product_id', 'products', 'id', '', 'CASCADE', 'fk_estimate_details_products');
        $this->forge->addKey('bulk_princing_id');
        $this->forge->addForeignKey('bulk_princing_id', 'bulk_pricing', 'id', '', 'CASCADE', 'fk_estimate_details_bulk_pricing');
        $this->forge->createTable('estimate_details');
    }

    public function down()
    {
        $this->forge->dropTable('estimate_details', true);
    }
}
