<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class CreateOrdersTable extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'INT',
                'unsigned' => true,
                'auto_increment' => true
            ],
            'number' => [
                'type' => 'VARCHAR',
                'constraint' => 100,
                'null' => false
            ],
            'client_id' => [
                'type' => 'INT',
                'unsigned' => true,
            ],
            'user_id' => [
                'type' => 'INT',
                'unsigned' => true,
            ],
            'date' => [
                'type' => 'datetime'
            ],
            'confirmation_date' => [
                'type' => 'datetime',
                'null' => true
            ],
            'delivery_date' => [
                'type' => 'datetime',
                'null' => true
            ],
            'shipping_date' => [
                'type' => 'datetime',
                'null' => true
            ],
            'shipping_method' => [
                'type' => 'VARCHAR',
                'constraint' => 10,
                'null'  => false
            ],
            'free_shipping' => [
                'type' => 'VARCHAR',
                'constraint' => 1,
                'null'  => false
            ],
            'client_fullname' => [
                'type' => 'VARCHAR',
                'constraint' => 100,
                'null'  => true
            ],
            'estimate_id' => [
                'type' => 'INT',
                'null'  => true,
                'unsigned' => true
            ],
            'currency' => [
                'type' => 'VARCHAR(5)',
                'null' => true
            ],
            'exchange_rate' => [
                'type' => 'DECIMAL',
                'constraint' => '10,4',
                'default' => 1,
                'null' => true
            ],
            'discount_percentage' => [
                'type' => 'DECIMAL',
                'constraint' => '10,4',
                'default' => 0
            ],
            'sale_condition_id' => [
                'type' => 'INT',
                'null' => true,
                'unsigned' => true
            ],
            'credit_term_id' => [
                'type' => 'INT',
                'null' => true,
                'unsigned' => true
            ],
            'notes' => [
                'type' => 'VARCHAR',
                'constraint' => 200,
                'null' => true
            ],
            'total' => [
                'type' => 'DECIMAL',
                'constraint' => '10,4'
            ],
            'total_discount' => [
                'type' => 'DECIMAL',
                'constraint' => '10,4'
            ],
            'total_taxes' => [
                'type' => 'DECIMAL',
                'constraint' => '10,4'
            ],
            'total_net' => [
                'type' => 'DECIMAL',
                'constraint' => '10,4'
            ],
            'company_id' => [
                'type'  => 'INT',
                'null'  => false
            ],
            'date datetime default current_timestamp',
            'created_by' => [
                'type' => 'VARCHAR',
                'null' => false,
                'constraint' => 100
            ],
            'updated_by' => [
                'type' => 'VARCHAR',
                'constraint' => 100,
                'null'  => true
            ],
            'deleted_by' => [
                'type' => 'VARCHAR',
                'constraint' => 100,
                'null'  => true
            ],
            'created_at datetime default current_timestamp',
            'updated_at datetime default current_timestamp on update current_timestamp',
            'deleted_at datetime'
        ]);
        $this->forge->addKey('id', true);

        $this->forge->addKey('client_id');
        $this->forge->addForeignKey('client_id', 'clients', 'id', '', 'CASCADE', 'fk_orders_clients');

        $this->forge->addKey('user_id');
        $this->forge->addForeignKey('user_id', 'users', 'id', '', 'CASCADE', 'fk_orders_users');

        $this->forge->addKey('estimate_id');
        $this->forge->addForeignKey('estimate_id', 'estimates', 'id', '', 'CASCADE', 'fk_orders_estimates');

        $this->forge->addKey('sale_condition_id');
        $this->forge->addForeignKey('sale_condition_id', 'sale_conditions', 'id', '', 'CASCADE', 'fk_orders_sales_conditions');
        
        $this->forge->addKey('credit_term_id');
        $this->forge->addForeignKey('credit_term_id', 'credit_terms', 'id', '', 'CASCADE', 'fk_orders_credit_terms');
        
        $this->forge->createTable('orders');
    }

    public function down()
    {
        $this->forge->dropTable('orders', true);
    }
}
