<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class CreateProductsTable extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'INT',
                'unsigned' => true,
                'auto_increment' => true
            ],
            'code'  => [
                'type' => 'VARCHAR',
                'constraint' => 100,
                'null' => false
            ],
            'name' => [
                'type' => 'VARCHAR',
                'constraint' => 250,
                'null' => false
            ],
            'description' => [
                'type' => 'VARCHAR',
                'constraint' => 500,
                'default'   => '',
                'null' => true
            ],
            'purchase_price' => [
                'type' => 'DECIMAL',
                'constraint' => '10,4',
                'null' => true,
                'default' => 0.00
            ],
            'sale_price' => [
                'type' => 'DECIMAL',
                'constraint' => '10,4',
                'default' => 0.00
            ],
            'sale_price_unit' => [
                'type' => 'DECIMAL',
                'constraint' => '10,4',
                'null' => true,
                'default' => 0.00
            ],
            'discount_percentage' => [
                'type' => 'DECIMAL',
                'constraint' => '10,4',
                'default' => 0.00
            ],
            'category_id' => [
                'type' => 'INT',
                'unsigned' => true,
                'null' => true
            ],
            'brand_id' => [
                'type' => 'INT',
                'unsigned' => true,
                'null' => true
            ],
            'supplier_id' => [
                'type' => 'INT',
                'unsigned' => true,
                'null' => true
            ],
            'min_package' => [
                'type' => 'DECIMAL',
                'constraint' => '10,4',
                'null' => true
            ],
            'image_url' => [
                'type' => 'VARCHAR',
                'constraint' => '250',
                'null' => true
            ],
            'active' => [
                'type' => 'TINYINT',
                'default' => 1
            ],
            'stock' => [
                'type' => 'DECIMAL',
                'constraint' => '10,4',
                'default' => 0
            ],
            'min_stock' => [
                'type' => 'DECIMAL',
                'constraint' => '10,4',
                'null' => true,
                'default' => 0
            ],
            'bulk_quantity' => [
                'type' => 'DECIMAL',
                'constraint' => '10,4',
                'default' => 1
            ],
            'is_new' => [
                'type' => 'TINYINT',
                'default' => 1,
                'null' => true
            ],
            'company_id' => [
                'type'  => 'INT',
                'null'  => false
            ],
            'created_by' => [
                'type' => 'VARCHAR',
                'null' => false,
                'constraint' => 100
            ],
            'updated_by' => [
                'type' => 'VARCHAR',
                'constraint' => 100,
                'null'  => true
            ],
            'deleted_by' => [
                'type' => 'VARCHAR',
                'constraint' => 100,
                'null'  => true
            ],
            'created_at datetime default current_timestamp',
            'updated_at datetime default current_timestamp on update current_timestamp',
            'deleted_at datetime'
        ]);
        $this->forge->addPrimaryKey('id');
        $this->forge->addKey('category_id');
        $this->forge->addForeignKey('category_id', 'categories', 'id', '', 'CASCADE', 'fk_products_categories');
        $this->forge->addKey('brand_id');
        $this->forge->addForeignKey('brand_id', 'brands', 'id', '', 'CASCADE', 'fk_products_brands');
        $this->forge->addKey('supplier_id');
        $this->forge->addForeignKey('supplier_id', 'suppliers', 'id', '', 'CASCADE', 'fk_products_suppliers');
        $this->forge->createTable('products');
    }

    public function down()
    {
        $this->forge->dropTable('products');
    }
}
