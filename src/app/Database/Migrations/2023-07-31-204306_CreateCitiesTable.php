<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class CreateCitiesTable extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'INT',
                'unsigned' => true,
                'auto_increment' => true
            ],
            'province_id' => [
                'type' => 'INT',
                'unsigned' => true
            ],
            'name' => [
                'type' => 'VARCHAR',
                'constraint' => 250,
                'null' => false
            ]
        ]);
        $this->forge->addKey('id', true);
        $this->forge->addKey('province_id');
        $this->forge->addForeignKey('province_id', 'provinces', 'id', '', 'CASCADE');
        $this->forge->createTable('cities');
    }

    public function down()
    {
        $this->forge->dropTable('cities');
    }
}
