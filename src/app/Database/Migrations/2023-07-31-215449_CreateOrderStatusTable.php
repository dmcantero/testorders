<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class CreateOrderStatusTable extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'INT',
                'unsigned' => true,
                'auto_increment' => true
            ],
            'order_id' => [
                'type' => 'INT',
                'unsigned' => true
            ],
            'status' => [
                'type' => 'VARCHAR',
                'constraint' => 20,
            ],
            'company_id' => [
                'type'  => 'INT',
                'null'  => false
            ],
            'date datetime default current_timestamp',
            'created_by' => [
                'type' => 'VARCHAR',
                'null' => false,
                'constraint' => 100
            ],
            'updated_by' => [
                'type' => 'VARCHAR',
                'constraint' => 100,
                'null'  => true
            ],
            'deleted_by' => [
                'type' => 'VARCHAR',
                'constraint' => 100,
                'null'  => true
            ],
            'created_at datetime default current_timestamp',
            'updated_at datetime default current_timestamp on update current_timestamp',
            'deleted_at datetime'
        ]);
        $this->forge->addKey('id', true);
        $this->forge->addKey('order_id');
        $this->forge->addForeignKey('order_id', 'orders', 'id', '', 'CASCADE', 'fk_order_states_order');
        $this->forge->createTable('order_status');
    }

    public function down()
    {
        $this->forge->dropTable('order_status');
    }
}
