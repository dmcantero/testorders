<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class CreateCreditTermsTable extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'INT',
                'unsigned' => true,
                'auto_increment' => true
            ],
            'description' => [
                'type' => 'VARCHAR',
                'constraint' => 50,
                'null' => false
            ],
            'sale_condition_id' => [
                'type'  => 'INT',
                'unsigned'  => true
            ],
            'percentage' => [
                'type'  => 'DECIMAL',
                'constraint' => '10,4',
                'null' => true
            ],
            'fees' => [
                'type'  => 'INT',
                'null' => true
            ],
            'expiration_days' => [
                'type'  => 'INT',
                'null'  => true,
            ],
            'days_between_feets' => [
                'type'  => 'INT',
                'null'  => true,
            ],
            'percentage' => [
                'type'  => 'DECIMAL',
                'constraint' => '10,4',
                'null'  => true,
            ],
            'company_id' => [
                'type'  => 'INT',
                'null'  => false
            ],
            'created_by' => [
                'type' => 'VARCHAR',
                'null' => false,
                'constraint' => 100
            ],
            'updated_by' => [
                'type' => 'VARCHAR',
                'constraint' => 100,
                'null'  => true
            ],
            'deleted_by' => [
                'type' => 'VARCHAR',
                'constraint' => 100,
                'null'  => true
            ],
            'created_at datetime default current_timestamp',
            'updated_at datetime default current_timestamp on update current_timestamp',
            'deleted_at datetime'
        ]);
        $this->forge->addKey('id', true);
        $this->forge->createTable('credit_terms');
    }

    public function down()
    {
        $this->forge->dropTable('credit_terms');
    }
}
