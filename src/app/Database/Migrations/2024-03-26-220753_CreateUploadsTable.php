<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class CreateUploadsTable extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'INT',
                'unsigned' => true,
                'auto_increment' => true
            ],
            'original_name'  => [
                'type' => 'VARCHAR',
                'constraint' => 100,
                'null' => FALSE
            ],
            'new_name' => [
                'type' => 'VARCHAR',
                'constraint' => 100,
                'null' => FALSE
            ],
            'description' => [
                'type' => 'VARCHAR',
                'constraint' => 200,
                'default'   => '',
                'null' => TRUE
            ],
            'path' => [
                'type' => 'VARCHAR',
                'constraint' => 200,
                'null' => FALSE
            ],
            'extension' => [
                'type' => 'VARCHAR',
                'constraint' => 20,
                'null' => FALSE
            ],
            'type' => [
                'type' => 'VARCHAR',
                'constraint' => 20,
                'null' => FALSE
            ],
            'id_type' => [
                'type' => 'INT',
                'null' => true
            ],
            'company_id' => [
                'type'  => 'INT',
                'null'  => false
            ],
            'created_by' => [
                'type' => 'VARCHAR',
                'null' => false,
                'constraint' => 100
            ],
            'updated_by' => [
                'type' => 'VARCHAR',
                'constraint' => 100,
                'null'  => true
            ],
            'deleted_by' => [
                'type' => 'VARCHAR',
                'constraint' => 100,
                'null'  => true
            ],
            'created_at datetime default current_timestamp',
            'updated_at datetime default current_timestamp on update current_timestamp',
            'deleted_at datetime'
        ]);
        $this->forge->addPrimaryKey('id');
        $this->forge->createTable('uploads');
    }

    public function down()
    {
        $this->forge->dropTable('uploads');
    }
}
